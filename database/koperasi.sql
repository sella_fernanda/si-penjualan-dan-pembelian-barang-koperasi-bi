-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2020 at 04:43 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koperasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id_akun` int(11) NOT NULL,
  `nip` varchar(10) DEFAULT NULL,
  `id_peran` varchar(10) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id_akun`, `nip`, `id_peran`, `username`, `password`) VALUES
(1, '16031', 'P001', 'sella1', '16031'),
(2, '16031', 'P002', 'sella2', '16031'),
(3, '16031', 'P003', 'sella3', '16031'),
(4, '16032', 'P004', 'nur4', '16032'),
(5, '16032', 'P005', 'nur5', '16032'),
(6, '14909', 'P001', 'abror12', '12345'),
(7, '16072', 'P005', 'gampang123', '12345'),
(8, '18762', 'P002', 'niken11', '12345'),
(9, '16081', 'P004', 'sarwoto', '12345'),
(10, '18762', 'P003', 'niken22', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `no_barcode` varchar(30) NOT NULL,
  `id_kategori` varchar(10) DEFAULT NULL,
  `id_jns_satuan` varchar(10) DEFAULT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `stok_awal` int(20) NOT NULL,
  `stok_total` int(20) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`no_barcode`, `id_kategori`, `id_jns_satuan`, `nama_barang`, `stok_awal`, `stok_total`, `gambar`) VALUES
('4751008716', 'K001', 'JS004', 'Buku', 12, 24, 'buku.jpg'),
('5003361239', 'K003', 'JS004', 'Aqua 600ml', 15, 15, 'aqua1.jpg'),
('5095261899', 'K001', 'JS004', 'Bolpoint Pilot', 10, 19, 'bolpoint1.jpg'),
('8791283391', 'K002', 'JS008', 'Oreo', 5, 39, 'oreo2.jpg'),
('8992745320142', 'K001', 'JS004', 'Bolpoint kenko 1', 10, 5, 'gulaku1.jpg'),
('8998103017488', 'K003', 'JS008', 'teh kotak', 0, 11, 'user.png');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembelian`
--

CREATE TABLE `detail_pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `no_pembelian` varchar(30) DEFAULT NULL,
  `id_kategori` varchar(10) DEFAULT NULL,
  `no_barcode` varchar(30) DEFAULT NULL,
  `jumlah_beli` int(20) NOT NULL,
  `harga_beli` int(20) NOT NULL,
  `ppn` int(20) NOT NULL,
  `diskon` int(20) NOT NULL,
  `hpp` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pembelian`
--

INSERT INTO `detail_pembelian` (`id_pembelian`, `no_pembelian`, `id_kategori`, `no_barcode`, `jumlah_beli`, `harga_beli`, `ppn`, `diskon`, `hpp`) VALUES
(129, 'PB00000001', 'K001', '4751008716', 12, 5000, 0, 0, 5500),
(130, 'PB00000001', 'K001', '5095261899', 12, 2500, 0, 0, 3000),
(131, 'PB00000002', 'K002', '8791283391', 24, 2000, 0, 0, 2000),
(132, 'PB00000002', 'K003', '8998103017488', 12, 3500, 0, 0, 3500),
(134, 'PB00000003', 'K003', '5003361239', 9, 3500, 0, 0, 3500),
(135, 'PB00000003', 'K002', '8791283391', 12, 2000, 0, 0, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_pengadaan`
--

CREATE TABLE `detail_pengadaan` (
  `id_pengadaan` int(11) NOT NULL,
  `no_pengadaan` varchar(20) DEFAULT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `jumlah_barang` int(20) NOT NULL,
  `id_jns_satuan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pengadaan`
--

INSERT INTO `detail_pengadaan` (`id_pengadaan`, `no_pengadaan`, `nama_barang`, `jumlah_barang`, `id_jns_satuan`) VALUES
(28, 'PA00001', 'Aqua 600ml', 1, 'JS008'),
(29, 'PA00001', 'Beras Lele 5kg', 2, 'JS007'),
(30, 'PA00002', 'Beras Lele 5kg', 5, 'JS004'),
(31, 'PA00003', 'Minyak Goreng Bimoli', 2, 'JS009'),
(32, 'PA00003', 'Beras Lele 5kg', 2, 'JS009'),
(33, 'PA00004', 'Gulaku', 2, 'JS004'),
(34, 'PA00004', 'Kecap bango botol', 3, 'JS004');

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `id_penjualan` int(11) NOT NULL,
  `no_transaksi` varchar(20) DEFAULT NULL,
  `no_barcode` varchar(30) DEFAULT NULL,
  `jumlah_barang` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_penjualan`
--

INSERT INTO `detail_penjualan` (`id_penjualan`, `no_transaksi`, `no_barcode`, `jumlah_barang`) VALUES
(172, NULL, '4751008716', 2),
(182, NULL, '5095261899', 1),
(183, NULL, '5003361239', 1),
(191, 'PJ00001', '5003361239', 2),
(192, 'PJ00001', '5095261899', 2),
(193, 'PJ00002', '5003361239', 2),
(194, 'PJ00003', '5003361239', 1),
(195, 'PJ00003', '5095261899', 1),
(196, 'PJ00003', '8998103017488', 1),
(197, 'PJ00004', '8791283391', 2),
(198, 'PJ00004', '5003361239', 1),
(199, 'PJ00005', '5003361239', 3),
(200, 'PJ00006', '8992745320142', 5);

-- --------------------------------------------------------

--
-- Table structure for table `harga`
--

CREATE TABLE `harga` (
  `id_harga` int(11) NOT NULL,
  `harga_jual` int(20) NOT NULL,
  `keterangan` text NOT NULL,
  `no_barcode` varchar(30) DEFAULT NULL,
  `tgl_ubah` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga`
--

INSERT INTO `harga` (`id_harga`, `harga_jual`, `keterangan`, `no_barcode`, `tgl_ubah`) VALUES
(86, 2000, 'Tidak Berlaku', '5095261899', '2020-07-07 04:35:10'),
(87, 5000, 'Tidak Berlaku', '4751008716', '2020-07-07 04:32:42'),
(88, 9000, 'Tidak Berlaku', '8791283391', '2020-07-07 04:34:21'),
(89, 3000, 'Tidak Berlaku', '5003361239', '2020-07-07 04:35:30'),
(92, 2000, 'Tidak Berlaku', '8992745320142', '2020-07-07 05:13:29'),
(98, 0, 'Tidak Berlaku', '8998103017488', '2020-07-18 03:13:38'),
(115, 5775, 'Tidak Berlaku', '4751008716', '2020-07-21 01:22:19'),
(116, 2750, 'Berlaku', '5095261899', '2020-07-21 01:22:20'),
(117, 6050, 'Tidak Berlaku', '8791283391', '2020-07-21 01:24:06'),
(118, 3850, 'Berlaku', '8998103017488', '2020-07-21 01:24:06'),
(119, 3850, 'Tidak Berlaku', '4751008716', '2020-07-21 01:25:53'),
(120, 5775, 'Berlaku', '4751008716', '2020-07-21 01:26:02'),
(121, 3575, 'Berlaku', '5003361239', '2020-07-21 01:57:10'),
(122, 4767, 'Berlaku', '8791283391', '2020-07-21 01:57:10'),
(123, 2500, 'Berlaku', '8992745320142', '2020-07-26 04:29:36');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pembayaran`
--

CREATE TABLE `jenis_pembayaran` (
  `id_jns_pembayaran` varchar(10) NOT NULL,
  `nama_pembayaran` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pembayaran`
--

INSERT INTO `jenis_pembayaran` (`id_jns_pembayaran`, `nama_pembayaran`) VALUES
('P001', 'Tunai'),
('P002', 'Hutang'),
('P003', 'Piutang');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_satuan`
--

CREATE TABLE `jenis_satuan` (
  `id_jns_satuan` varchar(10) NOT NULL,
  `nama_satuan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_satuan`
--

INSERT INTO `jenis_satuan` (`id_jns_satuan`, `nama_satuan`) VALUES
('JS001', 'Lusin'),
('JS003', 'Bendel'),
('JS004', 'Buah'),
('JS006', 'Codi'),
('JS007', 'Pack'),
('JS008', 'Box'),
('JS009', 'biji'),
('JS010', 'Kodi 2');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_barang`
--

CREATE TABLE `kategori_barang` (
  `id_kategori` varchar(10) NOT NULL,
  `nama_kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_barang`
--

INSERT INTO `kategori_barang` (`id_kategori`, `nama_kategori`) VALUES
('K001', 'ATK'),
('K002', 'Makanan'),
('K003', 'Minuman'),
('K004', 'Sembako'),
('K005', 'Alat Tulis Kantor');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `nip` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nama_suami_istri` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tmp_lahir` varchar(50) NOT NULL,
  `no_ktp` varchar(50) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `jk` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `foto_profil` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`nip`, `nama`, `nama_suami_istri`, `tgl_lahir`, `tmp_lahir`, `no_ktp`, `no_hp`, `jk`, `alamat`, `foto_profil`) VALUES
('14909', 'M. Abror', '', '1989-08-10', 'Boyolali', '3216575676575003', '081335467999', 'L', 'Solo', ''),
('16031', 'Sella fernanda', '', '1999-05-23', 'Ponorogo', '1234567890', '081252654822', 'P', 'Sawoo, Ponorogo', 'sella.jpg\r\n'),
('16032', 'Nur Hayati', '', '1999-01-15', 'Sukoharjo', '329747484480002', '085601455235', 'P', 'Sukoharjo', 'nur.png'),
('16072', 'Gampang Suryo Atmojo', '', '1990-06-01', 'Surakrata', '3674890173649003', '081678589311', 'L', 'Surakarta', ''),
('16081', 'Sarwoto', '', '1989-03-10', 'Sukoharjo', '32623973493002', '087637864922', 'L', 'Sukoharjo', ''),
('18762', 'Niken Widya', '', '1989-09-16', 'Surakarta', '245354665655001', '082678356478', 'P', 'Surakarta', '');

-- --------------------------------------------------------

--
-- Table structure for table `peran`
--

CREATE TABLE `peran` (
  `id_peran` varchar(10) NOT NULL,
  `nama_peran` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peran`
--

INSERT INTO `peran` (`id_peran`, `nama_peran`) VALUES
('P001', 'Admin'),
('P002', 'Teller'),
('P003', 'Stoker'),
('P004', 'Viewer'),
('P005', 'Anggota');

-- --------------------------------------------------------

--
-- Table structure for table `retur`
--

CREATE TABLE `retur` (
  `id_retur` int(11) NOT NULL,
  `no_pembelian` varchar(30) DEFAULT NULL,
  `no_barcode` varchar(30) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retur`
--

INSERT INTO `retur` (`id_retur`, `no_pembelian`, `no_barcode`, `jumlah`, `keterangan`) VALUES
(18, 'PB00000003', '5003361239', 1, 'potong hutang');

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE `stok` (
  `id_stok` int(11) NOT NULL,
  `no_barcode` varchar(30) DEFAULT NULL,
  `stok_awal` int(20) NOT NULL,
  `stok_buy` int(20) NOT NULL,
  `stok_penjualan` int(20) NOT NULL,
  `stok_retur` int(20) NOT NULL,
  `total` int(20) NOT NULL,
  `stok_opname` int(20) NOT NULL,
  `gain_loss` int(20) NOT NULL,
  `tgl_histori` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok`
--

INSERT INTO `stok` (`id_stok`, `no_barcode`, `stok_awal`, `stok_buy`, `stok_penjualan`, `stok_retur`, `total`, `stok_opname`, `gain_loss`, `tgl_histori`) VALUES
(64, '5095261899', 10, 1, 0, 1, 12, 11, -1, '2020-07-07 09:38:42'),
(65, '8992745320142', 10, 9, 0, 1, 20, 19, -1, '2020-07-07 10:19:42'),
(66, '5003361239', 15, 3, 2, 1, 15, 14, -1, '2020-07-09 01:51:13'),
(67, '8998103017488', 0, 12, 0, 1, 11, 10, -1, '2020-07-18 08:42:58'),
(68, '5003361239', 15, 10, 1, 1, 24, 25, 1, '2020-07-21 06:58:08');

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `id_suplier` varchar(10) NOT NULL,
  `nama_suplier` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`id_suplier`, `nama_suplier`, `alamat`, `no_hp`) VALUES
('S001', 'Pena', 'Ngoresan', '081252654822'),
('S002', 'Asgross', 'Solo', '085675387621'),
('S003', 'Rea', 'Jebres', '087996885345'),
('S004', 'Surya Mart', 'Surakarta', '089778635466'),
('S005', 'Lotte Mart', 'Solo', '081243567832'),
('S006', 'Indogrosir', 'Palur', '085678933245'),
('S007', 'Surya Toko', 'ponorogo', '0863974623'),
('S008', 'Toko Gamaresa', 'Ponorogo sawoo', '087353275436');

-- --------------------------------------------------------

--
-- Table structure for table `trans_pembelian`
--

CREATE TABLE `trans_pembelian` (
  `no_pembelian` varchar(30) NOT NULL,
  `tgl_suply` date NOT NULL,
  `id_suplier` varchar(10) DEFAULT NULL,
  `id_jns_pembayaran` varchar(10) DEFAULT NULL,
  `nip` varchar(10) DEFAULT NULL,
  `transportasi` int(10) NOT NULL,
  `angkut` int(10) NOT NULL,
  `biaya_lain` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_pembelian`
--

INSERT INTO `trans_pembelian` (`no_pembelian`, `tgl_suply`, `id_suplier`, `id_jns_pembayaran`, `nip`, `transportasi`, `angkut`, `biaya_lain`) VALUES
('PB00000001', '2020-07-16', 'S002', 'P001', '18762', 10000, 0, 2000),
('PB00000002', '2020-07-17', 'S005', 'P001', '18762', 0, 0, 0),
('PB00000003', '2020-07-08', 'S004', 'P002', '18762', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `trans_pengadaan`
--

CREATE TABLE `trans_pengadaan` (
  `no_pengadaan` varchar(20) NOT NULL,
  `nip` varchar(10) DEFAULT NULL,
  `tgl_pengadaan` datetime NOT NULL,
  `id_jns_pembayaran` varchar(10) DEFAULT NULL,
  `status` varchar(30) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `teller` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_pengadaan`
--

INSERT INTO `trans_pengadaan` (`no_pengadaan`, `nip`, `tgl_pengadaan`, `id_jns_pembayaran`, `status`, `keterangan`, `teller`) VALUES
('PA00001', '16072', '2020-07-07 05:00:06', 'P003', 'Ready', 'Pengadaan Pegawai', '18762'),
('PA00002', '16072', '2020-07-07 05:26:56', 'P003', 'Selesai', 'Pengadaan Pegawai', '18762'),
('PA00003', '16072', '2020-07-21 03:02:09', 'P003', 'Ready', 'Pengadaan Pegawai', '18762'),
('PA00004', '16072', '2020-07-21 03:10:21', 'P003', 'On Process', 'Pengadaan Pegawai', '18762');

-- --------------------------------------------------------

--
-- Table structure for table `trans_penjualan`
--

CREATE TABLE `trans_penjualan` (
  `no_transaksi` varchar(20) NOT NULL,
  `nip` varchar(10) DEFAULT NULL,
  `tgl_transaksi` datetime NOT NULL,
  `id_jns_pembayaran` varchar(10) DEFAULT NULL,
  `total_bayar` int(20) NOT NULL,
  `status` varchar(30) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `opsi` varchar(20) NOT NULL,
  `teller` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_penjualan`
--

INSERT INTO `trans_penjualan` (`no_transaksi`, `nip`, `tgl_transaksi`, `id_jns_pembayaran`, `total_bayar`, `status`, `keterangan`, `opsi`, `teller`) VALUES
('PJ00001', '16072', '2020-07-21 07:35:09', 'P003', 12650, 'selesai', 'Pembelian Offline', 'none', '18762'),
('PJ00002', NULL, '2020-07-21 07:37:22', 'P001', 7150, 'Selesai', 'Pembelian Offline', 'none', '18762'),
('PJ00003', '16072', '2020-07-21 07:59:05', 'P001', 10175, 'Selesai', 'Pembelian Online', 'Diantar', '18762'),
('PJ00004', '16072', '2020-07-21 07:59:24', 'P001', 13109, 'On Process', 'Pembelian Online', 'Diantar', '18762'),
('PJ00005', NULL, '2020-07-21 09:01:46', 'P001', 10725, 'Selesai', 'Pembelian Offline', 'none', '18762'),
('PJ00006', '16072', '2020-07-21 09:14:27', 'P003', 10000, 'selesai', 'Pembelian Offline', 'none', '18762');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`),
  ADD KEY `nip` (`nip`),
  ADD KEY `id_peran` (`id_peran`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`no_barcode`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_jns_satuan` (`id_jns_satuan`);

--
-- Indexes for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD PRIMARY KEY (`id_pembelian`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `no_pembelian` (`no_pembelian`),
  ADD KEY `no_barcode` (`no_barcode`);

--
-- Indexes for table `detail_pengadaan`
--
ALTER TABLE `detail_pengadaan`
  ADD PRIMARY KEY (`id_pengadaan`),
  ADD KEY `no_pengadaan` (`no_pengadaan`),
  ADD KEY `id_jns_satuan` (`id_jns_satuan`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`id_penjualan`),
  ADD KEY `no_transaksi` (`no_transaksi`),
  ADD KEY `no_barcode` (`no_barcode`);

--
-- Indexes for table `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id_harga`),
  ADD KEY `no_barcode` (`no_barcode`);

--
-- Indexes for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  ADD PRIMARY KEY (`id_jns_pembayaran`);

--
-- Indexes for table `jenis_satuan`
--
ALTER TABLE `jenis_satuan`
  ADD PRIMARY KEY (`id_jns_satuan`);

--
-- Indexes for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `peran`
--
ALTER TABLE `peran`
  ADD PRIMARY KEY (`id_peran`);

--
-- Indexes for table `retur`
--
ALTER TABLE `retur`
  ADD PRIMARY KEY (`id_retur`),
  ADD KEY `no_pembelian` (`no_pembelian`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id_stok`),
  ADD KEY `no_barcode` (`no_barcode`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id_suplier`);

--
-- Indexes for table `trans_pembelian`
--
ALTER TABLE `trans_pembelian`
  ADD PRIMARY KEY (`no_pembelian`),
  ADD KEY `id_suplier` (`id_suplier`),
  ADD KEY `id_jns_pembayaran` (`id_jns_pembayaran`),
  ADD KEY `no_anggota` (`nip`);

--
-- Indexes for table `trans_pengadaan`
--
ALTER TABLE `trans_pengadaan`
  ADD PRIMARY KEY (`no_pengadaan`),
  ADD KEY `nip` (`nip`),
  ADD KEY `id_jns_pembayaran` (`id_jns_pembayaran`);

--
-- Indexes for table `trans_penjualan`
--
ALTER TABLE `trans_penjualan`
  ADD PRIMARY KEY (`no_transaksi`),
  ADD KEY `id_jns_pembayaran` (`id_jns_pembayaran`),
  ADD KEY `nip` (`nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `detail_pengadaan`
--
ALTER TABLE `detail_pengadaan`
  MODIFY `id_pengadaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  MODIFY `id_penjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `harga`
--
ALTER TABLE `harga`
  MODIFY `id_harga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `retur`
--
ALTER TABLE `retur`
  MODIFY `id_retur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `akun`
--
ALTER TABLE `akun`
  ADD CONSTRAINT `akun_ibfk_1` FOREIGN KEY (`id_peran`) REFERENCES `peran` (`id_peran`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `akun_ibfk_2` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_barang` (`id_kategori`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`id_jns_satuan`) REFERENCES `jenis_satuan` (`id_jns_satuan`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD CONSTRAINT `detail_pembelian_ibfk_3` FOREIGN KEY (`no_barcode`) REFERENCES `barang` (`no_barcode`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_pembelian_ibfk_4` FOREIGN KEY (`no_pembelian`) REFERENCES `trans_pembelian` (`no_pembelian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_pengadaan`
--
ALTER TABLE `detail_pengadaan`
  ADD CONSTRAINT `detail_pengadaan_ibfk_1` FOREIGN KEY (`no_pengadaan`) REFERENCES `trans_pengadaan` (`no_pengadaan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_pengadaan_ibfk_2` FOREIGN KEY (`id_jns_satuan`) REFERENCES `jenis_satuan` (`id_jns_satuan`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD CONSTRAINT `detail_penjualan_ibfk_1` FOREIGN KEY (`no_transaksi`) REFERENCES `trans_penjualan` (`no_transaksi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_penjualan_ibfk_2` FOREIGN KEY (`no_barcode`) REFERENCES `barang` (`no_barcode`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `harga`
--
ALTER TABLE `harga`
  ADD CONSTRAINT `harga_ibfk_1` FOREIGN KEY (`no_barcode`) REFERENCES `barang` (`no_barcode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `retur`
--
ALTER TABLE `retur`
  ADD CONSTRAINT `retur_ibfk_1` FOREIGN KEY (`no_pembelian`) REFERENCES `trans_pembelian` (`no_pembelian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stok`
--
ALTER TABLE `stok`
  ADD CONSTRAINT `stok_ibfk_1` FOREIGN KEY (`no_barcode`) REFERENCES `barang` (`no_barcode`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `trans_pembelian`
--
ALTER TABLE `trans_pembelian`
  ADD CONSTRAINT `trans_pembelian_ibfk_1` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `trans_pembelian_ibfk_2` FOREIGN KEY (`id_jns_pembayaran`) REFERENCES `jenis_pembayaran` (`id_jns_pembayaran`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `trans_pengadaan`
--
ALTER TABLE `trans_pengadaan`
  ADD CONSTRAINT `trans_pengadaan_ibfk_1` FOREIGN KEY (`id_jns_pembayaran`) REFERENCES `jenis_pembayaran` (`id_jns_pembayaran`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `trans_penjualan`
--
ALTER TABLE `trans_penjualan`
  ADD CONSTRAINT `trans_penjualan_ibfk_3` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `trans_penjualan_ibfk_4` FOREIGN KEY (`id_jns_pembayaran`) REFERENCES `jenis_pembayaran` (`id_jns_pembayaran`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
