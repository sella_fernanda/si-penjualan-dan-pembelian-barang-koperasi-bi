<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/icon/bilogohitam16px.png" />
    <title>Bank Indonesia | KOPEBI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/bower_components/select2/dist/css/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
</head>
<body class="hold-transition login-page" style="background-image: url(<?php echo base_url('assets/img/bi-login2.jpg');?>);background-repeat: no-repeat;background-size: cover;">

    <div class="container">
        <div class="login-box" style="background-color: #4682B4"> <br>
            <div class="login-logo">
                <a href="#" style="color: white; font-family: verdana;"><b>KOPEBI</b></a>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">

                    <?php
   // Cetak session
                    if($this->session->flashdata('sukses')) {
                        echo '<p class="info" style="margin: 10px 20px;">'.$this->session->flashdata('sukses').'</p>';
                    }
                    ?>

                    <form action="<?php echo base_url('index.php/C_login') ?>" method="post">
                        <form method="POST" class="register-form" id="login-form">
                            
                            <div class="form-group">
                                <label>Peran</label>
                                <select name="id_peran" class="form-control select2" style="width: 100%;">
                                    <?php     
                                    $i=1;          
                                    foreach ($id_peran->result_array() as $peran) { 
                                      echo "<option value='".$peran['id_peran']."' >".$i++.". ".$peran['nama_peran']."</option>";
                                  }
                                  ?>
                              </select>
                          </div>

                          <div class="form-group">
                            <label>Username :</label>
                            <?php
                            $data = array(
                                'name'          => 'username',         
                                'id'            => 'username',
                                'type'          => 'text',
                                'placeholder'   => 'Masukan Username',
                                'class'         => 'form-control',
                                'onkeyup'       => 'isi_otomatis()',
                                'autocomplete'  => 'off',
                                'autofocus'     => 'on', 
                                'required'      => 'on'
                            );
                            echo form_input($data);
                            echo form_error();
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Password :</label>
                            <?php
                            $data = array(
                                'name'          => 'password',         
                                'id'            => 'password',
                                'type'          => 'password',
                                'placeholder'   => 'Masukan Password',
                                'class'         => 'form-control',
                                'onkeyup'       => 'isi_otomatis()',
                                'autocomplete'  => 'off',
                                'autofocus'     => 'on', 
                                'required'      => 'on'
                            );
                            echo form_input($data);
                            echo form_error();
                            ?>
                        </div>

                        <div class="row, col-md-18">
                            <?php
                            $data = array(
                                'name'          => 'submit',
                                'id'            => 'submit',
                                'value'         => 'Login',
                                'type'          => 'submit',
                                'class'       => 'form-control btn btn-primary'
                            );

                            echo form_submit($data);
                            ?>
                        </div>
                    </br>
                    <div class="row"  align="center">
                        Copyright &#169; Kantor Perwakilan Bank Indonesia Solo 2020
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

</div>

<!-- JS -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/jquery-1.12.4.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>