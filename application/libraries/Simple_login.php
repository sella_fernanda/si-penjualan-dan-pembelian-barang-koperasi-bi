<?php if(! defined('BASEPATH')) exit('Akses langsung tidak diperbolehkan'); 

class Simple_login {
	// SET SUPER GLOBAL
	var $CI = NULL;
	public function __construct() {
		$this->CI =& get_instance();
	}
	// Fungsi login
	public function login($id_peran,$username, $password) {
		$query = $this->CI->db->get_where('akun',array('id_peran'=>$id_peran));
		$query2 = $this->CI->db->get_where('akun',array('username'=>$username));
		$query3 = $this->CI->db->get_where('akun',array('password'=>$password));

		if($query->num_rows() < 1 ) {
			$this->CI->session->set_flashdata('sukses','Anda Tidak Terdaftar Pada Peran Tersebut!!');
			redirect(base_url('/index.php/C_login'));			
		}else
		if($query2->row()->username != $username) {
			$this->CI->session->set_flashdata('sukses','Username Salah!');
			redirect(base_url('/index.php/C_login'));
		}else
		if($query3->row()->password != $password) {
			$this->CI->session->set_flashdata('sukses','Password Salah!');
			redirect(base_url('/index.php/C_login'));
		}else
		if($id_peran == 'P001' )
		{
			$row = $this->CI->db->query('SELECT * FROM akun join pegawai on pegawai.nip=akun.nip where id_peran = "'.$id_peran.'" AND username="'.$username.'" AND password="'.$password.'" ');
			$id = $row->row()->id_akun;
			$peran = $row->row()->id_peran;
			$username = $row->row()->username;
			$nip = $row->row()->nip;
			$nama = $row->row()->nama;
			$this->CI->session->set_userdata('nama', $nama);
			$this->CI->session->set_userdata('username', $username);
			$this->CI->session->set_userdata('nip', $nip);
			$this->CI->session->set_userdata('id_peran', $peran);
			$this->CI->session->set_userdata('id_akun', $id);
			
			redirect(base_url('/admin'));
		}else
		if($id_peran == 'P002' )
		{
			$row = $this->CI->db->query('SELECT * FROM akun join pegawai on pegawai.nip=akun.nip where id_peran = "'.$id_peran.'" AND username="'.$username.'" AND password="'.$password.'" ');
			$id = $row->row()->id_akun;
			$peran = $row->row()->id_peran;
			$username = $row->row()->username;
			$nip = $row->row()->nip;
			$nama = $row->row()->nama;
			$this->CI->session->set_userdata('nama', $nama);
			$this->CI->session->set_userdata('username', $username);
			$this->CI->session->set_userdata('nip', $nip);
			$this->CI->session->set_userdata('id_peran', $peran);
			$this->CI->session->set_userdata('id_akun', $id);
			
			redirect(base_url('/teller'));
		}else
		if($id_peran == 'P003' )
		{
			$row = $this->CI->db->query('SELECT * FROM akun join pegawai on pegawai.nip=akun.nip where id_peran = "'.$id_peran.'" AND username="'.$username.'" AND password="'.$password.'" ');
			$id = $row->row()->id_akun;
			$peran = $row->row()->id_peran;
			$username = $row->row()->username;
			$nip = $row->row()->nip;
			$nama = $row->row()->nama;
			$this->CI->session->set_userdata('nama', $nama);
			$this->CI->session->set_userdata('username', $username);
			$this->CI->session->set_userdata('nip', $nip);
			$this->CI->session->set_userdata('id_peran', $peran);
			$this->CI->session->set_userdata('id_akun', $id);
			
			redirect(base_url('/stoker'));
		}else
		if($id_peran == 'P004' )
		{
			$row = $this->CI->db->query('SELECT * FROM akun join pegawai on pegawai.nip=akun.nip where id_peran = "'.$id_peran.'" AND username="'.$username.'" AND password="'.$password.'" ');
			$id = $row->row()->id_akun;
			$peran = $row->row()->id_peran;
			$username = $row->row()->username;
			$nip = $row->row()->nip;
			$nama = $row->row()->nama;
			$this->CI->session->set_userdata('nama', $nama);
			$this->CI->session->set_userdata('username', $username);
			$this->CI->session->set_userdata('nip', $nip);
			$this->CI->session->set_userdata('id_peran', $peran);
			$this->CI->session->set_userdata('id_akun', $id);
			
			redirect(base_url('/viewer'));
		}else
		if($id_peran == 'P005' )
		{
			$row = $this->CI->db->query('SELECT * FROM akun join pegawai on pegawai.nip=akun.nip where id_peran = "'.$id_peran.'" AND username="'.$username.'" AND password="'.$password.'" ');
			$id = $row->row()->id_akun;
			$peran = $row->row()->id_peran;
			$username = $row->row()->username;
			$nip = $row->row()->nip;
			$nama = $row->row()->nama;
			$this->CI->session->set_userdata('nama', $nama);
			$this->CI->session->set_userdata('username', $username);
			$this->CI->session->set_userdata('nip', $nip);
			$this->CI->session->set_userdata('id_peran', $peran);
			$this->CI->session->set_userdata('id_akun', $id);
			
			redirect(base_url('/anggota'));
		}
		return false;
	}

	// Proteksi halaman
	public function cek_login() {
		if($this->CI->session->userdata('username') == '' OR $this->CI->session->userdata('id_peran') != $query ) {
			$this->CI->session->set_flashdata('sukses','Anda belum login');
			redirect(base_url('index.php/C_login'));
		}
	}

	// Fungsi logout
	public function logout() {
		$this->CI->session->unset_userdata('nip');
		$this->CI->session->unset_userdata('nama');
		$this->CI->session->unset_userdata('username');
		$this->CI->session->unset_userdata('id_peran');
		$this->CI->session->unset_userdata('id_akun');
		redirect(base_url());
	}
}