<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
	}

	public function index() {
		$valid = $this->form_validation;
		$id_peran = $this->input->post('id_peran');
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$valid->set_rules('id_peran','id_peran');
		$valid->set_rules('username','Username','required');
		$valid->set_rules('password','Password','required');
		$valid->set_message('required','Lengkapi Form %s! ');
		if($valid->run()) {
			$this->simple_login->login($id_peran,$username,$password);
		}
		$data['id_peran']=$this->M_login->getPeran();
		
		$this->load->view('login',$data);
	}
	
	public function logout() {
		$this->simple_login->logout();	
	}
}