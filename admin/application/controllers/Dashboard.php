<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Barang');
	}

	public function index()
	{
     $this->template->set('title','HOME');
     $this->template->load('adminLTE2','contents','v_index2');
	}

	public function Toko()
	{	
		$queryHarga  = $this->M_Barang->get_harga();
		$data['jml_harga']   = count($queryHarga);
		$this->template->set('title', 'Dashboard');
		$this->template->load('adminLTE', 'contents' , 'v_index',$data);
	}

}

