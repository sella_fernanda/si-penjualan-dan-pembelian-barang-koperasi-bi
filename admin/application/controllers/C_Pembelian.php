<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Pembelian extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Pembelian');
	}

    public function laporan_pembelian()
    {       
    	$temp_tgl_mulai = date_create($this->input->get('tgl_mulai'));
        $tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
        $temp_tgl_selesai = date_create($this->input->get('tgl_selesai'));
        $tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

        // membedakan sebelum dan sesudah isi filter data
        if ($this->input->get('tgl_mulai') == '' && $this->input->get('tgl_selesai') == '')
        {
            // kondisi ketika pertama buka, belum filter data kosong
        	$data['tgl_mulai']=null;
            $data['tgl_selesai']=null;
            $data['laporan'] = null;
        }else
        {
            // jika sudah ngisi form filter
        	$data['tgl_mulai']=$tgl_mulai;
            $data['tgl_selesai']=$tgl_selesai;

            $data['laporan']=$this->M_Pembelian->get_laporan_pembelian($data['tgl_mulai'], $data['tgl_selesai']);
        }
        $this->template->set('title','Laporan Pembelian');
        $this->template->load('adminLTE', 'contents' , 'view_pembelian/laporan_pembelian', $data);
    }

    public function cetak_laporan_word($tgl_mulai, $tgl_selesai)
    {
    	header("Content-type: application/vnd.ms-word");
    	header("Content-Disposition: attachment;Filename=lap_pembelian__".$tgl_mulai."-".$tgl_selesai.".doc");

    	$temp_tgl_mulai = date_create($tgl_mulai);
    	$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
    	$temp_tgl_selesai = date_create($tgl_selesai);
    	$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

    	$data = array(
    		'tgl_mulai' => $tgl_mulai,
    		'tgl_selesai' => $tgl_selesai,
    		'laporan'=>$this->M_Pembelian->get_laporan_pembelian($tgl_mulai, $tgl_selesai),
            'start' => 0
        );

    	$this->load->view('view_pembelian/cetak_laporan_pembelian',$data);
    }

    public function cetak_laporan_pdf($tgl_mulai, $tgl_selesai)
    {
    	$temp_tgl_mulai = date_create($tgl_mulai);
    	$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
    	$temp_tgl_selesai = date_create($tgl_selesai);
    	$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');
    	$data = array(
    		'tgl_mulai' => $tgl_mulai,
    		'tgl_selesai' => $tgl_selesai,
    		'laporan'=>$this->M_Pembelian->get_laporan_pembelian($tgl_mulai, $tgl_selesai),
            'start' => 0
        );

    	$this->load->view('view_pembelian/cetak_laporan_pembelian',$data);
    }

    public function cetak_laporan_excel($tgl_mulai, $tgl_selesai)
    {
    	header("Content-type: application/vnd.ms.excel");

    	header("Content-Disposition: attachment;Filename=laporan_pembelian_".$tgl_mulai."-".$tgl_selesai.".xls");

    	$temp_tgl_mulai = date_create($tgl_mulai);
    	$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
    	$temp_tgl_selesai = date_create($tgl_selesai);
    	$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

    	$data = array(
    		'tgl_mulai' => $tgl_mulai,
    		'tgl_selesai' => $tgl_selesai,
    		'laporan'=>$this->M_Pembelian->get_laporan_pembelian($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
    	$this->load->view('view_pembelian/cetak_laporan_pembelian',$data);
    }

    public function laporan_hutang()
    {      
        $temp_tgl_mulai = date_create($this->input->get('tgl_mulai'));
        $tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
        $temp_tgl_selesai = date_create($this->input->get('tgl_selesai'));
        $tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

        // membedakan sebelum dan sesudah isi filter data
        if ($this->input->get('tgl_mulai') == '' && $this->input->get('tgl_selesai') == '') 
        {
            // kondisi ketika pertama buka, belum filter data kosong
            $data['tgl_mulai']=null;
            $data['tgl_selesai']=null;
            $data['hutang'] = [];
        }else
        {
            // jika sudah ngisi form filter
            $data['tgl_mulai']=$tgl_mulai;
            $data['tgl_selesai']=$tgl_selesai;

            $data['hutang']=$this->M_Pembelian->get_laporan_hutang($tgl_mulai, $tgl_selesai);
        }
        $this->template->set('title','Laporan hutang');
        $this->template->load('adminLTE', 'contents' , 'view_pembelian/laporan_hutang', $data);
    }

    public function cetak_laporan_hutang_word($tgl_mulai, $tgl_selesai)
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=laporan_hutang_".$tgl_mulai."-".$tgl_selesai.".doc");

        $temp_tgl_mulai = date_create($tgl_mulai);
    	$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
    	$temp_tgl_selesai = date_create($tgl_selesai);
    	$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'hutang' => $this->M_Pembelian->get_laporan_hutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        
        $this->load->view('view_pembelian/cetak_laporan_hutang',$data);
    }

    public function cetak_laporan_hutang_pdf($tgl_mulai, $tgl_selesai)
    {
    	$temp_tgl_mulai = date_create($tgl_mulai);
    	$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
    	$temp_tgl_selesai = date_create($tgl_selesai);
    	$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'hutang' => $this->M_Pembelian->get_laporan_hutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        
        $this->load->view('view_pembelian/cetak_laporan_hutang',$data);
    }

    public function cetak_laporan_hutang_excel($tgl_mulai, $tgl_selesai)
    {
        header("Content-type: application/vnd.ms.excel");

        header("Content-Disposition: attachment;Filename=laporan_hutang_".$tgl_mulai."-".$tgl_selesai.".xls");

        $temp_tgl_mulai = date_create($tgl_mulai);
    	$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
    	$temp_tgl_selesai = date_create($tgl_selesai);
    	$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'hutang' => $this->M_Pembelian->get_laporan_hutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        $this->load->view('view_pembelian/cetak_laporan_hutang',$data);
    }

}
