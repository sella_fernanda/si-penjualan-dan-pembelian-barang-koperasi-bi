<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Harga extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Harga');
	}

	public function list_harga()
	{
		$data['harga'] = $this->M_Harga->get_harga();

		$this->template->set('title', 'Management Data Harga');
		$this->template->load('adminLTE', 'contents' , 'view_harga/list_harga', $data);
	}

	public function edit_harga($no_barcode)
	{
		$this->form_validation->set_rules('id_harga','ID Harga','trim|required');
		$this->form_validation->set_rules('harga_jual','Harga Jual','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		if($this->form_validation->run() == TRUE) {
			//jika menyimpan
			$data['harga_jual'] = $this->input->post('harga_jual');

			$this->M_Harga->update_harga($no_barcode,$data);

			redirect(site_url('admin/C_Harga/list_harga'));

		}else{
			//jika tidak menyimpan
			$harga = $this->M_Harga->get_harga_by($no_barcode);
			$avg_HPP = $this->M_Harga->get_avg_hpp($no_barcode); 

			$data['id_harga'] = $harga['id_harga'];
			$data['harga_jual'] = $harga['harga_jual'];
			$data['no_barcode'] = $harga['no_barcode'];
			$data['nama_barang'] = $harga['nama_barang'];
			$data['rataHPP'] = $avg_HPP;

			//menghitung margin/keuntungan
			if ($avg_HPP!=0) {
				$data['margin'] = ($data['harga_jual']-$avg_HPP)/$avg_HPP*100;
			}else{
				$data['margin'] = "0";
			}
			$data['rataHPP'] = $avg_HPP;


			$this->template->set('title', 'Edit Data Harga');
            $this->template->load('adminLTE', 'contents' , 'view_harga/edit_harga', $data);
		}
	}
}
