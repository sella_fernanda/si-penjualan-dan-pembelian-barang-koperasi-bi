<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Stok extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Stok');
	}
	
	public function laporan_stok_opname()
    {       
        $tgl_mulai = $this->input->get('tgl_mulai');
        $tgl_selesai = $this->input->get('tgl_selesai');

        $data['laporan']=$this->M_Stok->get_laporan_stok($tgl_mulai, $tgl_selesai);

        // membedakan sebelum dan sesudah isi filter data
        if ($tgl_mulai == '' && $tgl_selesai == '') 
        {
            // kondisi ketika pertama buka, belum filter data kosong
            $data['tgl_mulai']='';
            $data['tgl_selesai']='';
        }else
        {
            // jika sudah ngisi form filter
            $data['tgl_mulai']=$tgl_mulai;
            $data['tgl_selesai']=$tgl_selesai;
            $data['laporan']=$this->M_Stok->get_laporan_stok($tgl_mulai, $tgl_selesai);
        }
        $this->template->set('title','Laporan Stok');
        $this->template->load('adminLTE', 'contents' , 'view_stok/laporan_stok_opname', $data);
    }

    public function cetak_laporan_word($tgl_mulai, $tgl_selesai)
    {
    	header("Content-type: application/vnd.ms-word");
    	header("Content-Disposition: attachment;Filename=lap_stok_opname_".$tgl_mulai."-".$tgl_selesai.".doc");

    	$data = array(
    		'tgl_mulai' => $tgl_mulai,
    		'tgl_selesai' => $tgl_selesai,
    		'laporan'=>$this->M_Stok->get_laporan_stok($tgl_mulai, $tgl_selesai),
            'start' => 0
        );

    	$this->load->view('view_stok/cetak_laporan_stok_opname',$data);
    }

    public function cetak_laporan_pdf($tgl_mulai, $tgl_selesai)
    {
    	$data = array(
    		'tgl_mulai' => $tgl_mulai,
    		'tgl_selesai' => $tgl_selesai,
    		'laporan'=>$this->M_Stok->get_laporan_stok($tgl_mulai, $tgl_selesai),
            'start' => 0
        );

    	$this->load->view('view_stok/cetak_laporan_stok_opname',$data);
    }

    public function cetak_laporan_excel($tgl_mulai, $tgl_selesai)
    {
    	header("Content-type: application/vnd.ms.excel");

    	header("Content-Disposition: attachment;Filename=lap_stok_opname_".$tgl_mulai."-".$tgl_selesai.".xls");

    	$data = array(
    		'tgl_mulai' => $tgl_mulai,
    		'tgl_selesai' => $tgl_selesai,
    		'laporan'=>$this->M_Stok->get_laporan_stok($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
    	$this->load->view('view_stok/cetak_laporan_stok_opname',$data);
    }
}

