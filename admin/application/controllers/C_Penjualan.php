<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Penjualan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Penjualan');
    }

    public function laporan_penjualan_on()
    {		
        $tgl_mulai = $this->input->get('tgl_mulai');
        $tgl_selesai = $this->input->get('tgl_selesai');

        $data['laporan']=$this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai);

        $data['jumlah']=$this->M_Penjualan->get_jumlah($tgl_mulai, $tgl_selesai);
        $data['jumlah_tunai']=$this->M_Penjualan->get_jumlah_tunai($tgl_mulai, $tgl_selesai);
        $data['jumlah_piutang']=$this->M_Penjualan->get_jumlah_piutang($tgl_mulai, $tgl_selesai);
		// membedakan sebelum dan sesudah isi filter data
        if ($tgl_mulai == '' && $tgl_selesai == '') 
        {
			// kondisi ketika pertama buka, belum filter data kosong
            $data['tgl_mulai']='';
            $data['tgl_selesai']='';
        }else
        {
			// jika sudah ngisi form filter
            $data['tgl_mulai']=$tgl_mulai;
            $data['tgl_selesai']=$tgl_selesai;
            $data['laporan']=$this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai);
        }
        $this->template->set('title','Laporan Penjualan Online');
        $this->template->load('adminLTE', 'contents' , 'view_penjualan/laporan_penjualan_online', $data);
    }

    public function cetak_laporan_on_word($tgl_mulai, $tgl_selesai)
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=laporan_penj_on_".$tgl_mulai."-".$tgl_selesai.".doc");

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'laporan'=>$this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai),
            'jumlah'=> $this->M_Penjualan->get_jumlah($tgl_mulai, $tgl_selesai),
            'jumlah_tunai'=> $this->M_Penjualan->get_jumlah_tunai($tgl_mulai, $tgl_selesai),
            'jumlah_piutang'=> $this->M_Penjualan->get_jumlah_piutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );

        $this->load->view('view_penjualan/cetak_laporan_penjualan_online',$data);
    }

    public function cetak_laporan_on_excel($tgl_mulai, $tgl_selesai)
    {
        $this->load->helper('exportexcel');
        $namaFile = "lap_penj_on". "_" . "$tgl_mulai-$tgl_selesai" .".xls";
        $judul = "Laporan Penjualan Online";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "No Transaksi");
        xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Transaksi");
        xlsWriteLabel($tablehead, $kolomhead++, "Pegawai");
        xlsWriteLabel($tablehead, $kolomhead++, "Pembayaran");
        xlsWriteLabel($tablehead, $kolomhead++, "Teller");
        xlsWriteLabel($tablehead, $kolomhead++, "Total Bayar");

        foreach ($this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai) as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->no_transaksi);
            xlsWriteLabel($tablebody, $kolombody++, $data->tgl_transaksi);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_pegawai);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_pembayaran);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_teller);
            xlsWriteNumber($tablebody, $kolombody++, $data->total_bayar);

            $tablebody++;
            $nourut++;
        }

        foreach ($this->M_Penjualan->get_jumlah($tgl_mulai, $tgl_selesai) as $data1) 
        {
            $kolombody = 5;
            xlsWriteLabel($tablebody, $kolombody++, "Total");
            xlsWriteNumber($tablebody, $kolombody++, $data1->total_bayar);
        }

        foreach ($this->M_Penjualan->get_jumlah_tunai($tgl_mulai, $tgl_selesai) as $data2) 
        {
            xlsWriteLabel($tablebody, $kolombody++, "Total Tunai");
            xlsWriteNumber($tablebody, $kolombody++, $data2->total_bayar);
        }

        foreach ($this->M_Penjualan->get_jumlah_piutang($tgl_mulai, $tgl_selesai) as $data3)
        {
            xlsWriteLabel($tablebody, $kolombody++, "Total Piutang");
            xlsWriteNumber($tablebody, $kolombody++, $data3->total_bayar);
        }

        xlsEOF();
        exit();
    }

    public function cetak_laporan_on_pdf($tgl_mulai, $tgl_selesai)
    {
        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'laporan'=>$this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai),
            'jumlah'=> $this->M_Penjualan->get_jumlah($tgl_mulai, $tgl_selesai),
            'jumlah_tunai'=> $this->M_Penjualan->get_jumlah_tunai($tgl_mulai, $tgl_selesai),
            'jumlah_piutang'=> $this->M_Penjualan->get_jumlah_piutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );

        $this->load->view('view_penjualan/cetak_laporan_penjualan_online',$data);
    }

    public function laporan_penjualan_off()
    {       
        $tgl_mulai = $this->input->get('tgl_mulai');
        $tgl_selesai = $this->input->get('tgl_selesai');

        $data['laporan']=$this->M_Penjualan->get_laporan_penjualan_off($tgl_mulai, $tgl_selesai);

        $data['jumlah']=$this->M_Penjualan->get_jumlah_off($tgl_mulai, $tgl_selesai);
        $data['jumlah_tunai']=$this->M_Penjualan->get_jumlah_tunai_off($tgl_mulai, $tgl_selesai);
        $data['jumlah_piutang']=$this->M_Penjualan->get_jumlah_piutang_off($tgl_mulai, $tgl_selesai);
        // membedakan sebelum dan sesudah isi filter data
        if ($tgl_mulai == '' && $tgl_selesai == '') 
        {
            // kondisi ketika pertama buka, belum filter data kosong
            $data['tgl_mulai']='';
            $data['tgl_selesai']='';
        }else
        {
            // jika sudah ngisi form filter
            $data['tgl_mulai']=$tgl_mulai;
            $data['tgl_selesai']=$tgl_selesai;
            $data['laporan']=$this->M_Penjualan->get_laporan_penjualan_off($tgl_mulai, $tgl_selesai);
        }
        $this->template->set('title','Laporan Penjualan Offline');
        $this->template->load('adminLTE', 'contents' , 'view_penjualan/laporan_penjualan_offline', $data);
    }

    public function cetak_laporan_off_word($tgl_mulai, $tgl_selesai)
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=lap_penj_off__".$tgl_mulai."-".$tgl_selesai.".doc");

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'laporan'=>$this->M_Penjualan->get_laporan_penjualan_off($tgl_mulai, $tgl_selesai),
            'jumlah'=> $this->M_Penjualan->get_jumlah_off($tgl_mulai, $tgl_selesai),
            'jumlah_tunai'=> $this->M_Penjualan->get_jumlah_tunai_off($tgl_mulai, $tgl_selesai),
            'jumlah_piutang'=> $this->M_Penjualan->get_jumlah_piutang_off($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        
        $this->load->view('view_penjualan/cetak_laporan_penjualan_offline',$data);
    }

    public function cetak_laporan_off_excel($tgl_mulai, $tgl_selesai)
    {
        $this->load->helper('exportexcel');
        $namaFile = "lap_penj_off". "_" . "$tgl_mulai-$tgl_selesai" .".xls";
        $judul = "Laporan Penjualan Offline";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "No Transaksi");
        xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Transaksi");
        xlsWriteLabel($tablehead, $kolomhead++, "Pegawai");
        xlsWriteLabel($tablehead, $kolomhead++, "Pembayaran");
        xlsWriteLabel($tablehead, $kolomhead++, "Teller");
        xlsWriteLabel($tablehead, $kolomhead++, "Total Bayar");

        foreach ($this->M_Penjualan->get_laporan_penjualan_off($tgl_mulai, $tgl_selesai) as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->no_transaksi);
            xlsWriteLabel($tablebody, $kolombody++, $data->tgl_transaksi);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_pegawai);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_pembayaran);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_teller);
            xlsWriteNumber($tablebody, $kolombody++, $data->total_bayar);

            $tablebody++;
            $nourut++;
        }

        foreach ($this->M_Penjualan->get_jumlah_off($tgl_mulai, $tgl_selesai) as $data1) 
        {
            $kolombody = 5;
            xlsWriteLabel($tablebody, $kolombody++, "Total");
            xlsWriteNumber($tablebody, $kolombody++, $data1->total_bayar);
        }

        foreach ($this->M_Penjualan->get_jumlah_tunai_off($tgl_mulai, $tgl_selesai) as $data2) {
            xlsWriteLabel($tablebody, $kolombody++, "Total Tunai");
            xlsWriteNumber($tablebody, $kolombody++, $data2->total_bayar);
        }

        foreach ($this->M_Penjualan->get_jumlah_piutang_off($tgl_mulai, $tgl_selesai) as $data3) {
            xlsWriteLabel($tablebody, $kolombody++, "Total Piutang");
            xlsWriteNumber($tablebody, $kolombody++, $data3->total_bayar);
        }

        xlsEOF();
        exit();
    }

    public function cetak_laporan_off_pdf($tgl_mulai, $tgl_selesai)
    {
        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'laporan'=>$this->M_Penjualan->get_laporan_penjualan_off($tgl_mulai, $tgl_selesai),
            'jumlah'=> $this->M_Penjualan->get_jumlah_off($tgl_mulai, $tgl_selesai),
            'jumlah_tunai'=> $this->M_Penjualan->get_jumlah_tunai_off($tgl_mulai, $tgl_selesai),
            'jumlah_piutang'=> $this->M_Penjualan->get_jumlah_piutang_off($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        
        $this->load->view('view_penjualan/cetak_laporan_penjualan_offline',$data);
    }

    public function laporan_piutang()
    {       
        $tgl_mulai = $this->input->get('tgl_mulai');
        $tgl_selesai = $this->input->get('tgl_selesai');

        $data['piutang']=$this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai);
        $data['jumlah']=$this->M_Penjualan->get_piutang($tgl_mulai, $tgl_selesai);

        // membedakan sebelum dan sesudah isi filter data
        if ($tgl_mulai == '' && $tgl_selesai == '') 
        {
            // kondisi ketika pertama buka, belum filter data kosong
            $data['tgl_mulai']='';
            $data['tgl_selesai']='';
        }else
        {
            // jika sudah ngisi form filter
            $data['tgl_mulai']=$tgl_mulai;
            $data['tgl_selesai']=$tgl_selesai;
            $data['piutang']=$this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai);
        }
        $this->template->set('title','Laporan Piutang');
        $this->template->load('adminLTE', 'contents' , 'view_penjualan/laporan_piutang', $data);
    }

    public function cetak_laporan_piutang_word($tgl_mulai, $tgl_selesai)
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=laporan_piutang_".$tgl_mulai."-".$tgl_selesai.".doc");

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'piutang' => $this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai),
            'jumlah' => $this->M_Penjualan->get_piutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        
        $this->load->view('view_penjualan/cetak_laporan_piutang',$data);
    }

    public function cetak_laporan_piutang_pdf($tgl_mulai, $tgl_selesai)
    {
        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'piutang' => $this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai),
            'jumlah' => $this->M_Penjualan->get_piutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        
        $this->load->view('view_penjualan/cetak_laporan_piutang',$data);
    }

    public function excel($tgl_mulai, $tgl_selesai)
    {
        header("Content-type: application/vnd.ms.excel");

        header("Content-Disposition: attachment;Filename=laporan_piutang_".$tgl_mulai."-".$tgl_selesai.".xls");

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'piutang' => $this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai),
            'jumlah' => $this->M_Penjualan->get_piutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        $this->load->view('view_penjualan/cetak_laporan_piutang',$data);
    }

    
}