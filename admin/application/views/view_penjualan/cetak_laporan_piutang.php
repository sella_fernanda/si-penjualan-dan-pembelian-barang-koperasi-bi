<!doctype html>
<html>
<head>
    <title>Laporan Piutang</title>
    <style>
        .word-table {
            border:1px solid black !important; 
            border-collapse: collapse !important;
            width: 100%;
        }
        .word-table tr th, .word-table tr td{
            border:1px solid black !important; 
            padding: 5px 10px;
        }
        .word-table tr th {
            text-align: center;
        }
    </style>
</head> 
<body>
    <br><br>
    <p style="font-family: Arial; font-size: 14;" align="center"><b>KOPERASI PEGAWAI BANK INDONESIA</b></p>
    <p style="font-family: Arial; font-size: 14;" align="center"><b>LAPORAN PIUTANG</b></p> 
    <p style="font-family: Arial; font-size: 14;" align="center"><b>Periode <?=date('d F Y', strtotime($tgl_mulai));?> - <?=date('d F Y', strtotime($tgl_selesai));?></b></p> <br>
    <table class="word-table" style="margin-bottom: 10px">
        <thead>
            <tr>
                <th>No</th>
                <th>Pegawai</th>
                <th>No Transaksi</th>
                <th>Tanggal Transaksi</th>
                <th>Keterangan</th>
                <th>Piutang</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $start = 0;
            $total_piutang = 0;
            $row = $piutang;
            for ($i = 0; $i < count($piutang); $i++) { ?> <!-- index buat nyari nip selanjutnya $i = index -->
            <?php $tempNip = $row[$i]->nip; ?> <!--variabel current nip.nyocokin nip sekarang sm nip berikutnya bikin dummy variabel-->
            <tr>
                <td align="center"><?php echo ++$start ?></td>
                <td align="center"><?php echo $row[$i]->nama; ?></td>
                <td align="center"><?php echo $row[$i]->no_transaksi; ?></td>
                <td align="center"><?php echo date('d F Y h:i:s', strtotime($row[$i]->tgl_transaksi)); ?></td>
                <td align="center"><?php echo $row[$i]->keterangan; ?></td>
                <td align="center"><?php echo 'Rp. '.number_format($row[$i]->total_bayar); ?></td>
            </tr>
            <?php $nextNip = isset($row[$i+1]->nip) ? $row[$i+1]->nip : 0 ?>
            <!-- nyari nip selanjutnya kalo ga ada di isi 0-->
            <?php $total_piutang += $row[$i]->total_bayar; ?>
            <!-- ngehitung total bayar per nip -->
            <?php if($tempNip != $nextNip): ?> <!-- nyocokin kalo nip current beda sama nip berikutnya maka ditampilkan totalnya -->
            <tr> 
                <td colspan="5" align="right"><b>Total</b></td>
                <td align="center"><b><?php echo 'Rp. '.number_format($total_piutang) ?></b></td>
            </tr>
            <?php $total_piutang = 0; ?> <!-- reset total piutang biar ga kejumlah sama piutang selanjutnya -->
        <?php endif; ?>
    <?php } ?>
</tbody>
<tfoot>
    <tr> 
        <?php foreach ($jumlah as $value) { ?>
            <td colspan="5" align="right"><b>Jumlah Total</b></td>
            <td align="center"><b><?php echo 'Rp. '.number_format($value->total_bayar); ?></b></td>
        <?php } ?>
    </tr>
</tfoot>
</table>
<script>
    window.print();
</script>
</body>
</html>