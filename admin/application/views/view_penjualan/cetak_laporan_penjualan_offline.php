<!doctype html>
<html>
<head>
    <title>Laporan Penjualan Offline</title>
    <style>
        .word-table {
            border:1px solid black !important; 
            border-collapse: collapse !important;
            width: 100%;
        }
        .word-table tr th, .word-table tr td{
            border:1px solid black !important; 
            padding: 5px 10px;
        }
        .word-table tr th {
            text-align: center;
        }
    </style>
</head> 
<body>
    <br><br>
    <p style="font-family: Arial; font-size: 14;" align="center"><b>KOPERASI PEGAWAI BANK INDONESIA</b></p>
    <p style="font-family: Arial; font-size: 14;" align="center"><b>LAPORAN PENJUALAN OFFLINE</b></p> 
    <p style="font-family: Arial; font-size: 14;" align="center"><b>Periode <?=date('d F Y', strtotime($tgl_mulai));?> - <?=date('d F Y', strtotime($tgl_selesai));?></b></p> <br>
    <table class="word-table" style="margin-bottom: 10px">
        <thead>
            <tr>
                <th>No</th>
                <th>No Transaksi</th>
                <th>Tanggal Transaksi</th>
                <th>Pegawai</th>
                <th>Pembayaran</th>
                <th>Teller</th>
                <th>Total Bayar</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $start = 0;
            foreach ($laporan as $row) { ?>
                <tr>
                    <td align="center"><?php echo ++$start ?></td>
                    <td align="center"><?php echo $row->no_transaksi; ?></td>
                    <td align="center"><?php echo date('d F Y h:i:s', strtotime($row->tgl_transaksi)); ?></td>
                    <td align="center"><?= $row->nip_pembeli== null ? 'Umum' :$row->nama_pegawai?>
                    <td align="center"><?php echo $row->nama_pembayaran; ?></td>
                    <td align="center"><?php echo $row->teller; ?></td>
                    <td align="center"><?php echo 'Rp. '.number_format($row->total_bayar); ?></td>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr> 
                <?php foreach ($jumlah as $value) { ?>
                    <td colspan="6" align="right"><b>Total</b></td>
                    <td align="center"><b><?php echo 'Rp. '.number_format($value->total_bayar); ?></b></td>
                <?php } ?>
            </tr>
            <tr> 
                <?php foreach ($jumlah_tunai as $value) { ?>
                    <td colspan="2" align="left"><b>Total Pembayaran Tunai</b></td>
                    <td align="center"><b><?php echo 'Rp. '.number_format($value->total_bayar); ?></b></td>
                <?php } ?>
            </tr>
            <tr> 
                <?php foreach ($jumlah_piutang as $value) { ?>
                    <td colspan="2" align="left"><b>Total Pembayaran Piutang</b></td>
                    <td align="center"><b><?php echo 'Rp. '.number_format($value->total_bayar); ?></b></td>
                <?php } ?>
            </tr>
        </tfoot>
    </table>
    <script>
        window.print();
    </script>
</body>
</html>