<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <br>
        <center>
          <img src="<?php echo base_url() ?>assets/img/BI.png" alt="logo bank indonesia" width="500" height="200">
        </center> 
        <br> <br>
      </div>
    </div>   
  </div>
</div>

<section class="content">
  <div class="col-sm-4"></div>
   <div class="row">
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-aqua">
        <div class="inner">
          <p>Draft Harga</p>

          <h3><?php echo $jml_harga ?></h3>

        </div>
        <div class="icon">
          <i class="fa fa-money"></i>
        </div>
        <a href="<?php echo site_url('admin/C_Harga/list_harga'); ?>" class="small-box-footer">
          Detail <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>
    <!-- ./col -->
  </div>
</section>