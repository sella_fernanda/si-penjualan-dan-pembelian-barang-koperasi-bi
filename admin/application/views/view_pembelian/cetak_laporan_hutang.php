<!doctype html>
<html>
<head>
    <title>Laporan Pembelian</title>
    <style>
        .word-table {
            border:1px solid black !important; 
            border-collapse: collapse !important;
            width: 100%;
        }
        .word-table tr th, .word-table tr td{
            border:1px solid black !important; 
            padding: 5px 10px;
            font-size: 10px;
        }
        .word-table tr th {
            text-align: center;
        }
    </style>
</head> 
<body>
    <br><br>
    <p style="font-family: Arial; font-size: 14;" align="center"><b>KOPERASI PEGAWAI BANK INDONESIA</b></p>
    <p style="font-family: Arial; font-size: 14;" align="center"><b>LAPORAN HUTANG</b></p> 
    <p style="font-family: Arial; font-size: 14;" align="center"><b>Periode <?=date('d F Y', strtotime($tgl_mulai));?> - <?=date('d F Y', strtotime($tgl_selesai));?></b></p> <br>
    <table class="word-table" style="margin-bottom: 10px">
        <thead>
            <tr>
                <th>No</th>
                <th>No Pembelian</th>
                <th>Suplier</th>
                <th>Tanggal</th>
                <th>Pembayaran</th>
                <th>Stoker</th>
                <th>Total Beli</th>
                <th>Transportasi</th>
                <th>Angkut</th>
                <th>Biaya Lain</th>
                <th>Total Bayar</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $start = 0;
            $total_hutang = 0;
            $row = $hutang;
            for ($i = 0; $i < count($hutang); $i++) { ?> <!-- index buat nyari id selanjutnya $i = index -->
            <?php $tempSup = $row[$i]->id_suplier; ?> <!--variabel current id. nyocokin id sekarang sm id berikutnya bikin dummy variabel-->
            <tr>
                <td align="center"><?php echo ++$start ?></td>
                <td align="center"><?php echo $row[$i]->no_pembelian; ?></td>
                <td align="center"><?php echo $row[$i]->nama_suplier; ?></td>
                <td align="center"><?php echo date('d F Y', strtotime($row[$i]->tgl_suply)); ?></td>
                <td align="center"><?php echo $row[$i]->nama_pembayaran; ?></td>
                <td align="center"><?php echo $row[$i]->nama; ?></td>
                <td align="center"><?php 
                $detailPembelian = $this->M_Pembelian->get_detail_pembelian($row[$i]->no_pembelian);
                $totalBeli = 0;
                foreach($detailPembelian as $key => $val){
                    $beli = $val->harga_beli * $val->jumlah_beli;
                    $totalBeli += $beli;
                }
                echo 'Rp. '.number_format($totalBeli);
                ?></td>
                <td align="center"><?php echo 'Rp. '.number_format($row[$i]->transportasi); ?></td>
                <td align="center"><?php echo 'Rp. '.number_format($row[$i]->angkut); ?></td>
                <td align="center"><?php echo 'Rp. '.number_format($row[$i]->biaya_lain); ?></td>
                <td align="center"><?php echo 'Rp. '.number_format($totalBeli + $row[$i]->transportasi + $row[$i]->angkut + $row[$i]->biaya_lain)?></td>
            </tr>
            <?php $nextSup = isset($row[$i+1]->id_suplier) ? $row[$i+1]->id_suplier : 1 ?>
            <!-- nyari id_suplier selanjutnya kalo ga ada di isi 0-->
            <?php $total_hutang += ($totalBeli + $row[$i]->transportasi + $row[$i]->angkut + $row[$i]->biaya_lain); ?>
            <!-- ngehitung total bayar per id_suplier -->
            <?php if($tempSup != $nextSup): ?> <!-- nyocokin kalo id_suplier current beda sama id_suplier berikutnya maka ditampilkan totalnya -->
            <tr> 
                <td colspan="10" align="right"><b>Total</b></td>
                <td align="center"><b><?php echo 'Rp. '.number_format($total_hutang) ?></b></td>
            </tr>
            <?php $total_hutang = 0; ?> <!-- reset total hutang biar ga kejumlah sama hutang selanjutnya -->
        <?php endif; ?>
    <?php } ?>
</tbody>
<tfoot>
    <?php
    $total_hutang = 0;
    $row = $hutang;
    for ($i = 0; $i < count($hutang); $i++) {
        $detailPembelian = $this->M_Pembelian->get_detail_pembelian($row[$i]->no_pembelian);
        $totalBeli = 0;
        foreach($detailPembelian as $key => $val){
            $beli = $val->harga_beli * $val->jumlah_beli;
            $totalBeli += $beli;
        }
        $total_hutang += $totalBeli + $row[$i]->transportasi + $row[$i]->angkut + $row[$i]->biaya_lain;
    }
    ?>
    <tr> 
        <td colspan="10" align="right"><b>Jumlah Total</b></td>
        <td align="center"><b><?php echo 'Rp. '.number_format($total_hutang); ?></b></td>
    </tr>
</tfoot>
</table>
<script>
    window.print();
</script>
</body>
</html>