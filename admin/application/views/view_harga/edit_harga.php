<?php echo form_open('admin/C_Harga/edit_harga/'.$no_barcode); ?>
<section >
  <div class="box box-default" style="width: 700px; float: center; margin-left: 200px; box-shadow: 10px;">
    <div class="box box-primary">
      <form role="form">
        <div class="box-body">
          
          <div class="form-group">
            <label>ID Harga</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-key"></i>
              </div>
              <input type="text" name="id_harga" class="form-control" value="<?php echo $id_harga ?>" readonly style="width: 250px;">
              <?php echo form_error('id_harga'); ?>
            </div>
          </div>

          <div class="form-group">
            <label for="">Nama Barang</label>
            <input type="hidden" name="no_barcode" value="<?= $no_barcode ?>">
            <input type="text" name="nama_barang" class="form-control" value="<?php echo $nama_barang ?>" readonly>
            <?php echo form_error('nama_barang'); ?>
          </div>

          <div class="form-group">
            <label for="">Rata-Rata HPP</label>
            <input type="text" name="rataHPP" id="rataHPP" class="form-control" value="<?php echo round($rataHPP) ?>" onkeyup="untung();" readonly>
            <?php echo form_error('rataHPP'); ?>
          </div>

          <div class="form-group">
            <label for="">Margin</label>
            <input type="text" name="margin" id="margin" class="form-control" value="<?php echo $margin.'%' ?>"  onkeyup="untung();" readonly>
            <?php echo form_error('margin'); ?>
          </div>

          <div class="form-group">
            <label>Harga</label>
            <input type="number" name="harga_jual" class="form-control" id="harga_jual" value="<?php echo $harga_jual ?>" onkeyup="untung();" required>
            <?php echo form_error('harga_jual'); ?>
          </div>

          <div class="form-group">
            <input type="hidden" name="tgl_ubah" class="form-control" value="<?php echo date('Y-m-d h:i:s'); ?>" style="width: 250px;">
            <?php echo form_error('tgl_ubah'); ?>
          </div> 

          <div class="box-footer" align="center">
            <a href="<?php echo site_url('admin/C_Harga/list_harga')?>"><button type="button" class="btn btn-default">Cancel</button></a>
            
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </section>
  <?php echo form_close(); ?>

  <script type="text/javascript">

    function untung() {
      var rataHPP = document.getElementById('rataHPP').value;
      var margin = document.getElementById('margin').value;
      var harga_jual = document.getElementById('harga_jual').value;
      
      var untung = parseFloat(harga_jual) - parseFloat(rataHPP);
      var laba= untung / parseFloat(rataHPP) * 100;

      if (!isNaN(untung)) {
       document.getElementById('margin').value = laba + '%';
     }
   }
 </script>