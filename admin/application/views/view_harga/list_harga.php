<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>KOPEBI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

      <div class="row" >
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                  
              <table id="example1" class="table table-bordered table-striped" style="width: 700px; float: center; margin-left: 200px; box-shadow: 10px;">
                <thead>
                    <tr>
                      <th>No</th>
                      <th>No Barcode</th>
                      <th>Nama Barang</th>
                      <th>Harga Jual</th>
                      <th>Waktu Ubah</th>
                      <th ></th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  $no=0;
                  foreach ($harga->result_array() as $value) { ?>
                    <tr class="odd gradeX">
                      <td ><?php echo ++$no?></td>
                      <td ><?php echo $value['no_barcode']?></td>
                      <td ><?php echo $value['nama_barang']?></td>
                      <td ><?php echo 'Rp '.number_format($value['harga_jual'])?></td>
                      <td ><?=date('d F Y h:i:s', strtotime($value['tgl_ubah']));?></td>
                                        
                      <td align="center">
                      <a href="<?php echo site_url('admin/C_Harga/edit_harga/'.$value['no_barcode'])?>"><button class="btn btn-warning" type="button">
                      <i class="fa fa-edit"></i> Edit</button></a>
                    </td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody></table>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>