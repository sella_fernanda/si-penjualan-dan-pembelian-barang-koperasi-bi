<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Stok extends CI_Model {

    public function get_laporan_stok($tgl_mulai,$tgl_selesai)
    {
        $this->db->select('*');
        $this->db->from('stok');
        $this->db->join('barang', 'barang.no_barcode = stok.no_barcode');
        $this->db->where('tgl_histori >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_histori <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }
}