<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Harga extends CI_Model {

	public function get_harga()
	{
		$this->db->select('*');
		$this->db->from('harga');
		$this->db->join('barang', 'barang.no_barcode = harga.no_barcode');
		$this->db->where('keterangan', 'Berlaku');
		$this->db->order_by('nama_barang', "asc");
		return $this->db->get();
	}

	public function getHarga($no_barcode)
    {
        $this->db->select('harga_jual');
        $this->db->from('harga');
        $this->db->where('no_barcode', $no_barcode);
        $this->db->where('keterangan', 'Tidak Berlaku','LIMIT 1');
        $this->db->order_by('id_harga', 'desc','LIMIT 1');
        $query =$this->db->get();
        $query = $query->row();
        return $query->harga_jual;
    }

    public function getHarga_awal($no_barcode)
    {
        $this->db->select('harga_jual');
        $this->db->from('harga');
        $this->db->where('no_barcode', $no_barcode);
        $this->db->order_by('id_harga', 'asc','LIMIT 1');
        $query =$this->db->get();
        $query = $query->row();
        return $query->harga_jual;
    }

    public function getRataHpp($no_barcode)
    {
        $this->db->select_avg('hpp');
        $this->db->from('detail_pembelian');
        $this->db->where('no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->row();
        return $query->hpp;
    }

    public function getJumlahHpp($no_barcode)
    {           
        $this->db->select_sum('hpp');
        $this->db->from('detail_pembelian');
        $this->db->where('no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->row();
        return $query->hpp;
    }

    public function getjumlahlisthpp($no_barcode)
    {           
        $this->db->select('no_barcode');
        $this->db->from('detail_pembelian');
        $this->db->where('no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->result_array();
        return count($query);
    }

	public function get_avg_hpp($no_barcode)
    {
        if ($this->getHarga_awal($no_barcode)==0) {
            $rata_hpp=$this->getRataHpp($no_barcode);
        }else{
            $harga=$this->getHarga_awal($no_barcode);
            $jml_hpp=$this->getJumlahHpp($no_barcode);
            $jml_list_hpp=$this->getjumlahlisthpp($no_barcode) + 1;
            $rata_hpp=($harga+$jml_hpp)/$jml_list_hpp;
        }
        return $rata_hpp;
    }

	public function get_harga_by($no_barcode)
	{
		$this->db->select('*');
		$this->db->from('harga');
		$this->db->join('barang', 'barang.no_barcode = harga.no_barcode');
		$this->db->where('harga.no_barcode', $no_barcode);
		$this->db->where('keterangan', 'Berlaku');
		return $this->db->get()->row_array();
	}

	public function getKet($no_barcode)
	{
		$data['keterangan']='Tidak Berlaku';
		$this->db->where('no_barcode',$no_barcode);
		$this->db->update('harga',$data);
	}

	public function update_harga($no_barcode,$data)
	{
		$this->getKet($no_barcode);
		$tgl_ubah=$this->input->post('tgl_ubah');
		$data['keterangan']='Berlaku';
		$data['no_barcode']=$no_barcode;
		$data['tgl_ubah']=$tgl_ubah;

		$this->db->insert('harga', $data); 
	}
}