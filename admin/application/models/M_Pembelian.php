<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pembelian extends CI_Model {

    public function get_laporan_pembelian($tgl_mulai,$tgl_selesai)
    {
        $this->db->select('*');
        $this->db->from('trans_pembelian');
        $this->db->join('jenis_pembayaran', 'trans_pembelian.id_jns_pembayaran = jenis_pembayaran.id_jns_pembayaran');
        $this->db->join('pegawai', 'trans_pembelian.nip = pegawai.nip');
        $this->db->join('suplier', 'trans_pembelian.id_suplier = suplier.id_suplier');
        $this->db->where('tgl_suply >=', $tgl_mulai);
        $this->db->where('tgl_suply <=', $tgl_selesai);
        return $this->db->get()->result();
    }

    public function get_detail_pembelian($no_pembelian){
        $this->db->select('no_pembelian,harga_beli,jumlah_beli');
        $this->db->from('detail_pembelian');
        $this->db->where('no_pembelian', $no_pembelian);
        return $this->db->get()->result();
    }

    public function get_laporan_hutang($tgl_mulai,$tgl_selesai)
    {
        $this->db->select('*');
        $this->db->from('trans_pembelian');
        $this->db->join('jenis_pembayaran', 'trans_pembelian.id_jns_pembayaran = jenis_pembayaran.id_jns_pembayaran');
        $this->db->join('suplier', 'trans_pembelian.id_suplier = suplier.id_suplier');
        $this->db->join('pegawai', 'trans_pembelian.nip = pegawai.nip');
        $this->db->where('trans_pembelian.id_jns_pembayaran =','P002');
        $this->db->where('tgl_suply >=', $tgl_mulai);
        $this->db->where('tgl_suply <=', $tgl_selesai);
        $this->db->order_by('trans_pembelian.id_suplier');
        return $this->db->get()->result();
    }
}