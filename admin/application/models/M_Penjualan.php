<?php
class M_Penjualan extends CI_Model{

    public function get_laporan_penjualan_on($tgl_mulai,$tgl_selesai)
    {
        $this->db->select('*,p.nama as nama_pegawai,pt.nama as nama_teller');
        $this->db->from('trans_penjualan');
        $this->db->join('jenis_pembayaran', 'trans_penjualan.id_jns_pembayaran = jenis_pembayaran.id_jns_pembayaran');
        $this->db->join('pegawai p', 'trans_penjualan.nip = p.nip');
        $this->db->join('pegawai pt', 'trans_penjualan.teller = pt.nip');
        $this->db->where('keterangan =','Pembelian Online');
        $this->db->where('status =','Selesai');
        $this->db->where('tgl_transaksi >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_transaksi <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }

    public function get_jumlah($tgl_mulai,$tgl_selesai)
    {
        $this->db->select_sum('total_bayar');
        $this->db->from('trans_penjualan');
        $this->db->where('keterangan =','Pembelian Online');
        $this->db->where('status =','Selesai');
        $this->db->where('tgl_transaksi >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_transaksi <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }

    public function get_jumlah_tunai($tgl_mulai,$tgl_selesai)
    {
        $this->db->select_sum('total_bayar');
        $this->db->from('trans_penjualan');
        $this->db->where('id_jns_pembayaran =','P001');
        $this->db->where('keterangan =','Pembelian Online');
        $this->db->where('status =','Selesai');
        $this->db->where('tgl_transaksi >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_transaksi <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }

    public function get_jumlah_piutang($tgl_mulai,$tgl_selesai)
    {
        $this->db->select_sum('total_bayar');
        $this->db->from('trans_penjualan');
        $this->db->where('id_jns_pembayaran =','P003');
        $this->db->where('keterangan =','Pembelian Online');
        $this->db->where('status =','Selesai');
        $this->db->where('tgl_transaksi >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_transaksi <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }

    public function get_laporan_penjualan_off($tgl_mulai,$tgl_selesai)
    {
        $this->db->select('*,p.nip as nip_pembeli, pt.nip as nip_teller,p.nama as nama_pegawai,pt.nama as nama_teller');
        $this->db->from('trans_penjualan');
        $this->db->join('jenis_pembayaran', 'trans_penjualan.id_jns_pembayaran = jenis_pembayaran.id_jns_pembayaran');
        $this->db->join('pegawai p', 'trans_penjualan.nip = p.nip','left');
        $this->db->join('pegawai pt', 'trans_penjualan.teller = pt.nip','left');
        $this->db->where('keterangan =','Pembelian Offline');
        $this->db->where('tgl_transaksi >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_transaksi <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }

    public function get_jumlah_off($tgl_mulai,$tgl_selesai)
    {
        $this->db->select_sum('total_bayar');
        $this->db->from('trans_penjualan');
        $this->db->where('keterangan =','Pembelian Offline');
        $this->db->where('tgl_transaksi >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_transaksi <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }

    public function get_jumlah_tunai_off($tgl_mulai,$tgl_selesai)
    {
        $this->db->select_sum('total_bayar');
        $this->db->from('trans_penjualan');
        $this->db->where('id_jns_pembayaran =','P001');
        $this->db->where('keterangan =','Pembelian Offline');
        $this->db->where('tgl_transaksi >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_transaksi <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }

    public function get_jumlah_piutang_off($tgl_mulai,$tgl_selesai)
    {
        $this->db->select_sum('total_bayar');
        $this->db->from('trans_penjualan');
        $this->db->where('id_jns_pembayaran =','P003');
        $this->db->where('keterangan =','Pembelian Offline');
        $this->db->where('tgl_transaksi >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_transaksi <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }

    public function get_laporan_piutang($tgl_mulai,$tgl_selesai)
    {
        $this->db->select('*');
        $this->db->from('trans_penjualan');
        $this->db->join('pegawai', 'trans_penjualan.nip = pegawai.nip');
        $this->db->where('id_jns_pembayaran =','P003');
        $this->db->where('status =','Selesai');
        $this->db->where('tgl_transaksi >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_transaksi <=', $tgl_selesai.' 23:59:59');
        $this->db->order_by('trans_penjualan.nip');
        return $this->db->get()->result();
    }

    public function get_piutang($tgl_mulai,$tgl_selesai)
    {
        $this->db->select_sum('total_bayar');
        $this->db->from('trans_penjualan');
        $this->db->where('id_jns_pembayaran =','P003');
        $this->db->where('status =','Selesai');
        $this->db->where('tgl_transaksi >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_transaksi <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }
}