<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Retur extends CI_Model {

	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('retur');
        $this->db->join('trans_pembelian', 'retur.no_pembelian = trans_pembelian.no_pembelian');
        $this->db->join('barang', 'retur.no_barcode = barang.no_barcode');
        $this->db->order_by('retur.no_pembelian', "asc");
		return $this->db->get();
	}
}