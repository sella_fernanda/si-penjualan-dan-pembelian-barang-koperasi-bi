<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Kasir extends CI_Model {

	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('trans_penjualan');
        $this->db->join('jenis_pembayaran', 'trans_penjualan.id_jns_pembayaran = jenis_pembayaran.id_jns_pembayaran');
        /*$this->db->join('pegawai', 'trans_penjualan.teller = pegawai.nip');*/
		$this->db->where('keterangan', 'Pembelian Offline');
        $this->db->order_by('no_transaksi', "asc");
		return $this->db->get();
	}

	public function detail($no_transaksi)
    {
        $this->db->select('*'); 
        $this->db->from('detail_penjualan');
        $this->db->join('barang', 'barang.no_barcode = detail_penjualan.no_barcode');
        $this->db->join('jenis_satuan', 'jenis_satuan.id_jns_satuan = barang.id_jns_satuan');
        $this->db->join('harga', 'harga.no_barcode = detail_penjualan.no_barcode');
        $this->db->join('trans_penjualan', 'trans_penjualan.no_transaksi = detail_penjualan.no_transaksi');
        $this->db->join('pegawai', 'trans_penjualan.teller = pegawai.nip',"left");
        $this->db->where('harga.keterangan', "Berlaku");

        $query = $this->db->get_where('', array('detail_penjualan.no_transaksi' => $no_transaksi));
        return $query;
    }

}