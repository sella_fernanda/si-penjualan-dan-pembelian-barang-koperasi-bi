<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pembelian extends CI_Model {

	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('trans_pembelian');
        $this->db->join('suplier', 'suplier.id_suplier = trans_pembelian.id_suplier');
        $this->db->join('jenis_pembayaran', 'jenis_pembayaran.id_jns_pembayaran = trans_pembelian.id_jns_pembayaran');
        $this->db->join('pegawai', 'pegawai.nip = trans_pembelian.nip');
        $this->db->order_by('no_pembelian', "asc");
        return $this->db->get();
    }

    public function detail($no_pembelian)
    {
        $this->db->select('*'); 
        $this->db->from('trans_pembelian');
        $this->db->join('detail_pembelian', 'detail_pembelian.no_pembelian = trans_pembelian.no_pembelian');
        $this->db->join('pegawai', 'trans_pembelian.nip = pegawai.nip');
        $this->db->join('barang', 'detail_pembelian.no_barcode = barang.no_barcode');
        $this->db->join('kategori_barang', 'detail_pembelian.id_kategori = kategori_barang.id_kategori');
        $query = $this->db->get_where('', array('trans_pembelian.no_pembelian' => $no_pembelian));
        return $query;
    }

    public function get_laporan_pembelian($tgl_mulai,$tgl_selesai)
    {
        $this->db->select('*');
        $this->db->from('trans_pembelian');
        $this->db->join('jenis_pembayaran', 'trans_pembelian.id_jns_pembayaran = jenis_pembayaran.id_jns_pembayaran');
        $this->db->join('pegawai', 'trans_pembelian.nip = pegawai.nip');
        $this->db->join('suplier', 'trans_pembelian.id_suplier = suplier.id_suplier');
        $this->db->where('tgl_suply >=', $tgl_mulai);
        $this->db->where('tgl_suply <=', $tgl_selesai);
        return $this->db->get()->result();
    }

    public function get_detail_pembelian($no_pembelian){
        $this->db->select('no_pembelian,harga_beli,jumlah_beli');
        $this->db->from('detail_pembelian');
        $this->db->where('no_pembelian', $no_pembelian);
        return $this->db->get()->result();
    }

    public function get_laporan_hutang($tgl_mulai,$tgl_selesai)
    {
        $this->db->select('*');
        $this->db->from('trans_pembelian');
        $this->db->join('jenis_pembayaran', 'trans_pembelian.id_jns_pembayaran = jenis_pembayaran.id_jns_pembayaran');
        $this->db->join('suplier', 'trans_pembelian.id_suplier = suplier.id_suplier');
        $this->db->join('pegawai', 'trans_pembelian.nip = pegawai.nip');
        $this->db->where('trans_pembelian.id_jns_pembayaran =','P002');
        $this->db->where('tgl_suply >=', $tgl_mulai);
        $this->db->where('tgl_suply <=', $tgl_selesai);
        $this->db->order_by('trans_pembelian.id_suplier');
        return $this->db->get()->result();
    }
}