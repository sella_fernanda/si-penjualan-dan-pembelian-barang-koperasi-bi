<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Barang extends CI_Model {
	
	public function get_barang()
	{
		return $this->db->query("SELECT no_barcode FROM barang ")->result();
	}

	public function get_pembelian()
	{
		return $this->db->query("SELECT no_pembelian FROM trans_pembelian 
			")->result();
	}

	public function get_penjualan_on()
	{
		return $this->db->query("SELECT no_transaksi FROM trans_penjualan 
			WHERE keterangan = 'Pembelian Online' AND status = 'Selesai'")->result();
	}

	public function get_penjualan_off()
	{
		return $this->db->query("SELECT no_transaksi FROM trans_penjualan 
			WHERE keterangan = 'Pembelian Offline' AND status = 'Selesai'")->result();
	}

	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('barang');
		$this->db->join('harga', 'barang.no_barcode = harga.no_barcode');
		$this->db->where('harga.keterangan', "Berlaku");
		$this->db->order_by('nama_barang', "asc");
		return $this->db->get();
	}

	public function detail($no_barcode)
	{
		$this->db->select('*'); 
		$this->db->from('barang');
		$this->db->join('kategori_barang', 'barang.id_kategori = kategori_barang.id_kategori');
		$this->db->join('jenis_satuan', 'barang.id_jns_satuan = jenis_satuan.id_jns_satuan');
		$this->db->join('harga', 'barang.no_barcode = harga.no_barcode');
		$this->db->where('harga.keterangan', "Berlaku");
		$query = $this->db->get_where('', array('barang.no_barcode' => $no_barcode));
		return $query;
	}
}