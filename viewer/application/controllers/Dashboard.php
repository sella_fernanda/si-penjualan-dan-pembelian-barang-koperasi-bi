<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Barang');
	}

	public function index()
	{
	//check_not_login();
     $this->template->set('title','HOME');
     $this->template->load('adminLTE2','contents','v_index2');
	}

	public function Toko()
	{
		//check_not_login();

		$queryBarang = $this->M_Barang->get_barang();	
		$queryPembelian  = $this->M_Barang->get_pembelian();
		$queryPenjualanOn = $this->M_Barang->get_penjualan_on();
		$queryPenjualanOff = $this->M_Barang->get_penjualan_off();
		$data['jml_barang'] = count($queryBarang);
		$data['jml_pembelian']   = count($queryPembelian);
		$data['jml_penjualan_on']  = count($queryPenjualanOn);
		$data['jml_penjualan_off']  = count($queryPenjualanOff);
		$this->template->set('title', 'Dashboard');
		$this->template->load('adminLTE', 'contents' , 'v_index',$data);
	}

}

