<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Retur extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Retur');
	}

	public function list_retur()
	{
		$data['retur'] = $this->M_Retur->get_list();

		$this->template->set('title', 'Data Retur');
		$this->template->load('adminLTE', 'contents' , 'view_retur/list_retur', $data);
	}
	
}