<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Barang');
	}

	public function list_barang()
	{
		$data['barang'] = $this->M_Barang->get_list();
		$this->template->set('title', 'Data Barang');
		$this->template->load('adminLTE', 'contents' , 'view_barang/list_barang', $data);
	}

	public	function detail($no_barcode)
	{	
		$this->template->set('title', 'Detail Data');

		$query = $this->M_Barang->detail($no_barcode);
		$data['query'] = $query;
		$this->template->load('adminLTE', 'contents' , 'view_barang/detail_barang', $data);

	}	


}

