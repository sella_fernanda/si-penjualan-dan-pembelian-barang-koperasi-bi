<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Kasir extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Kasir');
	}

	public function list_kasir()
	{
		$data['kasir'] = $this->M_Kasir->get_list();
		$this->template->set('title', 'Data Penjualan Offline');
		$this->template->load('adminLTE', 'contents' , 'view_kasir/list_kasir', $data);
	}

	public function detail($no_transaksi)
	{
		$query = $this->M_Kasir->detail($no_transaksi);
		$data['query'] = $query;
		
		$this->template->set('title', 'Detail Pembalian Offline');
		$this->template->load('adminLTE', 'contents' , 'view_kasir/detail_transaksi_kasir', $data);
	}

}