<!doctype html>
<html>
<head>
    <title>Laporan Pembelian</title>
    <style>
        .word-table {
            border:1px solid black !important; 
            border-collapse: collapse !important;
            width: 100%;
        }
        .word-table tr th, .word-table tr td{
            border:1px solid black !important; 
            padding: 5px 10px;
            font-size: 12px;
        }
        .word-table tr th {
            text-align: center;
        }
    </style>
</head> 
<body>
    <br><br>
    <p style="font-family: Arial; font-size: 14;" align="center"><b>KOPERASI PEGAWAI BANK INDONESIA</b></p>
    <p style="font-family: Arial; font-size: 14;" align="center"><b>LAPORAN STOK OPNAME</b></p> 
    <p style="font-family: Arial; font-size: 14;" align="center"><b>Periode <?=date('d F Y', strtotime($tgl_mulai));?> - <?=date('d F Y', strtotime($tgl_selesai));?></b></p> <br>
    <table class="word-table" style="margin-bottom: 10px">
        <thead>
            <tr>
              <th>No</th>
              <th>Nama Barang</th>
              <th>Stok Awal</th>
              <th>Stok Pembelian</th>
              <th>Stok Penjualan</th>
              <th>Stok Retur</th>
              <th>Jumlah Stok</th>
              <th>Stok Opname</th>
              <th>Stok Gain Loss</th>
              <th>Waktu Pengecekan</th>
          </tr>
      </thead>
      <tbody>
        <?php
        $start = 0;
        foreach ($laporan as $value) { ?>
            <tr>
                <td align="center"><?php echo ++$start ?></td>
                <td align="center"><?php echo $value->nama_barang?></td>
                <td align="center"><?php echo $value->stok_awal?></td>
                <td align="center"><?php echo $value->stok_buy?></td>
                <td align="center"><?php echo $value->stok_penjualan?></td>
                <td align="center"><?php echo $value->stok_retur?></td>
                <td align="center"><?php echo $value->total?></td>
                <td align="center"><?php echo $value->stok_opname?></td>
                <td align="center"><?php echo $value->gain_loss?></td>
                <td align="center"><?php echo date('d F Y', strtotime($value->tgl_histori)); ?></td>
            </tr>
        <?php } ?>


    </tbody>
</table>
<script>
    window.print();
</script>
</body>
</html>