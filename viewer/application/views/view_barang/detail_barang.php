<!DOCTYPE html>
<html lang="en">
<head>
  <body>
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">

            <table id="example" class="table table-bordered table-hover">
              <thead>
                <tr>                      
                  <th>No Barcode</th>
                  <th>Satuan</th>
                  <th>Kategori</th>
                  <th>Nama Barang</th>
                  <th>Harga Jual</th>
                  <th>Stok Awal</th>
                  <th>Stok Total</th>
                  <th>Gambar</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $no=0;
                foreach ($query->result_array() as $value) { ?>
                  <tr>
                    <td><?php echo $value['no_barcode']?></td>
                    <td><?php echo $value['nama_satuan']?></td>
                    <td><?php echo $value['nama_kategori']?></td>
                    <td><?php echo $value['nama_barang']?></td>
                    <td><?php echo $value['harga_jual']?></td>
                    <td><?php echo $value['stok_awal']?></td>
                    <td><?php echo $value['stok_total']?></td>
                    <td>
                      <?php if ($value['gambar'] == NULL) { ?>
                        <img style="width: 100px; height:100px" src="<?php echo base_url('stoker/upload/default.JPG') ?>">
                      <?php } else { ?>
                        <img style="width: 100px; height:100px" src="<?php echo base_url('stoker/upload/'.$value['gambar']) ?>">
                      <?php } ?>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </br>
          <center><a href="<?php echo site_url('viewer/C_Barang/list_barang')?>"><button type="submit" class="btn btn-default">Kembali</button></a><br><br></center>
        </br>
      </div>
    </div>
  </div>

</body>
</html>