<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>KOPEBI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini"><div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border"></div>
            <div class="box-body">
                <form action="<?php echo site_url('viewer/C_Pembelian/laporan_hutang'); ?>" method="get" class="form-inline"> 
                    <div class="row">
                        <div class="col-md-2">
                            <span style="font-size: 18px;">Tanggal Mulai</span>
                        </div>
                        <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" onfocus="(this.type='date')" placeholder="Tanggal Mulai" name="tgl_mulai" class="form-control pull-right" id="tgl_mulai" required 
                            value = <?php
                            if($tgl_mulai == ''){
                                echo '';
                            }else{
                                echo date('d-F-Y',strtotime($tgl_mulai));
                            }
                            ?>>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <span style="font-size: 18px;">Tanggal Selesai</span>
                    </div>
                    <div class="form-group">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" onfocus="(this.type='date')" name="tgl_selesai" class="form-control pull-right" id="tgl_selesai" placeholder="Tanggal Selesai" required
                        value = <?php
                        if($tgl_selesai == ''){
                            echo '';
                        }else{
                            echo date('d-F-Y',strtotime($tgl_selesai));
                        }
                        ?>>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-5">
                    <h4><b><span style="line-height: 0"></span></b></h4>
                </div>
                <div class="col-md-7">
                    <button class="btn btn-success"><i class="fa fa-search fa fw"></i> Cari</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="body-body">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-3"><span>Laporan Hutang</span></div>
                        
                        </div>
                    </div>
                </form>
                <div class="box-body">

                    <table class="table table-bordered table-striped table-hover" style="margin-bottom: 10px" id="example">
                        <thead>
                            <th>No</th>
                            <th>No Pembelian</th>
                            <th>Suplier</th>
                            <th>Tanggal</th>
                            <th>Pembayaran</th>
                            <th>Stoker</th>
                            <th>Total Beli</th>
                            <th>Transportasi</th>
                            <th>Angkut</th>
                            <th>Biaya Lain</th>
                            <th>Total Bayar</th>
                        </thead>
                        <tbody>
                            <?php
                            $start = 0;
                            $total_hutang = 0;
                            $row = $hutang;
                            for ($i = 0; $i < count($hutang); $i++) { ?> <!-- index buat nyari id selanjutnya $i = index -->
                            <?php $tempSup = $row[$i]->id_suplier; ?> <!--variabel current id. nyocokin id sekarang sm id berikutnya bikin dummy variabel-->
                            <tr>
                                <td><?php echo ++$start ?></td>
                                <td><?php echo $row[$i]->no_pembelian; ?></td>
                                <td><?php echo $row[$i]->nama_suplier; ?></td>
                                <td><?php echo date('d F Y', strtotime($row[$i]->tgl_suply)); ?></td>
                                <td><?php echo $row[$i]->nama_pembayaran; ?></td>
                                <td><?php echo $row[$i]->nama; ?></td>
                                <td><?php 
                                $detailPembelian = $this->M_Pembelian->get_detail_pembelian($row[$i]->no_pembelian);
                                $totalBeli = 0;
                                foreach($detailPembelian as $key => $val){
                                    $beli = $val->harga_beli * $val->jumlah_beli;
                                    $totalBeli += $beli;
                                }
                                echo 'Rp. '.number_format($totalBeli);
                                ?></td>
                                <td><?php echo 'Rp. '.number_format($row[$i]->transportasi); ?></td>
                                <td><?php echo 'Rp. '.number_format($row[$i]->angkut); ?></td>
                                <td><?php echo 'Rp. '.number_format($row[$i]->biaya_lain); ?></td>
                                <td><?php echo 'Rp. '.number_format($totalBeli + $row[$i]->transportasi + $row[$i]->angkut + $row[$i]->biaya_lain)?></td>
                            </tr>
                            <?php $nextSup = isset($row[$i+1]->id_suplier) ? $row[$i+1]->id_suplier : 1 ?>
                            <!-- nyari id_suplier selanjutnya kalo ga ada di isi 0-->
                            <?php $total_hutang += ($totalBeli + $row[$i]->transportasi + $row[$i]->angkut + $row[$i]->biaya_lain); ?>
                            <!-- ngehitung total bayar per id_suplier -->
                            <?php if($tempSup != $nextSup): ?> <!-- nyocokin kalo id_suplier current beda sama id_suplier berikutnya maka ditampilkan totalnya -->
                            <tr> 
                                <td colspan="10" align="right"><b>Total</b></td>
                                <td><b><?php echo 'Rp. '.number_format($total_hutang) ?></b></td>
                            </tr>
                            <?php $total_hutang = 0; ?> <!-- reset total hutang biar ga kejumlah sama hutang selanjutnya -->
                        <?php endif; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <?php
                    $total_hutang = 0;
                    $row = $hutang;
                    for ($i = 0; $i < count($hutang); $i++) {
                        $detailPembelian = $this->M_Pembelian->get_detail_pembelian($row[$i]->no_pembelian);
                        $totalBeli = 0;
                        foreach($detailPembelian as $key => $val){
                            $beli = $val->harga_beli * $val->jumlah_beli;
                            $totalBeli += $beli;
                        }
                        $total_hutang += $totalBeli + $row[$i]->transportasi + $row[$i]->angkut + $row[$i]->biaya_lain;
                    }
                    ?>
                    <tr> 
                        <td colspan="10" align="right"><b>Jumlah Total</b></td>
                        <td><b><?php echo 'Rp. '.number_format($total_hutang); ?></b></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</div>
</div>

<!-- /.row -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
  })
})
</script>
</body>
</html>
