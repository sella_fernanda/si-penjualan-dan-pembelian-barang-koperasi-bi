 <section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <?php date_default_timezone_set("Asia/Jakarta");
        $tgl = date('d/m/Y h:i:s'); ?>

        <i class="fa fa-globe"></i> KOPEBI.
        <small class="pull-right">Date: <?php echo $tgl ;?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
    </div>
    <!-- /.col -->
    <div class="col-sm-3 invoice-col">
      <?php 
      $no=0;
      foreach ($query->result_array() as $value) { ?>
        <?php $id = $value['id_pembelian'] ?>
        <?php $no = $value['no_pembelian'] ?>
        <?php $nama = $value['nama'] ?>
      <?php
      }
      ?>
      <b>Invoice : </b> <?php echo "#".$id ;?> <br>
      <br>
      <b>No Pembelian :</b> <?php echo $no ;?><br>
      <b>Stoker :</b> <?php echo $nama ;?>
      <br><br>
    </div>

    <div class="col-sm-3 invoice-col">
      <?php 
      $no=0;
      foreach ($query->result_array() as $value) { ?>
        <?php $transport = $value['transportasi'] ?>
        <?php $angkut = $value['angkut'] ?>
        <?php $lain = $value['biaya_lain'] ?>
      <?php
      }
      ?>
      <b>Biaya Transportasi :</b> <?php echo $transport ;?><br>
      <b>Biaya Angkut :</b> <?php echo $angkut ;?><br>
      <b>Biaya Lain-Lain :</b> <?php echo $lain ;?><br><br><br>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Kategori</th>
            <th>No Barcode</th>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Harga Beli</th>
            <th>PPN</th>
            <th>Diskon</th>
            <th>HPP</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $no=0;
          foreach ($query->result_array() as $value) { ?>
            <tr>
              <td ><?php echo ++$no?></td>
              <td><?php echo $value['nama_kategori']?></td>
              <td><?php echo $value['no_barcode']?></td>
              <td><?php echo $value['nama_barang']?></td>
              <td><?php echo $value['jumlah_beli']?></td>
              <td><?php echo $value['harga_beli']?></td>
              <td><?php echo $value['ppn']?></td>
              <td><?php echo $value['diskon']?></td>
              <td><?php echo 'Rp '.number_format($value['hpp'])?></td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="row">
    <!-- accepted payments column -->
    <div class="col-xs-6">
    
    </div>
    <!-- /.col -->
    <div class="col-xs-6">
      <?php 
      $no=0;
      foreach ($query->result_array() as $value) { ?>
        <?php 
        $tgl_trans = $value['tgl_suply'];?>
      <?php
      }
      ?>
      <p class="lead">Waktu Transaksi : <?php echo $tgl_trans ;?></p>

      <div class="table-responsive">
        <table class="table">
          <tr>
          </tr>
        </table>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="<?php echo site_url('viewer/C_Pembelian/list_pembelian')?>" class="btn btn-default">Kembali</a>
      <a href="" onclick="window.print()" class="btn btn-primary">Cetak <span class="fa fa-print"></span></a>
    </div>
  </div>
</section>