<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>KOPEBI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini"><div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border"></div>
            <div class="box-body">
                <form action="<?php echo site_url('viewer/C_Pembelian/laporan_pembelian'); ?>" method="get" class="form-inline"> 
                    <div class="row">
                        <div class="col-md-2">
                            <span style="font-size: 18px;">Tanggal Mulai</span>
                        </div>
                        <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" onfocus="(this.type='date')" placeholder="Tanggal Mulai" name="tgl_mulai" class="form-control pull-right" id="tgl_mulai" required 
                            value = <?php
                            if($tgl_mulai == ''){
                                echo '';
                            }else{
                                echo date('d-F-Y',strtotime($tgl_mulai));
                            }
                            ?>>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <span style="font-size: 18px;">Tanggal Selesai</span>
                    </div>
                    <div class="form-group">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" onfocus="(this.type='date')" name="tgl_selesai" class="form-control pull-right" id="tgl_selesai" placeholder="Tanggal Selesai" required
                        value = <?php
                        if($tgl_selesai == ''){
                            echo '';
                        }else{
                            echo date('d-F-Y',strtotime($tgl_selesai));
                        }
                        ?>>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-5">
                    <h4><b><span style="line-height: 0"></span></b></h4>
                </div>
                <div class="col-md-7">
                    <button class="btn btn-success"><i class="fa fa-search fa fw"></i> Cari</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="body-body">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-3"><span>Laporan Pembelian</span></div>
                    </div>
                </form>
                <div class="box-body">

                    <table class="table table-bordered table-striped table-hover" style="margin-bottom: 10px" id="example">
                        <thead>
                            <th>No</th>
                            <th>No Pembelian</th>
                            <th>Suplier</th>
                            <th>Tanggal</th>
                            <th>Pembayaran</th>
                            <th>Stoker</th>
                            <th>Total Beli</th>
                            <th>Transportasi</th>
                            <th>Angkut</th>
                            <th>Biaya Lain</th>
                            <th>Total Bayar</th>
                        </thead>
                        <tbody>
                            <?php
                            $start = 0;
                            if($laporan != null):
                            foreach ($laporan as $row) { ?>
                                <tr>
                                    <td><?php echo ++$start ?></td>
                                    <td><?php echo $row->no_pembelian; ?></td>
                                    <td><?php echo $row->nama_suplier; ?></td>
                                    <td><?php echo date('d F Y', strtotime($row->tgl_suply)); ?></td>
                                    <td><?php echo $row->nama_pembayaran; ?></td>
                                    <td><?php echo $row->nama; ?></td>
                                    <td><?php 
                                    $detailPembelian = $this->M_Pembelian->get_detail_pembelian($row->no_pembelian);
                                    $totalBeli = 0;
                                    foreach($detailPembelian as $key => $val){
                                        $beli = $val->harga_beli * $val->jumlah_beli;
                                        $totalBeli += $beli;
                                    }
                                    echo 'Rp. '.number_format($totalBeli);
                                    ?></td>
                                    <td><?php echo 'Rp. '.number_format($row->transportasi); ?></td>
                                    <td><?php echo 'Rp. '.number_format($row->angkut); ?></td>
                                    <td><?php echo 'Rp. '.number_format($row->biaya_lain); ?></td>
                                    <td><?php echo 'Rp. '.number_format($totalBeli + $row->transportasi + $row->angkut + $row->biaya_lain)?></td>
                                </tr>
                            <?php } endif; ?>
                        </tbody>
                        <tfoot>
                            <?php
                            $total=0;
                            if($laporan):
                            foreach ($laporan as $row) {
                                $detail = $this->M_Pembelian->get_detail_pembelian($row->no_pembelian);
                                $totalBeli = 0;
                                foreach($detail as $key => $val){
                                    $jumlah = $val->harga_beli * $val->jumlah_beli;
                                    $totalBeli += $jumlah;
                                }
                                $total += $totalBeli + $row->transportasi + $row->angkut + $row->biaya_lain;
                            }
                        endif;
                            ?>
                            <tr> 
                                <td colspan="10" align="right"><b>Jumlah Total</b></td>
                                <td><b><?php echo 'Rp. '.number_format($total); ?></b></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /.row -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
  })
})
</script>
</body>
</html>
