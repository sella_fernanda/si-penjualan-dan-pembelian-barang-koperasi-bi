<?php echo form_open('stoker/C_Satuan/input_satuan/'); ?>
<section >
    <div class="box box-default" style="width: 700px; float: center; margin-left: 200px; box-shadow: 10px;">
      <div class="box box-primary">
            <form role="form">
              <div class="box-body">
                
                <div class="form-group">
                  <label>ID Satuan</label>
                <div class="input-group">
                  <div class="input-group-addon">
                      <i class="fa fa-key"></i>
                  </div>
                  <input type="text" name="id_jns_satuan" class="form-control" value="<?= $kodeunik; ?>" readonly style="width: 250px;">
                  <?php echo form_error('id_jns_satuan'); ?>
                </div>
              </div>

              	<div class="form-group">
                  <label>Nama Satuan</label>
                  <input type="text" name="nama_satuan" class="form-control" id="" placeholder="Masukkan nama satuan" required>
                  <?php echo form_error('nama_satuan'); ?>
                </div>

                <div class="box-footer" align="center">
                <a href="<?php echo site_url('stoker/C_Satuan/list_satuan')?>"><button type="button" class="btn btn-default">Cancel</button></a>
              
                <button type="reset" class="btn btn-danger">Reset</button>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
</section>
          <?php echo form_close(); ?>