<?php echo form_open_multipart('stoker/C_Barang/input_barang/'); ?>
<section >
  <div class="box box-default" style="width: 700px; float: center; margin-left: 200px; box-shadow: 10px;">
    <div class="box box-primary">
      <form role="form">
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">

              <div class="form-group">
                <label>Barcode : </label>
                <input type="radio" class="barcode" name="barcode" value="manual" checked> Manual
                <input type="radio" class="barcode" name="barcode" value="generate"> Generate
                <?php echo form_error('barcode'); ?>
              </div>

              <div class="form-group">
                <label>No Barcode</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-key"></i>
                  </div>
                  <input type="text" name="no_barcode" class="form-control" id="no_barcode" placeholder="Masukkan no barcode" required>
                  <?php echo form_error('no_barcode'); ?>
                </div>
              </div>

              <div class="form-group">
                <label>Kategori</label>
                <?php  
                foreach ($query->result_array() as $row)
                {
                 $option[$row['id_kategori']]=$row['nama_kategori'];
               }
               echo form_dropdown('id_kategori', $option,'', 'class="form-control"');
               echo form_error('nama_kategori');
               ?>

               <a href="<?php echo site_url('stoker/C_Kategori_Barang/input_kategori')?>"><button type="button"  class="btn btn-success" data-toggle="modal" data-target="#modal-default"><span class="fa fa-plus"></span></button></a>
             </div>

             <div class="form-group">
              <label>Nama Barang</label>
              <input type="text" name="nama_barang" class="form-control" id="" placeholder="Masukkan nama barang" required>
              <?php echo form_error('nama_barang'); ?>
            </div>
          </div>

          <div class="col-md-6">

            <div class="form-group">
              <label>Satuan</label>
              <?php  
              foreach ($query2->result_array() as $row2)
              {
                $option2[$row2['id_jns_satuan']]=$row2['nama_satuan'];
              }
              echo form_dropdown('id_jns_satuan', $option2,'', 'class="form-control"');
              echo form_error('nama_satuan');
              ?>
            </div>

            <div class="form-group">
              <label>Stok Awal</label>
              <input type="text" name="stok_awal" class="form-control" id="" placeholder="Masukkan jumlah stok awal" required>
              <?php echo form_error('stok_awal'); ?>
            </div>     

            <div class="form-group">
              <label for="">Harga Awal</label>
              <input type="text" name="harga_jual" class="form-control" id="" placeholder="Harga Jual" required>
              <?php echo form_error('harga_jual'); ?>
            </div>

            <!-- tgl ubah untuk tgl di tabel harga -->
            <div class="form-group">
              <input type="hidden" name="tgl_ubah" class="form-control" value="<?php echo date('Y-m-d h:i:s'); ?>" style="width: 250px;">
              <?php echo form_error('tgl_ubah'); ?>
            </div> 

            <!-- keterangan untuk keterangan di tabel harga -->
            <div class="form-group">
              <input type="hidden" name="keterangan" class="form-control" value="Berlaku" style="width: 250px;">
              <?php echo form_error('keterangan'); ?>
            </div>   

            <div class="form-group">
              <label>File Gambar</label>
              <input type="file" name="gambar" id="gambar">
              <p class="help-block">Unggah gambar barang</p> 
              <?php echo form_error('gambar'); ?>
            </div>
          </div>
        </div>
      </div>

      <div class="box-footer" align="center">
        <a href="<?php echo site_url('stoker/C_Barang/list_barang')?>"><button type="button" class="btn btn-default">Cancel</button></a>

        <button type="reset" class="btn btn-danger">Reset</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
</section>
<?php echo form_close(); ?>

<script type="text/javascript">
  $(document).ready(function () {

    function enableInput(i) {
      $(i).removeClass('readonly').prop('readonly', false).prop('required', true);
    }

    function disableInput(i) {
      $(i).addClass('readonly').prop('readonly', true).prop('required', false).val('');
    }

    $('input[name="barcode"]').on('change', function () {
      var value = $(this).val();

      if (value != "generate") {
        enableInput('input[name="no_barcode"]');
      } else {
        disableInput('input[name="no_barcode"]');
        //random generate code wiyh multiple random together
        var tendigitrandom = Math.floor(Math.random()*Math.floor(Math.random()*Date.now())); 
        document.getElementById("no_barcode").value = tendigitrandom;
      }
    });
  });
</script>