<?php echo form_open_multipart('stoker/C_Barang/edit/'.$no_barcode); ?>
<section >
  <div class="box box-default" style="width: 700px; float: center; margin-left: 200px; box-shadow: 10px;">
    <div class="box box-primary">
      <form role="form">
        <div class="box-body">

          <div class="row">
            <div class="col-md-6">

              <div class="form-group">
                <label for="">No Barcode</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-key"></i>
                  </div>
                  <input type="text" name="no_barcode" class="form-control" id="" placeholder="No barcode" value="<?php echo $no_barcode ?>" required readonly>
                  <?php echo form_error('no_barcode'); ?>
                </div>
              </div>

              <div class="form-group">
                <label>Kategori</label>
                <?php  
                foreach ($query->result_array() as $row)
                {
                 $option[$row['id_kategori']]=$row['nama_kategori'];
               }
               echo form_dropdown('id_kategori', $option, $id_kategori, 'class="form-control"');
               echo form_error('nama_kategori'); 
               ?>
             </div>

             <div class="form-group">
              <label for="">Nama Barang</label>
              <input type="text" name="nama_barang" class="form-control" id="" placeholder="Nama Barang" value="<?php echo $nama_barang ?>" required>
              <?php echo form_error('nama_barang'); ?>
            </div>
            
            <div class="form-group">
              <label>Satuan</label>
              <?php  
              foreach ($query2->result_array() as $rows)
              {
                $options[$rows['id_jns_satuan']]=$rows['nama_satuan'];
              }
              echo form_dropdown('id_jns_satuan', $options, $id_jns_satuan, 'class="form-control"');
              echo form_error('nama_satuan');
              ?>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="">Stok Awal</label>
              <input type="text" name="stok_awal" class="form-control" id="" placeholder="" value="<?php echo $stok_awal ?>" readonly>
              <?php echo form_error('stok_awal'); ?>
            </div>

            <div class="form-group">
              <input type="hidden" name="id_harga" class="form-control" value="<?php echo $id_harga ?>" style="width: 250px;">
              <?php echo form_error('id_harga'); ?>
            </div>

            <div class="form-group">
              <label for="">Harga Awal</label>
              <input type="text" name="harga_jual" class="form-control" id="" placeholder="Harga Jual" value="<?php echo $harga_jual ?>" readonly>
              <?php echo form_error('harga_jual'); ?>
            </div>

            <div class="form-group">
              <input type="hidden" name="tgl_ubah" class="form-control" value="<?php echo date('Y-m-d h:i:s'); ?>" style="width: 250px;">
              <?php echo form_error('tgl_ubah'); ?>
            </div> 

            <div class="form-group">
              <input type="hidden" name="keterangan" class="form-control" value="<?php echo $keterangan ?>" style="width: 250px;">
              <?php echo form_error('keterangan'); ?>
            </div>  

            <div class="form-group">
              <label>File Gambar</label>
              <br><?php foreach ($query1->result_array() as $g) { ?> 
                <?php if ($g['gambar'] == NULL) { ?>
                      <img style="width: 100px; height:100px" src="<?php echo base_url('stoker/upload/default.JPG') ?>">
                    <?php } else { ?>
                      <img style="width: 100px; height:100px" src="<?php echo base_url('stoker/upload/'.$g['gambar']) ?>">
                    <?php } ?>
                  <?php echo form_upload('gambar',$g['gambar']); } ?>
                  <p class="help-block">Update gambar barang</p>  
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer" align="center">
            <a href="<?php echo site_url('stoker/C_Barang/list_barang')?>"><button type="button" class="btn btn-default">Cancel</button></a>

            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <?php echo form_close(); ?>