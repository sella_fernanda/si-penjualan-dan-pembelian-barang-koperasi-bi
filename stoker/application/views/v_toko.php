<!DOCTYPE html>
<html>
<head>
<style>
div.gallery {
  border: 1px solid #ccc;
  background-color: #ffff;
}

div.gallery:hover {
  border: 3px solid #008CBA;
}

div.gallery img {
  width: 50%;
  height: 50%;
}

div.nama {
  font-size: 18px;
  padding-top: 10px;
  padding-left: 10px;
}

div.harga {
  font-size: 18px;
  padding-left: 10px;
  -webkit-box-flex: 1;
-webkit-flex-grow: 1;
-moz-box-flex: 1;
-ms-flex-positive: 1;
flex-grow: 1;
-webkit-flex-shrink: 0;
-ms-flex-negative: 0;
flex-shrink: 0;
color: #ee4d2d;
}

div.stok {
  font-size: 12px;
  text-align: right;
  font-family: helvetica;
  padding-right: 10px;
}

* {
  box-sizing: border-box;
}

.responsive {
  padding: 0 10px;
  float: left;
  width: 20%;
}

@media only screen and (max-width: 700px) {
  .responsive {
    width: 49.99999%;
    margin: 6px 0;
  }
}

@media only screen and (max-width: 500px) {
  .responsive {
    width: 100%;
  }
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 10px 25px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;
}

.button1 {
  background-color: white; 
  color: #4CAF50; 
  border: 2px solid #4CAF50;
}

.button1:hover {
  background-color: #4CAF50;
  color: white;
}

.button2 {
  background-color: white; 
  color: #008CBA; 
  border: 2px solid #008CBA;
}

.button2:hover {
  background-color: #008CBA;
  color: white;
}

</style>
</head>
<body>

<?php foreach ($barang->result_array() as $row) {
  ?>
<div class="responsive">
  <div class="gallery">
    <a target="">
      <center><img src="<?php echo base_url('upload/'.$row['gambar']) ?>" style="background-size: contain; background-repeat: no-repeat;"></center>
    </a>
    <div class="nama"><?php echo $row['nama_barang']?></div>
    <div class="harga">Rp <?php echo $row['harga_jual']?>,-</div>
    <div class="stok">Sisa stok <?php echo $row['stok_total']?></div>
    <center>
      <form method="post" action="<?php echo base_url('Cart/add_to_cart'); ?>">
                <input type="hidden" name="no_barcode" value="<?php echo $row['no_barcode'];?>">
                <input type="hidden" name="nama_barang" value="<?php echo $row['nama_barang'];?>">
                <input type="hidden" name="harga_jual" value="<?php echo $row['harga_jual'];?>">
                <input type="hidden" name="jumlah_barang" value="1">

      <?php if($this->session->userdata('username') == '') {?>
      <a class="button button2" href="<?php echo base_url('C_login');?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Add to Cart</a>
      <?php }else{?>
      <button type="submit" class="button button2"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Add to Cart</button>
      <?php } ?>
    </form>

    </center>
  </div>
</div>
<?php } ?>

</body>
</html>
