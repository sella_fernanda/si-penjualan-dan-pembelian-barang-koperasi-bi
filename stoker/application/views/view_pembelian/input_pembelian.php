<?php echo form_open('stoker/C_Pembelian/input_pembelian/'); ?>

<section>
  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Pembelian Barang</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>No Pembelian</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-key"></i>
              </div>
              <input type="text" name="no_pembelian" class="form-control" placeholder="Masukkan No Pembelian" value="<?= $kode_pembelian; ?>" readonly
              <?php 
              if ($this->session->has_userdata('no_pembelian')) {
                echo 'value="'.$this->session->userdata('no_pembelian').'" readonly';
              } ?>>
              <?php echo form_error('no_pembelian'); ?>
            </div>
          </div>

          <div class="form-group">
            <label>Tanggal Pembelian</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" name="tgl_suply" class="form-control pull-right" id="datepicker" placeholder="Masukkan tanggal beli" required
              <?php 
              if ($this->session->has_userdata('tgl_suply')) {
                echo 'value="'.$this->session->userdata('tgl_suply').'"readonly';
              } ?>>
            </div>
            <?php echo form_error('tgl_suply'); ?>
          </div>
          <!-- /.input group -->


          <div class="form-group">
            <label>Nama Suplier</label>
            <?php  
            foreach ($query3->result_array() as $row)
            {
              $option[$row['id_suplier']]=$row['nama_suplier'];
            }
            echo form_dropdown('id_suplier', $option,'', 'class="form-control"', 
              'if ($this->session->has_userdata("id_suplier")) {
                echo "value="'.$this->session->userdata('id_suplier').'" readonly";
              }'
            );
            ?>
          </div>

          <div class="form-group">
            <label>Jenis Pembayaran</label>
            <?php  
            foreach ($query2->result_array() as $rows)
            {
              $options[$rows['id_jns_pembayaran']]=$rows['nama_pembayaran'];
            }
            echo form_dropdown('id_jns_pembayaran', $options,'','class="form-control"',
              'if ($this->session->has_userdata("id_jns_pembayaran")) {
                echo "value="'.$this->session->userdata('id_jns_pembayaran').'" readonly";
              } '
            );
            ?>
          </div>

          <div class="box-footer" style="padding-top: 230px">
            <a href="<?php echo site_url('stoker/C_Pembelian/list_pembelian')?>"><button type="button" class="btn btn-default">Cancel</button></a>
          </div>
        </div>
        <!-- /.col -->

        <div class="col-md-6">
          <div class="form-group">
            <label>Kategori Barang</label>
            <select name="id_kategori" id="kategori" class="form-control select2">
              <option value="0"></option>
              <?php foreach($query4->result() as $row):?>
                <option value="<?php echo $row->id_kategori;?>"><?php echo $row->nama_kategori;?></option>
              <?php endforeach;?>
            </select>
          </div>

          <div class="form-group">
            <label>Nama Barang</label>
            <select id="no_barcode" name="no_barcode" class="barang form-control select2">
              <option value="0"></option>
              <?php foreach($query->result() as $row1):?>
                <option value="<?php echo $row1->no_barcode;?>"><?php echo $row1->nama_barang;?></option>
              <?php endforeach;?>
            </select>      
          </div>
          <div class="form-group">
            <label>Jumlah Barang</label>
            <input type="number" name="jumlah_beli" class="form-control" placeholder="Masukkan jumlah barang" required>
            <?php echo form_error('jumlah_beli'); ?>
          </div>

          <div class="form-group">
            <label>PPN</label>
            <select class="form-control" value="0" name="ppn" id="ppn" class="form-control" onkeyup="sum();" style="width: 100%;">
              <option selected="selected">0%</option>
              <option>5%</option>
              <option>10%</option>\
            </select>
            <?php echo form_error('ppn'); ?>
          </div>

          <div class="form-group">
            <label>Harga Beli</label>
            <input type="text" min="0" value="0" name="harga_beli" id="harga_beli" class="form-control" onkeyup="sum();" required>
            <?php echo form_error('harga_beli'); ?>
          </div>

          <div class="form-group">
            <label>Diskon</label>
            <input type="text" min="0" value="0" name="diskon" id="diskon" class="form-control" placeholder="Masukkan diskon per@ dalam rupiah" onkeyup="sum();" required>
            <?php echo form_error('diskon'); ?>
          </div>

          <div class="form-group">
            <label>HPP</label>
            <input type="" value='0' name="hpp" id="hpp" class="form-control" onkeyup="sum();" readonly="" style="width: 150px; font-size: 17px;">
            <?php echo form_error('hpp'); ?>
          </div>

          <div class="box-footer" align="right">
            <button type="submit" class="btn btn-info"><span class="fa fa-shopping-cart"></span> Tambahkan</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="box box-success">
    <div class="box-header with-border">
      <form role="form" form name='pembelian'>
        <div class="box-body">
          <tbody>
            <td style="vertical-align: top">
              No Pembelian  : <?php echo $this->session->userdata('no_pembelian'); ?><br>
              Tanggal : <?php echo $this->session->userdata('tgl_suply'); ?><br>
              Supplier : <?php echo $this->session->userdata('suplier'); ?><br>
              Jenis Pembayaran : <?php echo $this->session->userdata('jenis_pembayaran'); ?><br>
              <hr>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Kategori</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Harga Beli</th>
                    <th>PPN</th>
                    <th>Diskon</th>
                    <th>HPP</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <?php
                $total_jumlah = 0;
                if ($this->session->has_userdata('barang')) {
                  $list_barang = $this->session->userdata('barang');
                  $total_jumlah=0;
                  foreach ($list_barang as $key => $value) {
                    ?>
                    <tr>
                      <td><?php echo $this->M_Pembelian->getIdKategori($value['id_kategori']); ?></td>
                      <td><?php echo $this->M_Pembelian->getIdBarang($value['no_barcode']); ?></td>
                      <td><?php echo $value['jumlah_beli']; ?></td>
                      <td><?php echo $value['harga_beli']; ?></td>
                      <td><?php echo $value['ppn']; ?></td>
                      <td><?php echo $value['diskon']; ?></td>
                      <td><?php echo $value['hpp']; ?></td>
                      <td><a href="<?php echo base_url('stoker/C_Pembelian/hapus/'.$key) ?>"><span class="fa fa-trash"></span></a></td>
                    </tr>

                    <?php 

                    $total_jumlah = $total_jumlah + $value['jumlah_beli'];
                  }  
                }
                ?>
              </table>
            </td>
          </tbody>
        </div>
      </form>
    </div>
  </div>
</section>
<div align="center" style="padding-left: 900px;">
  <a href="<?php echo base_url('stoker/C_Pembelian/reset') ?>"><button class="btn btn-danger">Reset</button></a>
  <a href="<?php echo base_url('stoker/C_Pembelian/input_biaya/'.$total_jumlah) ?>"><button class="btn btn-primary">Lanjutkan <span class="fa fa-arrow-circle-right"></span></button></a>
</div>
<?php echo form_close(); ?>


<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-2.2.3.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#kategori').change(function(){
      var no_barcode=$(this).val();
      $.ajax({
        url : "<?php echo base_url();?>stoker/C_Pembelian/get_subkategori",
        method : "POST",
        data : {no_barcode: no_barcode},
        async : false, 
        dataType : 'json', // tipe pertukaran data
        success: function(data){
          var html = '';
          var i;
          for(i=0; i<data.length; i++){
            html += '<option value='+data[i].no_barcode+'>'+data[i].nama_barang+'</option>';
          }
          $('.barang').html(html);
        }
      });
    });
  });

  function sum() {
    var harga_beli = document.getElementById('harga_beli').value;
    var ppn = document.getElementById('ppn').value;
    var diskon = document.getElementById('diskon').value;

    var ppn2 = parseFloat(harga_beli) * parseFloat(ppn) / 100;
    var result = parseFloat(harga_beli) + ppn2 - parseFloat(diskon);
    if (!isNaN(result)) {
     document.getElementById('hpp').value = "Rp "+result;
   }
 }
</script>

<script type="text/javascript">
   //Date picker
   $('#datepicker').datepicker({
    autoclose: true
  })
</script>
</div>