<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>KOPEBI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

  <div class="row">
    <div class="col-xs-12">

      <div class="box">
        <div class="box-body">
          <a href="<?php echo site_url('stoker/C_Pembelian/input_pembelian')?>"><button type="submit" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</button></a><br><br>

          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>No Pembelian</th>
                <th>Tanggal Pembelian</th>
                <th>Suplier</th>
                <th>Pembayaran</th>
                <th>Total Bayar</th>
                <th>Stoker</th>
                <th ></th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $no=0;
              foreach ($pembelian->result_array() as $value) { ?>
                <tr class="odd gradeX">
                  <td ><?php echo ++$no?></td>
                  <td ><?php echo $value['no_pembelian']?></td>
                  <td ><?=date('d F Y', strtotime($value['tgl_suply']));?></td>
                  <td ><?php echo $value['nama_suplier']?></td>
                  <td ><?php echo $value['nama_pembayaran']?></td>
                  <?php 
                  $detailPembelian = $this->M_Pembelian->get_detail_pembelian($value['no_pembelian']);
                  $totalBeli = 0;
                  foreach($detailPembelian as $key => $val){
                    $beli = $val->harga_beli * $val->jumlah_beli;
                    $totalBeli += $beli;
                  }?>
                  <td><?php echo 'Rp. '.number_format($totalBeli + $value['transportasi'] + $value['angkut'] + $value['biaya_lain'])?></td>
                  <td ><?php echo $value['nama']?></td>

                  <td align="center">
                    <a href="<?php echo site_url('stoker/C_Pembelian/detail/'.$value['no_pembelian'])?>"><button class="btn btn-info" type="button"><i class="fa fa-info"></i></button>
                    </a>
                    <a href="<?php echo site_url('stoker/C_Pembelian/delete/'.$value['no_pembelian'])?>" onclick="return confirm('Yakin Hapus?')"><button class="btn btn-danger" type="button"><i class="fa fa-trash"></i></button>
                    </a>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-retur<?php echo $value['no_pembelian']?>"><span class="fa fa-retweet"></span> Retur
                    </button>

                  </td>
                </tr>
                <?php
              }
              ?>
            </tbody></table>

            <!-- MODAL RETUR -->
            <?php 
            foreach ($pembelian->result_array() as $value) { ?>
              <div class="modal fade" id="modal-retur<?php echo $value['no_pembelian']?>">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header" style="background-color: #3c8dbc; color: #fff">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Input Retur</h4>
                      </div>
                      <form class="form-horizontal" method="post" 
                      action="<?php echo base_url().'stoker/C_Retur/input_retur'?>"> <!-- aksi -->
                      <div class="modal-body">
                        <form role="form">
                          <div class="box-body">
                            <div class="form-group">
                              <label>No Pembelian</label>
                              <input type="text" name="no_pembelian" class="form-control" id="no_pembelian" placeholder="" value="<?php echo $value['no_pembelian'] ?>" readonly>
                              <?php echo form_error('no_pembelian'); ?>
                            </div>

                            <?php
                            $bayar=$value['id_jns_pembayaran'];
                            if ($bayar =='P001') {?>
                              <div class="form-group">
                                <label>Retur</label> <br>
                                <input type="radio" class="jenis_retur" name="jenis_retur" value="dikembalikan barang"> Dikembalikan Barang <br>
                                <input type="radio" class="jenis_retur" name="jenis_retur" value="dikembalian kas"> Dikembalikan Kas <br>
                              </div>
                            <?php } else { ?>
                              <div class="form-group">
                                <label>Retur</label> <br>
                                <input type="radio" class="jenis_retur" name="jenis_retur" value="dikembalikan barang"> Dikembalikan Barang <br>
                                <input type="radio" class="jenis_retur" name="jenis_retur" value="potong hutang"> Potong Hutang
                              </div>
                            <?php } ?>

                            <div class="form-group">
                              <label>Barang</label>
                              <select class="form-control" id="no_barcode" name="no_barcode" required>
                               <?php
                               $controller->get_pembelian_list($value['no_pembelian']);
                               ?>      
                             </select>
                           </div>

                           <div class="form-group">
                            <label>Jumlah Retur</label>
                            <input type="text" name="jumlah" class="form-control" id="" placeholder="Masukkan Jumlah Barang" required>
                            <?php echo form_error('jumlah'); ?>
                          </div> 
                        </form>
                        <?php echo form_close(); ?>

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Retur <span class="fa fa-retweet"></span></button>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
      <script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
      <script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

      <script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
      <script>
        $(function () {
          $('#example1').DataTable()
          $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
          })
        })

          $(".jenis_retur").on("click", function(){ //ambil data jenis_retur
            var jenis_retur = $(this).val(); // mengambil id dari jenis_retur yang dipilih
            $.ajax({
              method : 'POST',
              url : '<?php echo base_url('stoker/C_Retur/ambil_jenis_retur'); ?>',
                data :  { jenis_retur: jenis_retur}, //mengirim variabel dengan nilai dari variabel itu
              });
          });
        </script>
      </body>
      </html>