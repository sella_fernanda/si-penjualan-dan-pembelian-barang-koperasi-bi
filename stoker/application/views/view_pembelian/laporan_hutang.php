<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>KOPEBI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini"><div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border"></div>
            <div class="box-body">
                <form action="<?php echo site_url('stoker/C_Pembelian/laporan_hutang'); ?>" method="get" class="form-inline"> 
                    <div class="row">
                        <div class="col-md-2">
                            <span style="font-size: 18px;">Tanggal Mulai</span>
                        </div>
                        <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" onfocus="(this.type='date')" placeholder="Tanggal Mulai" name="tgl_mulai" class="form-control pull-right" id="tgl_mulai" required 
                            value = <?php
                            if($tgl_mulai == ''){
                                echo '';
                            }else{
                                echo date('d-F-Y',strtotime($tgl_mulai));
                            }
                            ?>>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <span style="font-size: 18px;">Tanggal Selesai</span>
                    </div>
                    <div class="form-group">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" onfocus="(this.type='date')" name="tgl_selesai" class="form-control pull-right" id="tgl_selesai" placeholder="Tanggal Selesai" required
                        value = <?php
                        if($tgl_selesai == ''){
                            echo '';
                        }else{
                            echo date('d-F-Y',strtotime($tgl_selesai));
                        }
                        ?>>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-5">
                    <h4><b><span style="line-height: 0"></span></b></h4>
                </div>
                <div class="col-md-7">
                    <button class="btn btn-success"><i class="fa fa-search fa fw"></i> Cari</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="body-body">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-3"><span>Laporan Hutang</span></div>
                        <div class="col-md-9 text-right">
                            <?php if ($tgl_mulai == '' || $tgl_selesai == '') { ?>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-flat" disabled>Cetak Laporan</button>
                                    <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown" disabled>
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                </div>  
                            <?php } else { ?>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-flat">Cetak Laporan</button>
                                    <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?php echo site_url('stoker/C_Pembelian/cetak_laporan_hutang_word/'.$_GET['tgl_mulai'].'/'.$_GET['tgl_selesai']); ?>"><i class="fa fa-file-word-o fa fw primary" style="color: #0D47A1"></i> Word</a></li>
                                        <li><a href="<?php echo site_url('stoker/C_Pembelian/cetak_laporan_hutang_excel/'.$_GET['tgl_mulai'].'/'.$_GET['tgl_selesai']); ?>"><i class="fa fa-file-excel-o fa fw" style="color: #1B5E20"></i>Excel</a></li>
                                        <li><a href="#" 
                                            onclick="window.open('<?php echo site_url('stoker/C_Pembelian/cetak_laporan_hutang_pdf/'.$_GET['tgl_mulai'].'/'.$_GET['tgl_selesai']); ?>',
                                            'name','width=1024,height=768')"><i class="fa fa-file-pdf-o fa fw" style="color: #D50000"></i> Pdf</a></li>
                                        </ul>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </form>
                <div class="box-body">

                    <table class="table table-bordered table-striped table-hover" style="margin-bottom: 10px" id="example">
                        <thead>
                            <th>No</th>
                            <th>No Pembelian</th>
                            <th>Suplier</th>
                            <th>Tanggal</th>
                            <th>Pembayaran</th>
                            <th>Stoker</th>
                            <th>Total Beli</th>
                            <th>Transportasi</th>
                            <th>Angkut</th>
                            <th>Biaya Lain</th>
                            <th>Total Bayar</th>
                        </thead>
                        <tbody>
                            <?php
                            $start = 0;
                            $total_hutang = 0;
                            $row = $hutang;
                            for ($i = 0; $i < count($hutang); $i++) { ?> <!-- index buat nyari id selanjutnya $i = index -->
                            <?php $tempSup = $row[$i]->id_suplier; ?> <!--variabel current id. nyocokin id sekarang sm id berikutnya bikin dummy variabel-->
                            <tr>
                                <td><?php echo ++$start ?></td>
                                <td><?php echo $row[$i]->no_pembelian; ?></td>
                                <td><?php echo $row[$i]->nama_suplier; ?></td>
                                <td><?php echo date('d F Y', strtotime($row[$i]->tgl_suply)); ?></td>
                                <td><?php echo $row[$i]->nama_pembayaran; ?></td>
                                <td><?php echo $row[$i]->nama; ?></td>
                                <td><?php 
                                $detailPembelian = $this->M_Pembelian->get_detail_pembelian($row[$i]->no_pembelian);
                                $totalBeli = 0;
                                foreach($detailPembelian as $key => $val){
                                    $beli = $val->harga_beli * $val->jumlah_beli;
                                    $totalBeli += $beli;
                                }
                                echo 'Rp. '.number_format($totalBeli);
                                ?></td>
                                <td><?php echo 'Rp. '.number_format($row[$i]->transportasi); ?></td>
                                <td><?php echo 'Rp. '.number_format($row[$i]->angkut); ?></td>
                                <td><?php echo 'Rp. '.number_format($row[$i]->biaya_lain); ?></td>
                                <td><?php echo 'Rp. '.number_format($totalBeli + $row[$i]->transportasi + $row[$i]->angkut + $row[$i]->biaya_lain)?></td>
                            </tr>
                            <?php $nextSup = isset($row[$i+1]->id_suplier) ? $row[$i+1]->id_suplier : 1 ?>
                            <!-- nyari id_suplier selanjutnya kalo ga ada di isi 0-->
                            <?php $total_hutang += ($totalBeli + $row[$i]->transportasi + $row[$i]->angkut + $row[$i]->biaya_lain); ?>
                            <!-- ngehitung total bayar per id_suplier -->
                            <?php if($tempSup != $nextSup): ?> <!-- nyocokin kalo id_suplier current beda sama id_suplier berikutnya maka ditampilkan totalnya -->
                            <tr> 
                                <td colspan="10" align="right"><b>Total</b></td>
                                <td><b><?php echo 'Rp. '.number_format($total_hutang) ?></b></td>
                            </tr>
                            <?php $total_hutang = 0; ?> <!-- reset total hutang biar ga kejumlah sama hutang selanjutnya -->
                        <?php endif; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <?php
                    $total_hutang = 0;
                    $row = $hutang;
                    for ($i = 0; $i < count($hutang); $i++) {
                        $detailPembelian = $this->M_Pembelian->get_detail_pembelian($row[$i]->no_pembelian);
                        $totalBeli = 0;
                        foreach($detailPembelian as $key => $val){
                            $beli = $val->harga_beli * $val->jumlah_beli;
                            $totalBeli += $beli;
                        }
                        $total_hutang += $totalBeli + $row[$i]->transportasi + $row[$i]->angkut + $row[$i]->biaya_lain;
                    }
                    ?>
                    <tr> 
                        <td colspan="10" align="right"><b>Jumlah Total</b></td>
                        <td><b><?php echo 'Rp. '.number_format($total_hutang); ?></b></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</div>
</div>

<!-- /.row -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
  })
})
</script>
</body>
</html>
