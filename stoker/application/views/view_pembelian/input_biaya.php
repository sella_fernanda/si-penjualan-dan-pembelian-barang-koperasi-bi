<?php echo form_open('stoker/C_Pembelian/input_biaya/'.$this->uri->segment(3)) ?>

<section>
  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Biaya Pengiriman</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Transportasi</label>
            <input type="text" min="0" value="0" name="transport" id="transport" class="form-control" onkeyup="sum();" required >
            <?php echo form_error('transport'); ?>
          </div>

          <div class="form-group">
            <label>Angkut</label>
            <input type="text" min="0" value="0" name="angkut" id="angkut" class="form-control"  onkeyup="sum();" required>
            <?php echo form_error('angkut'); ?>
          </div>

          <div class="form-group">
            <label>Biaya Lain</label>
            <input type="text" min="0" value="0" name="biaya_lain" id="biaya_lain" class="form-control"  onkeyup="sum();" required>
            <?php echo form_error('biaya_lain'); ?>
          </div>

          <div class="box-footer" align="left">
            <a href="<?php echo site_url('stoker/C_Pembelian/input_pembelian')?>" class="btn btn-default">Kembali</a>
          </div>

        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Total Biaya</label>
            <input type="" value='0' name="biaya" id="biaya" class="form-control" onkeyup="sum();" readonly="" style="width: 150px; font-size: 17px;">
            <?php echo form_error('biaya'); ?>
          </div>


          <div class="form-group">
            <label>Total barang</label>
            <input type="text" value='<?php 
            echo $this->session->userdata('total_jumlah');?>' name="total_jumlah" id="total_jumlah" class="form-control" onkeyup="sum();"style="width: 150px; font-size: 17px;">
            <?php echo form_error('total_jumlah'); ?>
          </div>

          <div class="form-group">
            <label>Biaya per Satuan Barang</label>
            <input type="text" value='0' name="biaya_jadi" id="biaya_jadi" class="form-control" onkeyup="sum();" readonly="" style="width: 150px; font-size: 17px;">
            <?php echo form_error('biaya_jadi'); ?>
          </div>
          <div class="box-footer" align="right">
            <button type="submit" class="btn btn-info"><span class="fa fa-shopping-cart"></span> Tambahkan</button>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="box box-success">
      <div class="box-header with-border">
        <form role="form" form name='pembelian'>
          <div class="box-body">
            <tbody>
              <td style="vertical-align: top">
                No Pembelian  : <?php echo $this->session->userdata('no_pembelian'); ?><br>
                Tanggal : <?php echo $this->session->userdata('tgl_suply'); ?><br>
                Supplier : <?php echo $this->session->userdata('suplier'); ?><br>
                Jenis Pembayaran : <?php echo $this->session->userdata('jenis_pembayaran'); ?><br>
                Transportasi : <?php echo $this->session->userdata('transport'); ?><br>
                Angkut : <?php echo $this->session->userdata('angkut'); ?><br>
                Biaya Lain : <?php echo $this->session->userdata('biaya_lain'); ?><br>
                <hr>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Kategori</th>
                      <th>Nama Barang</th>
                      <th>Jumlah</th>
                      <th>Harga Beli</th>
                      <th>PPN</th>
                      <th>Diskon</th>
                      <th>HPP</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <?php
                  $total_jumlah = 0;
                  if ($this->session->has_userdata('barang')) {
                    $list_barang = $this->session->userdata('barang');
                    $total_jumlah=0;
                    foreach ($list_barang as $key => $value) {
                      ?>
                      <tr>
                        <td><?php echo $this->M_Pembelian->getIdKategori($value['id_kategori']); ?></td>
                        <td><?php echo $this->M_Pembelian->getIdBarang($value['no_barcode']); ?></td>
                        <td><?php echo $value['jumlah_beli']; ?></td>
                        <td><?php echo $value['harga_beli']; ?></td>
                        <td><?php echo $value['ppn']; ?></td>
                        <td><?php echo $value['diskon']; ?></td>
                        <td><?php echo $value['hpp']; ?></td>
                        <td><a href="<?php echo base_url('stoker/C_Pembelian/hapus/'.$key) ?>"><span class="fa fa-trash"></span></a></td>
                      </tr>

                      <?php 

                      $total_jumlah = $total_jumlah + $value['jumlah_beli'];
                    }  
                  }
                  ?>
                </table>
              </td>
            </tbody>
          </div>
        </form>
      </div>
    </div>
    <div align="center" style="padding-left: 900px;">
      
      <?php
        if ($this->session->has_userdata('transport')){
          ?>
          <a class="btn btn-primary" href="<?php echo base_url('stoker/C_Pembelian/simpan') ?>">Simpan <span class="fa fa-arrow-circle-right"></span></a>
          <?php
        }else{
          ?>
          <button class="btn btn-primary" disabled="">Simpan <span class="fa fa-arrow-circle-right"></span></button>

          <?php
        }
      ?>
    </div>
  </section>

  <?php echo form_close(); ?>


  <script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- bootstrap datepicker -->
  <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-2.2.3.min.js'?>"></script>
  <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
  <script type="text/javascript">

    function sum() {
      var transport = document.getElementById('transport').value;
      var angkut = document.getElementById('angkut').value;
      var biaya_lain = document.getElementById('biaya_lain').value;
      var total_jumlah = document.getElementById('total_jumlah').value;

      var result = parseFloat(transport) + parseFloat(angkut) + parseFloat(biaya_lain);
      var biaya= result / parseFloat(total_jumlah);

      if (!isNaN(result)) {
       document.getElementById('biaya').value = result;
       document.getElementById('biaya_jadi').value = biaya;
     }
   }
 </script>
</div>
