<!doctype html>
<html>
<head>
    <title>Laporan Pembelian</title>
    <style>
        .word-table {
            border:1px solid black !important; 
            border-collapse: collapse !important;
            width: 100%;
        }
        .word-table tr th, .word-table tr td{
            border:1px solid black !important; 
            padding: 5px 10px;
            font-size: 10px;
        }
        .word-table tr th {
            text-align: center;
        }
    </style>
</head> 
<body>
    <br><br>
    <p style="font-family: Arial; font-size: 14;" align="center"><b>KOPERASI PEGAWAI BANK INDONESIA</b></p>
    <p style="font-family: Arial; font-size: 14;" align="center"><b>LAPORAN PEMBELIAN</b></p> 
    <p style="font-family: Arial; font-size: 14;" align="center"><b>Periode <?=date('d F Y', strtotime($tgl_mulai));?> - <?=date('d F Y', strtotime($tgl_selesai));?></b></p> <br>
    <table class="word-table" style="margin-bottom: 10px">
        <thead>
            <tr>
                <th>No</th>
                <th>No Pembelian</th>
                <th>Suplier</th>
                <th>Tanggal</th>
                <th>Pembayaran</th>
                <th>Stoker</th>
                <th>Total Beli</th>
                <th>Transportasi</th>
                <th>Angkut</th>
                <th>Biaya Lain</th>
                <th>Total Bayar</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $start = 0;
            foreach ($laporan as $row) { ?>
                <tr>
                    <td align="center"><?php echo ++$start ?></td>
                    <td align="center"><?php echo $row->no_pembelian; ?></td>
                    <td align="center"><?php echo $row->nama_suplier; ?></td>
                    <td align="center"><?php echo date('d F Y', strtotime($row->tgl_suply)); ?></td>
                    <td align="center"><?php echo $row->nama_pembayaran; ?></td>
                    <td align="center"><?php echo $row->nama; ?></td>
                    <td align="center"><?php 
                    $detailPembelian = $this->M_Pembelian->get_detail_pembelian($row->no_pembelian);
                    $totalBeli = 0;
                    foreach($detailPembelian as $key => $val){
                        $beli = $val->harga_beli * $val->jumlah_beli;
                        $totalBeli += $beli;
                    }
                    echo 'Rp. '.number_format($totalBeli);
                    ?></td>
                    <td align="center"><?php echo 'Rp. '.number_format($row->transportasi); ?></td>
                    <td align="center"><?php echo 'Rp. '.number_format($row->angkut); ?></td>
                    <td align="center"><?php echo 'Rp. '.number_format($row->biaya_lain); ?></td>
                    <td align="center"><?php echo 'Rp. '.number_format($totalBeli + $row->transportasi + $row->angkut + $row->biaya_lain)?></td>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <?php
            $total=0;
            foreach ($laporan as $row) {
                $detail = $this->M_Pembelian->get_detail_pembelian($row->no_pembelian);
                $totalBeli = 0;
                foreach($detail as $key => $val){
                    $jumlah = $val->harga_beli * $val->jumlah_beli;
                    $totalBeli += $jumlah;
                }
                $total += $totalBeli + $row->transportasi + $row->angkut + $row->biaya_lain;
            }
            ?>
            <tr> 
                <td colspan="10" align="right"><b>Jumlah Total</b></td>
                <td align="center"><b><?php echo 'Rp. '.number_format($total); ?></b></td>
            </tr>
        </tfoot>
    </table>
    <script>
        window.print();
    </script>
</body>
</html>