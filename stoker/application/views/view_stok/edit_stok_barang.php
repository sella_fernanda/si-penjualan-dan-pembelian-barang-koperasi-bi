<?php echo form_open('stoker/C_Stok/edit_stok_barang/'.$no_barcode); ?>
<section >
  <div class="box box-default" style="width: 700px; float: center; margin-left: 200px; box-shadow: 10px;">
    <div class="box box-primary">
      <form role="form">
        <div class="box-body">

          <div class="form-group">
            <label>ID Barang</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-key"></i>
              </div>
              <input type="text" name="no_barcode" class="form-control" value="<?php echo $no_barcode ?>" readonly style="width: 250px;">
              <?php echo form_error('no_barcode'); ?>
            </div>
          </div>

          <div class="form-group">
            <label for="">Nama Barang</label>
            <input type="hidden" name="no_barcode" value="<?= $no_barcode ?>">
            <input type="text" name="nama_barang" class="form-control" value="<?php echo $nama_barang ?>" readonly>
            <?php echo form_error('nama_barang'); ?>
          </div>


          <div class="form-group">
            <label for="">Stok Awal</label>
            <input type="text" name="stok_awal" class="form-control" value="<?php echo $stok_awal ?>" readonly>
            <?php echo form_error('stok_awal'); ?>
          </div>

          <div class="form-group">
            <label for="">Stok Pembelian</label>
            <input type="text" name="stok_buy" class="form-control" value="<?php echo $stok_buy ?>" readonly>
            <?php echo form_error('stok_buy'); ?>
          </div>
          
          <div class="form-group">
            <label for="">Stok Retur</label>
            <input type="text" name="stok_retur" class="form-control" value="<?php echo $stok_retur ?>" readonly>
            <?php echo form_error('stok_retur'); ?>
          </div>

          <div class="form-group">
            <label for="">Stok Penjualan</label>
            <input type="text" name="stok_penjualan" class="form-control" value="<?php echo $stok_penjualan ?>" readonly>
            <?php echo form_error('stok_penjualan'); ?>
          </div>

          <div class="form-group">
            <label for="">Stok Total</label>
            <input type="text" name="stok_total" id="stok_total" onkeyup="sum();" class="form-control" value="<?php echo $stok_total ?>" readonly>
            <?php echo form_error('stok_total'); ?>
          </div>

          <div class="form-group">
            <label for="">Stok Opname</label>
            <input type="text" name="stok_opname" id="stok_opname" onkeyup="sum();" class="form-control" value="0">
            <?php echo form_error('stok_opname'); ?>
          </div>

          <div class="form-group">
            <label for="">Gain Loss</label>
            <input type="text" name="gain_loss" id="gain_loss" class="form-control" value="0">
            <?php echo form_error('gain_loss'); ?>
          </div>

          <div class="form-group">
            <?php date_default_timezone_set("Asia/Jakarta");
            $tgl_transaksi = date('Y-m-d h:i:s');?>
            
            <input type="hidden" name="tgl_histori" class="form-control" value="<?php echo $tgl_transaksi; ?>" style="width: 250px;">
            <?php echo form_error('tgl_histori'); ?>
          </div> 

          <div class="box-footer" align="center">
            <a href="<?php echo site_url('stoker/C_Stok/list_stok_barang')?>"><button type="button" class="btn btn-default">Cancel</button></a>

            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </section>

  <?php echo form_close(); ?>

  <script type="text/javascript">
    function sum() {
      var num = "-1";
      var stok_total = document.getElementById('stok_total').value;
      var stok_opname = document.getElementById('stok_opname').value;
      var result = num * (parseFloat(stok_total) - parseFloat(stok_opname));
      if (!isNaN(result)) {
        if (stok_opname>stok_total) {
         document.getElementById('gain_loss').value = "+"+result;
       }else{
         document.getElementById('gain_loss').value = result;
       }
     }
   }
 </script>