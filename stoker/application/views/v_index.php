<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <br>
                <center>
                    <img src="<?php echo base_url() ?>assets/img/BI.png" alt="logo bank indonesia" width="500" height="200">
                </center> 
                <br> <br>
            </div>
        </div>   
    </div>
</div>

<section class="content">
          <!-- small box -->
        <div class="col-sm-1"></div>
           <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <p>Draft Pembelian</p>

              <h3><?php echo $jml_pembelian ?></h3>

            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="<?php echo site_url('stoker/C_Pembelian/list_pembelian'); ?>" class="small-box-footer">
              Detail <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <p>Draft Retur</p>

              <h3><?php echo $jml_retur ?></h3>

            </div>
            <div class="icon">
              <i class="fa fa-envelope-o"></i>
            </div>
            <a href="<?php echo site_url('stoker/C_Retur/list_retur'); ?>" class="small-box-footer">
              Detail <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <p>Draft Logistik</p>

              <h3><?php echo $jml_logistik ?></h3>

            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="<?php echo site_url('stoker/C_Barang/list_barang'); ?>" class="small-box-footer">
              Detail <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
      </div>
</section>