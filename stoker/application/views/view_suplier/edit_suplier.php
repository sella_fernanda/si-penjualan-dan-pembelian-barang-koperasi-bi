<?php echo form_open('stoker/C_Suplier/edit_suplier/'.$id_suplier); ?>
<section >
    <div class="box box-default" style="width: 700px; float: center; margin-left: 200px; box-shadow: 10px;">
      <div class="box box-primary">
            <form role="form">
              <div class="box-body">
                
                <div class="form-group">
                  <label>ID Suplier</label>
                <div class="input-group">
                  <div class="input-group-addon">
                      <i class="fa fa-key"></i>
                  </div>
                  <input type="text" name="id_suplier" class="form-control" value="<?php echo $id_suplier ?>" readonly style="width: 250px;">
                  <?php echo form_error('id_suplier'); ?>
                </div>
              </div>

              	<div class="form-group">
                  <label>Nama Suplier</label>
                  <input type="text" name="nama_suplier" class="form-control" id="" placeholder="Masukkan nama suplier" value="<?php echo $nama_suplier ?>" required>
                  <?php echo form_error('nama_suplier'); ?>
                </div>

                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control" id="" placeholder="Masukkan alamat" value="<?php echo $alamat ?>" required>
                  <?php echo form_error('alamat'); ?>
                </div>

                <div class="form-group">
                  <label>No Telp</label>
                  <input type="text" name="no_hp" class="form-control" id="" placeholder="Masukkan no telepon" value="<?php echo $no_hp ?>" required>
                  <?php echo form_error('no_hp'); ?>
                </div> 

                <div class="box-footer" align="center">
                <a href="<?php echo site_url('stoker/C_Suplier/list_suplier')?>"><button type="button" class="btn btn-default">Cancel</button></a>
              
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
</section>
          <?php echo form_close(); ?>