<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Stok extends CI_Model {

	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('barang');
        $this->db->join('kategori_barang', 'barang.id_kategori = kategori_barang.id_kategori');
        $this->db->order_by('nama_kategori','asc');
        $this->db->order_by('nama_barang','asc');
		return $this->db->get();
	}

	public function get_by($no_barcode)
	{
		$this->db->select('*');
		$this->db->from('barang');
		$this->db->where('barang.no_barcode', $no_barcode);
		return $this->db->get()->row_array();
	}

	public function get_histori()
	{
		$this->db->select('*');
		$this->db->from('stok');
        $this->db->join('barang', 'barang.no_barcode = stok.no_barcode');
        $this->db->order_by('nama_barang', "asc");
        return $this->db->get();
    }

    public function update_stok_opname($no_barcode,$data)
    {
    	$this->db->insert('stok', $data);
    }

    public function get_total_stok($no_barcode)
    {
        $this->db->select('stok_awal');
        $this->db->from('barang');
        $this->db->where('barang.no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->row();
        return $query->stok_awal;
    }

    public function get_total_stok2($no_barcode)
    {
        $this->db->select_sum('jumlah_barang','jual');
        $this->db->where('no_barcode', $no_barcode);
        return $stok_penjualan['stok_penjualan']=$this->db->get('detail_penjualan')->row()->jual;
    }

    public function get_total_stok3($no_barcode)
    {

        $this->db->select_sum('jumlah','retur_beli');
        $this->db->where('no_barcode', $no_barcode);
        $this->db->where('keterangan !=', 'dikembalikan barang');
        return $stok_retur['stok_retur']=$this->db->get('retur')->row()->retur_beli;
    }

    public function get_total_stok5($no_barcode)
    {

        $this->db->select_sum('jumlah_beli','beli');
        $this->db->where('no_barcode', $no_barcode);
        return $stok_buy['stok_buy']=$this->db->get('detail_pembelian')->row()->beli;
    }

    public function delete($id_stok)
    {
        $this->db->where('id_stok', $id_stok);
        $this->db->delete('stok');
    }

    public function get_laporan_stok($tgl_mulai,$tgl_selesai)
    {
        $this->db->select('*');
        $this->db->from('stok');
        $this->db->join('barang', 'barang.no_barcode = stok.no_barcode');
        $this->db->where('tgl_histori >=', $tgl_mulai.' 00:00:00');
        $this->db->where('tgl_histori <=', $tgl_selesai.' 23:59:59');
        return $this->db->get()->result();
    }
}