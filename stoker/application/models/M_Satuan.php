<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Satuan extends CI_Model {

		public function get_satuan()
	{
		$this->db->select('*');
		$this->db->from('jenis_satuan');
		return $this->db->get();
	}

	public function insert_satuan($data)
	{
    	$this->db->insert('jenis_satuan', $data); 
	}

	public function get_satuan_by($id_jns_satuan)
	{
		$this->db->select('*');
		$this->db->from('jenis_satuan');
		$this->db->where('id_jns_satuan', $id_jns_satuan);
		return $this->db->get()->row_array();
	}

	public function update_satuan($id_jns_satuan,$data)
	{
		$this->db->where('id_jns_satuan', $id_jns_satuan);
		return $this->db->update('jenis_satuan',$data); 
	}

	public function delete_satuan($id_jns_satuan)
	{
		$this->db->where('id_jns_satuan', $id_jns_satuan);
		$this->db->delete('jenis_satuan');
	}

	function buat_kode()   
	{    
			$this->db->select('RIGHT(jenis_satuan.id_jns_satuan,3) as kode', FALSE);
			$this->db->order_by('id_jns_satuan','DESC');    
			$this->db->limit(1);    
			$query = $this->db->get('jenis_satuan');      //cek dulu apakah ada sudah ada kode di tabel.    
				if($query->num_rows() <> 0)
				{      
		   		//jika kode ternyata sudah ada.      
			 		$data = $query->row();      
			 		$kode = intval($data->kode) + 1;    
				}
				else{      
			   //jika kode belum ada      
			 		$kode = 1;    
				}
			$kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			$kodejadi = "JS".$kodemax;    
			return $kodejadi;  
	}
}