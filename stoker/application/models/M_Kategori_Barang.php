<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Kategori_Barang extends CI_Model {

		public function get_kategori()
	{
		$this->db->select('*');
		$this->db->from('kategori_barang');
		return $this->db->get();
	}

	public function insert_kategori($data)
	{
    	$this->db->insert('kategori_barang', $data); 
	}

	public function get_kategori_by($id_kategori)
	{
		$this->db->select('*');
		$this->db->from('kategori_barang');
		$this->db->where('id_kategori', $id_kategori);
		return $this->db->get()->row_array();
	}

	public function update_kategori($id_kategori,$data)
	{
		$this->db->where('id_kategori', $id_kategori);
		return $this->db->update('kategori_barang',$data); 
	}

	public function delete_kategori($id_kategori)
	{
		$this->db->where('id_kategori', $id_kategori);
		$this->db->delete('kategori_barang');
	}

	function buat_kode()   
	{    
			$this->db->select('RIGHT(kategori_barang.id_kategori,3) as kode', FALSE);
			$this->db->order_by('id_kategori','DESC');    
			$this->db->limit(1);    
			$query = $this->db->get('kategori_barang');      //cek dulu apakah ada sudah ada kode di tabel.    
				if($query->num_rows() <> 0)
				{      
		   		//jika kode ternyata sudah ada.      
			 		$data = $query->row();      
			 		$kode = intval($data->kode) + 1;    
				}
				else{      
			   //jika kode belum ada      
			 		$kode = 1;    
				}
			$kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			$kodejadi = "K".$kodemax;    
			return $kodejadi;  
	}
}