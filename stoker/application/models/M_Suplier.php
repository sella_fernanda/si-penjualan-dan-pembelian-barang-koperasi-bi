<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Suplier extends CI_Model {

		public function get_suplier()
	{
		$this->db->select('*');
		$this->db->from('suplier');
		return $this->db->get();
	}

	public function insert_suplier($data)
	{
    	$this->db->insert('suplier', $data); 
	}

	public function get_suplier_by($id_suplier)
	{
		$this->db->select('*');
		$this->db->from('suplier');
		$this->db->where('id_suplier', $id_suplier);
		return $this->db->get()->row_array();
	}

	public function update_suplier($id_suplier,$data)
	{
		$this->db->where('id_suplier', $id_suplier);
		return $this->db->update('suplier',$data); 
	}

	public function delete_suplier($id_suplier)
	{
		$this->db->where('id_suplier', $id_suplier);
		$this->db->delete('suplier');
	}

	function buat_kode()   
	{    
			$this->db->select('RIGHT(suplier.id_suplier,3) as kode', FALSE);
			$this->db->order_by('id_suplier','DESC');    
			$this->db->limit(1);    
			$query = $this->db->get('suplier');      //cek dulu apakah ada sudah ada kode di tabel.    
				if($query->num_rows() <> 0)
				{      
		   		//jika kode ternyata sudah ada.      
			 		$data = $query->row();      
			 		$kode = intval($data->kode) + 1;    
				}
				else{      
			   //jika kode belum ada      
			 		$kode = 1;    
				}
			$kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			$kodejadi = "S".$kodemax;    
			return $kodejadi;  
	}
}