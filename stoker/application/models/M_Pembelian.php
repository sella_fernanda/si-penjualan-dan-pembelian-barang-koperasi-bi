<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pembelian extends CI_Model {

	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('trans_pembelian');
        $this->db->join('suplier', 'suplier.id_suplier = trans_pembelian.id_suplier');
        $this->db->join('jenis_pembayaran', 'jenis_pembayaran.id_jns_pembayaran = trans_pembelian.id_jns_pembayaran');
        $this->db->join('pegawai', 'pegawai.nip = trans_pembelian.nip');
        $this->db->order_by('no_pembelian', "desc");
        return $this->db->get();
    }

    public function get_by($no_pembelian)
    {
        $this->db->select('*');
        $this->db->from('detail_pembelian');   
        $this->db->where('no_pembelian', $no_pembelian);
        return $this->db->get();
    }

    public function tampil_input_suplier($id)
    {
        $this->db->select('nama_suplier');
        $this->db->from('suplier');
        $this->db->where('id_suplier', $id);
        return $this->db->get();
    }

    public function tampil_input_bayar($id)
    {
        $this->db->select('nama_pembayaran');
        $this->db->from('jenis_pembayaran');
        $this->db->where('id_jns_pembayaran', $id);
        return $this->db->get();
    }

    public function tampil_input_kategori($id)
    {
        $this->db->select('nama_kategori');
        $this->db->from('kategori_barang');
        $this->db->where('id_kategori', $id);
        return $this->db->get();
    }

    public function tampil_input_barang($id)
    {
        $this->db->select('nama_barang');
        $this->db->from('barang');
        $this->db->where('no_barcode', $id);
        return $this->db->get();
    }

    public function getDataBarang()
    {
        return $this->db->get('barang'); 
    }

    public function getJenisPembayaran()
    {
        $this->db->select('*');
        $this->db->from('jenis_pembayaran');
        $this->db->where('nama_pembayaran !=','piutang');
        return $this->db->get(); 
    }

    public function getDataSuplier()
    {
        return $this->db->get('suplier'); 
    }

    public function getTotalBarang($no_barcode)
    {
        $this->db->select('stok_total');
        $this->db->from('barang');
        $this->db->where('no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->row();
        return $query->stok_total;
    }

    public function getJumlahbeli($no_pembelian,$no_barcode)
    {
        $this->db->select('jumlah_beli');
        $this->db->from('detail_pembelian');
        $this->db->where('no_pembelian', $no_pembelian);
        $this->db->where('no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->row();
        return $query->jumlah_beli;
    }

    public function getHarga($no_barcode)
    {
        $this->db->select('harga_jual');
        $this->db->from('harga');
        $this->db->where('no_barcode', $no_barcode);
        $this->db->order_by('id_harga', 'asc','LIMIT 1');
        $query =$this->db->get();
        $query = $query->row();
        return $query->harga_jual;
    }

    public function getRataHpp($no_barcode)
    {
        $this->db->select_avg('hpp');
        $this->db->from('detail_pembelian');
        $this->db->where('no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->row();
        return $query->hpp;
    }

    public function getJumlahHpp($no_barcode)
    {           
        $this->db->select_sum('hpp');
        $this->db->from('detail_pembelian');
        $this->db->where('no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->row();
        return $query->hpp;
    }

    public function getjumlahlisthpp($no_barcode)
    {           
        $this->db->select('no_barcode');
        $this->db->from('detail_pembelian');
        $this->db->where('no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->result_array();
        return count($query);
    }

    public function getKet($no_barcode)
    {
        $data['keterangan']='Tidak Berlaku';
        $this->db->where('no_barcode',$no_barcode);
        $this->db->update('harga',$data);
    }

    public function insert_trans($data)
    {
        $this->db->insert('trans_pembelian', $data); 
    }

    public function insert_detail($data,$no_barcode,$jumlah_beli)
    {
        $this->db->insert('detail_pembelian', $data);

        $stok_ttl =$this->getTotalBarang($no_barcode);
        $stok_beli = $jumlah_beli;
        $hasil = ($stok_ttl + $stok_beli) ;
        
        $data1['stok_total']= $hasil;

        $this->db->where('no_barcode',$no_barcode);
        $this->db->update('barang',$data1);

        if ($this->getHarga($no_barcode)==0) {
            $rata_hpp=$this->getRataHpp($no_barcode);
            $rata_baru=$rata_hpp + (10/100*$rata_hpp);

            $this->getKet($no_barcode);
            $tgl_ubah=date('Y-m-d h:i:s');
            $data3['harga_jual']=$rata_baru;
            $data3['keterangan']='Berlaku';
            $data3['no_barcode']=$no_barcode;
            $data3['tgl_ubah']=$tgl_ubah;

            $this->db->insert('harga', $data3);
        }else{
            $harga=$this->getHarga($no_barcode);
            $jml_hpp=$this->getJumlahHpp($no_barcode);
            $jml_list_hpp=$this->getjumlahlisthpp($no_barcode) + 1;
            $rata_hpp=($harga+$jml_hpp)/$jml_list_hpp;
            $rata_baru = $rata_hpp + (10/100*$rata_hpp);

            $this->getKet($no_barcode);
            $tgl_ubah=date('Y-m-d h:i:s');
            $data3['harga_jual']=$rata_baru;
            $data3['keterangan']='Berlaku';
            $data3['no_barcode']=$no_barcode;
            $data3['tgl_ubah']=$tgl_ubah;

            $this->db->insert('harga', $data3);
        }
    }

    public function update($no_pembelian,$no_barcode)
    {
        $stok_ttl=$this->getTotalBarang($no_barcode);
        $stok_beli=$this->getJumlahbeli($no_pembelian,$no_barcode);
        $hasil=($stok_ttl - $stok_beli) ;
        
        $data1['stok_total']= $hasil;

        $this->db->where('no_barcode',$no_barcode);
        $this->db->update('barang',$data1);

    }

    public function delete($no_pembelian)
    {
        $this->db->where('no_pembelian', $no_pembelian);
        $this->db->delete('trans_pembelian');
    }

    public function update_harga($no_barcode)
    {
        if ($this->getHarga($no_barcode)==0) {
            $rata_hpp=$this->getRataHpp($no_barcode);
            $rata_baru=$rata_hpp + (10/100*$rata_hpp);

            $this->getKet($no_barcode);
            $tgl_ubah=date('Y-m-d h:i:s');
            $data3['harga_jual']=$rata_baru;
            $data3['keterangan']='Berlaku';
            $data3['no_barcode']=$no_barcode;
            $data3['tgl_ubah']=$tgl_ubah;

            $this->db->insert('harga', $data3);
        }else{
            $harga=$this->getHarga($no_barcode);
            $jml_hpp=$this->getJumlahHpp($no_barcode);
            $jml_list_hpp=$this->getjumlahlisthpp($no_barcode) + 1;
            $rata_hpp=($harga+$jml_hpp)/$jml_list_hpp;
            $rata_baru = $rata_hpp + (10/100*$rata_hpp);

            $this->getKet($no_barcode);
            $tgl_ubah=date('Y-m-d h:i:s');
            $data3['harga_jual']=$rata_baru;
            $data3['keterangan']='Berlaku';
            $data3['no_barcode']=$no_barcode;
            $data3['tgl_ubah']=$tgl_ubah;

            $this->db->insert('harga', $data3);
        }
    }

    public function getDataKategori()
    {
        return $this->db->get('kategori_barang'); 
    }
    public function getIdKategori($id)
    {
        $this->db->where('id_kategori',$id);
        return $this->db->get('kategori_barang')->row()->nama_kategori; 
    }

    public function getIdBarang($id)
    {
        $this->db->where('no_barcode',$id);
        return $this->db->get('barang')->row()->nama_barang; 
    }

    public function get_subkategori($no_barcode){
        $hasil=$this->db->query("SELECT * FROM barang WHERE id_kategori='$no_barcode'");
        return $hasil->result();
    }

    public function detail($no_pembelian)
    {
        $this->db->select('*'); 
        $this->db->from('trans_pembelian');
        $this->db->join('detail_pembelian', 'detail_pembelian.no_pembelian = trans_pembelian.no_pembelian');
        $this->db->join('pegawai', 'trans_pembelian.nip = pegawai.nip');
        $this->db->join('barang', 'detail_pembelian.no_barcode = barang.no_barcode');
        $this->db->join('kategori_barang', 'detail_pembelian.id_kategori = kategori_barang.id_kategori');
        $query = $this->db->get_where('', array('trans_pembelian.no_pembelian' => $no_pembelian));
        return $query;
    }

    public function detail2($no_pembelian)
    {
        $this->db->select('*'); 
        $this->db->from('trans_pembelian');
        $this->db->join('detail_pembelian', 'detail_pembelian.no_pembelian = trans_pembelian.no_pembelian');
        $this->db->join('pegawai', 'trans_pembelian.nip = pegawai.nip');
        $this->db->join('barang', 'detail_pembelian.no_barcode = barang.no_barcode');
        $this->db->join('kategori_barang', 'detail_pembelian.id_kategori = kategori_barang.id_kategori');
        $query = $this->db->get_where('', array('trans_pembelian.no_pembelian' => $no_pembelian));
        return $query->result();
    }

    public function buat_kode()   
    {    
        $this->db->select('RIGHT(trans_pembelian.no_pembelian,3) as kode', FALSE);
        $this->db->order_by('no_pembelian','DESC');    
        $this->db->limit(1);    
            $query = $this->db->get('trans_pembelian');      //cek dulu apakah ada sudah ada kode di tabel.    
            if($query->num_rows() <> 0)
            {      
                //jika kode ternyata sudah ada.      
                $data = $query->row();      
                $kode = intval($data->kode) + 1;    
            }
            else{      
               //jika kode belum ada      
                $kode = 1;    
            }
            $kodemax = str_pad($kode, 8, "0", STR_PAD_LEFT);    
            $kodejadi = "PB".$kodemax;    
            return $kodejadi;  
        }

        public function get_laporan_pembelian($tgl_mulai,$tgl_selesai)
        {
            $this->db->select('*');
            $this->db->from('trans_pembelian');
            $this->db->join('jenis_pembayaran', 'trans_pembelian.id_jns_pembayaran = jenis_pembayaran.id_jns_pembayaran');
            $this->db->join('pegawai', 'trans_pembelian.nip = pegawai.nip');
            $this->db->join('suplier', 'trans_pembelian.id_suplier = suplier.id_suplier');
            $this->db->where('tgl_suply >=', $tgl_mulai);
            $this->db->where('tgl_suply <=', $tgl_selesai);
            return $this->db->get()->result();
        }

        public function get_detail_pembelian($no_pembelian){
            $this->db->select('no_pembelian,harga_beli,jumlah_beli');
            $this->db->from('detail_pembelian');
            $this->db->where('no_pembelian', $no_pembelian);
            return $this->db->get()->result();
        }

        public function get_laporan_hutang($tgl_mulai,$tgl_selesai)
        {
            $this->db->select('*');
            $this->db->from('trans_pembelian');
            $this->db->join('jenis_pembayaran', 'trans_pembelian.id_jns_pembayaran = jenis_pembayaran.id_jns_pembayaran');
            $this->db->join('suplier', 'trans_pembelian.id_suplier = suplier.id_suplier');
            $this->db->join('pegawai', 'trans_pembelian.nip = pegawai.nip');
            $this->db->where('trans_pembelian.id_jns_pembayaran =','P002');
            $this->db->where('tgl_suply >=', $tgl_mulai);
            $this->db->where('tgl_suply <=', $tgl_selesai);
            $this->db->order_by('trans_pembelian.id_suplier');
            return $this->db->get()->result();
        }
    }