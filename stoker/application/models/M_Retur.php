<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Retur extends CI_Model {

	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('retur');
        $this->db->join('trans_pembelian', 'retur.no_pembelian = trans_pembelian.no_pembelian');
        $this->db->join('barang', 'retur.no_barcode = barang.no_barcode');
        $this->db->order_by('retur.id_retur', "desc");
		return $this->db->get();
	}

	public function getBarang($id)
	{
		$this->db->select('*');
		$this->db->from('barang');
		return $this->db->get();
	}

	public function get_by($id_retur)
	{
		$this->db->select('*');
		$this->db->from('retur');	
		$this->db->where('id_retur', $id_retur);
		return $this->db->get()->row_array();
	}

	public function getJumlahbeli($no_pembelian,$no_barcode)
	{
		$this->db->select('jumlah_beli');
		$this->db->from('detail_pembelian');
		$this->db->where('no_pembelian', $no_pembelian);
		$this->db->where('no_barcode', $no_barcode);
		$query =$this->db->get();
		$query = $query->row();
		return $query->jumlah_beli;
	}

	public function getJumlahretur($id_retur)
	{
		$this->db->select('jumlah');
		$this->db->from('retur');
		$this->db->where('id_retur', $id_retur);
		$query =$this->db->get();
		$query = $query->row();
		return $query->jumlah;
	}

	public function getTotalBarang($no_barcode)
    {
        $this->db->select('stok_total');
        $this->db->from('barang');
        $this->db->where('no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->row();
        return $query->stok_total;
    }

	public function insert_retur_uang($no_pembelian,$no_barcode,$data)
	{
		$this->db->insert('retur', $data);

		$jml=$this->getJumlahbeli($no_pembelian,$no_barcode);
        $retur=$this->input->post('jumlah');
        $hasil=($jml - $retur) ;
        
        $data1['jumlah_beli']= $hasil;

        $this->db->where('no_pembelian',$no_pembelian);
        $this->db->where('no_barcode',$no_barcode);
        $this->db->update('detail_pembelian',$data1);

        $stok_ttl=$this->getTotalBarang($no_barcode);
        $total=($stok_ttl - $retur) ;
        
        $data2['stok_total']= $total;

        $this->db->where('no_barcode',$no_barcode);
        $this->db->update('barang',$data2);

        /*echo "<pre>";
		print_r($jml);
		echo "<pre>";
		echo "<pre>";
		print_r($retur);
		echo "<pre>";
		echo "<pre>";
		print_r($hasil);
		echo "<pre>";
		echo "<pre>";
		print_r($stok_ttl);
		echo "<pre>";
		echo "<pre>";
		print_r($jml);
		echo "<pre>";
		echo "<pre>";
		print_r($total);
		echo "<pre>";
		exit();*/


	}

	public function insert_retur_barang($data)
	{
		$this->db->insert('retur', $data);
		/*echo "<pre>";
		print_r($data);
		echo "<pre>";
		exit();*/
	}

	public function batal_barang($id_retur)
	{
		$this->db->where('id_retur', $id_retur);
		$this->db->delete('retur');
	}

    public function batal_uang($id_retur,$no_pembelian,$no_barcode)
	{
		$jmlbeli=$this->getJumlahbeli($no_pembelian,$no_barcode);
        $retur=$this->getJumlahretur($id_retur);
        $hasil=($jmlbeli + $retur) ;
        
        $data1['jumlah_beli']= $hasil;

        $this->db->where('no_pembelian',$no_pembelian);
        $this->db->where('no_barcode',$no_barcode);
        $this->db->update('detail_pembelian',$data1);

        $stok_ttl=$this->getTotalBarang($no_barcode);
        $total=($stok_ttl + $retur) ;
        
        $data2['stok_total']= $total;

        $this->db->where('no_barcode',$no_barcode);
        $this->db->update('barang',$data2);

		$this->db->where('id_retur', $id_retur);
		$this->db->delete('retur');
	}
}