<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Pembelian extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('M_Pembelian'));
	}

	//List Dropdown retur barang
	public function get_pembelian_list($no_pembelian)
	{
		$data['query2'] = $this->M_Pembelian->detail2($no_pembelian);
		foreach ($data['query2'] as $baris) {
			echo "<option value='".$baris->no_barcode."'>".$baris->nama_barang."</option>";
		}
	}

	public function list_pembelian()
	{
		$data['pembelian'] = $this->M_Pembelian->get_list();
		$data['controller'] = $this; 
		$this->template->set('title', 'Data Transaksi Pembelian');
		$this->template->load('adminLTE', 'contents' , 'view_pembelian/list_pembelian', $data);
	}

	public function input_pembelian()
	{
		$this->form_validation->set_Rules('tgl_suply','Tanggal Suply','required');
		$this->form_validation->set_Rules('jumlah_beli','Jumlah Stok','required');
		$this->form_validation->set_Rules('harga_beli','Harga Beli','required');
		$this->form_validation->set_Rules('diskon','Diskon','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		//jika menyimpan
		if($this->form_validation->run() == TRUE) {
			if ($this->input->post()) {
				if ($this->session->has_userdata('no_pembelian')) {
					$data=$this->session->userdata();
					$this->session->unset_userdata('id_pembelian');
					$this->session->unset_userdata('no_pembelian');
					$this->session->unset_userdata('tgl_suply');
					$this->session->unset_userdata('id_suplier');
					$this->session->unset_userdata('id_jns_pembayaran');
					$this->session->unset_userdata('barang');
					$barang=array(
						'id_kategori' => $this->input->post('id_kategori'), 
						'no_barcode' => $this->input->post('no_barcode'), 
						'jumlah_beli' => $this->input->post('jumlah_beli'), 
						'harga_beli' => $this->input->post('harga_beli'), 
						'ppn' => $this->input->post('ppn'), 
						'diskon' => $this->input->post('diskon'),
						'hpp' => $this->input->post('hpp'),
					);
					//menyimpan session dengan bentuk array/multi array
					$data['barang'][]=$barang;
					$this->session->set_userdata($data);

				}else{
					$sup = $this->M_Pembelian->tampil_input_suplier($this->input->post('id_suplier'))->result_array();
					$byr = $this->M_Pembelian->tampil_input_bayar($this->input->post('id_jns_pembayaran'))->result_array();
					$data1 = array('suplier' => $sup[0]['nama_suplier'], 
						'jenis_pembayaran' => $byr[0]['nama_pembayaran'],
					);
					$data = array(
						'id_pembelian' => $this->input->post('id_pembelian'),
						'no_pembelian' => $this->input->post('no_pembelian'),  
						'tgl_suply' => $this->input->post('tgl_suply'), 
						'id_suplier' => $this->input->post('id_suplier'), 
						'id_jns_pembayaran' => $this->input->post('id_jns_pembayaran'),
						'barang' => array(
							array(
								'no_barcode' => $this->input->post('no_barcode'), 
								'id_kategori' => $this->input->post('id_kategori'), 
								'jumlah_beli' => $this->input->post('jumlah_beli'), 
								'harga_beli' => $this->input->post('harga_beli'), 
								'ppn' => $this->input->post('ppn'), 
								'diskon' => $this->input->post('diskon'),
								'hpp' => $this->input->post('hpp'),  
							)
						), 
					);
					$this->session->set_userdata($data);
					$this->session->set_userdata($data1);
				}
			}
			redirect(site_url('stoker/C_Pembelian/input_pembelian'));

		}else{
			//jika tidak menyimpan
			$data['query4']=$this->M_Pembelian->getDataKategori();
			$data['query']=$this->M_Pembelian->getDataBarang();
			$data['query2']=$this->M_Pembelian->getJenisPembayaran();
			$data['query3']=$this->M_Pembelian->getDataSuplier();
			$data['kode_pembelian'] = $this->M_Pembelian->buat_kode();

			$this->template->set('title', 'Transaksi Pembelian');
			$this->template->load('adminLTE', 'contents' , 'view_pembelian/input_pembelian', $data);
		}
	}

	public function input_biaya($total_jumlah)
	{
		if (!$this->session->userdata('barang')) {
			redirect('stoker/C_Pembelian/input_pembelian');
		}else{
			$total_jumlah = $total_jumlah;
			$this->session->set_userdata('total_jumlah',$total_jumlah);	
			if ($this->input->post()) {
				$data=$this->session->userdata();
				$this->session->unset_userdata('transport');
				$this->session->unset_userdata('angkut');
				$this->session->unset_userdata('biaya_lain');

				//hitung hpp baru
				$biaya_jadi=$this->input->post('biaya_jadi');
				foreach ($data['barang'] as $key => $value) {
					//replace hpp lama dg hpp baru
					$hpp=(int)preg_replace('/\D/','', $value['hpp']) +$biaya_jadi;
					$data['barang'][$key]['hpp']=$hpp;
				}
				$this->session->set_userdata($data);
				$this->session->set_userdata('hpp_new',$hpp);

				//set session untuk menampilkan data di tabel info
				$data3 = array(
					'transport' => $this->input->post('transport'),
					'angkut' => $this->input->post('angkut'),  
					'biaya_lain' => $this->input->post('biaya_lain')
				); 
				$this->session->set_userdata($data3);

			}
			$this->template->set('title', 'Transaksi Pembelian');
			$this->template->load('adminLTE', 'contents' , 'view_pembelian/input_biaya');
		}
	}	

	public function simpan()
	{
		$nip=$this->session->userdata('nip');
		$id_pembelian=$this->session->userdata('id_pembelian');
		$no_pembelian=$this->session->userdata('no_pembelian');
		$tgl_suply=$this->session->userdata('tgl_suply');
		$id_suplier=$this->session->userdata('id_suplier');
		$id_jns_pembayaran=$this->session->userdata('id_jns_pembayaran');
		$transport=$this->session->userdata('transport');
		$angkut=$this->session->userdata('angkut');
		$biaya_lain=$this->session->userdata('biaya_lain');
		$barang = $this->session->userdata('barang');

		$temp_tgl_suply = date_create($tgl_suply);
		$tgl_suply_format = date_format($temp_tgl_suply,'Y-m-d');

		$trans = array(
			'no_pembelian'=> $no_pembelian,
			'tgl_suply'=> $tgl_suply_format,
			'id_suplier'=> $id_suplier,
			'id_jns_pembayaran'=> $id_jns_pembayaran,
			'nip'=> $nip,
			'transportasi'=> $transport,
			'angkut'=> $angkut,
			'biaya_lain'=> $biaya_lain
		);
		$this->M_Pembelian->insert_trans($trans);

		$data_barang=$this->session->userdata('barang');
		$i = 0;
		foreach ($data_barang as $brg) {
			$hpp = $this->session->barang[$i]['hpp'];
			$detail[] = array(
				'id_pembelian'=> $id_pembelian,
				'no_pembelian'=> $no_pembelian,
				'id_kategori'=> $brg['id_kategori'],
				'no_barcode'=> $brg['no_barcode'],
				'harga_beli'=> $brg['harga_beli'],
				'ppn'=> $brg['ppn'],
				'diskon'=> $brg['diskon'],
				'jumlah_beli'=> $brg['jumlah_beli'],
				'hpp'=> $hpp,
			);

			$no_barcode[]=$brg['no_barcode'];
			$jumlah_beli[]=$brg['jumlah_beli'];
			$this->M_Pembelian->insert_detail($detail[$i],$no_barcode[$i],$jumlah_beli[$i]);
			$i++;
		}
		$this->session->unset_userdata('id_pembelian');
		$this->session->unset_userdata('no_pembelian');
		$this->session->unset_userdata('tgl_suply');
		$this->session->unset_userdata('suplier');
		$this->session->unset_userdata('jenis_pembayaran');
		$this->session->unset_userdata('barang');
		$this->session->unset_userdata('transport');
		$this->session->unset_userdata('angkut');
		$this->session->unset_userdata('biaya_lain');
		$barang=array(
			'id_kategori' => $this->input->post('id_kategori'), 
			'no_barcode' => $this->input->post('no_barcode'), 
			'jumlah_beli' => $this->input->post('jumlah_beli'), 
			'harga_beli' => $this->input->post('harga_beli'), 
			'ppn' => $this->input->post('ppn'), 
			'diskon' => $this->input->post('diskon'),
			'hpp' => $this->input->post('hpp'),
		);
		redirect(site_url('stoker/C_Pembelian/list_pembelian'));	
	}

	public function reset(){
		$this->session->unset_userdata('id_pembelian');
		$this->session->unset_userdata('no_pembelian');
		$this->session->unset_userdata('tgl_suply');
		$this->session->unset_userdata('suplier');
		$this->session->unset_userdata('jenis_pembayaran');
		$this->session->unset_userdata('barang');
		$this->session->unset_userdata('transport');
		$this->session->unset_userdata('angkut');
		$this->session->unset_userdata('biaya_lain');
		$barang=array(
			'id_kategori' => $this->input->post('id_kategori'), 
			'no_barcode' => $this->input->post('no_barcode'), 
			'jumlah_beli' => $this->input->post('jumlah_beli'), 
			'harga_beli' => $this->input->post('harga_beli'), 
			'ppn' => $this->input->post('ppn'), 
			'diskon' => $this->input->post('diskon'),
			'hpp' => $this->input->post('hpp'),
		);
		redirect(base_url('stoker/C_Pembelian/input_pembelian'));
	}

	//hapus data tabel session 
	public function hapus($no_barcode){
		$list_barang=$this->session->userdata('barang');
		$this->session->unset_userdata('barang');
		unset($list_barang[$no_barcode]);
		$this->session->set_userdata('barang',$list_barang);
		redirect(base_url('stoker/C_Pembelian/input_pembelian'));
	} 

	//menampilkan list barang sesuai kategori
	public function get_subkategori(){
		$no_barcode=$this->input->post('no_barcode');
		$data=$this->M_Pembelian->get_subkategori($no_barcode);
		echo json_encode($data);
	}

	public	function detail($no_pembelian)
	{	
		$this->template->set('title', 'Detail Pembelian');

		$query = $this->M_Pembelian->detail($no_pembelian);
		$data['query'] = $query;
		$this->template->load('adminLTE', 'contents' , 'view_pembelian/detail_pembelian', $data);
	}	

	//hapus transaksi
	public	function delete($no_pembelian)
	{	
		$pembelian = $this->M_Pembelian->get_by($no_pembelian);
		$i=0;
		foreach($pembelian->result_array() as $value) {
			$no_barcode= $value['no_barcode'];

			$this->M_Pembelian->update($no_pembelian,$no_barcode);
			$i++;
		}
		
		$this->M_Pembelian->delete($no_pembelian);

		foreach($pembelian->result_array() as $value) {
			$no_barcode= $value['no_barcode'];

			$this->M_Pembelian->update_harga($no_barcode);
			$i++;
		}

		redirect(site_url('stoker/C_Pembelian/list_pembelian'));
	}

	//form cari laporan
	public function laporan_pembelian()
	{      
		$temp_tgl_mulai = date_create($this->input->get('tgl_mulai'));
		$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
		$temp_tgl_selesai = date_create($this->input->get('tgl_selesai'));
		$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

        // membedakan sebelum dan sesudah isi filter data
		if ($this->input->get('tgl_mulai') == '' && $this->input->get('tgl_selesai') == '') 
		{
            // kondisi ketika pertama buka, belum filter data kosong
			$data['tgl_mulai']=null;
			$data['tgl_selesai']=null;
			$data['laporan'] = null;
		}else
		{
            // jika sudah ngisi form filter
			$data['tgl_mulai']=$tgl_mulai;
			$data['tgl_selesai']=$tgl_selesai;

			$data['laporan']=$this->M_Pembelian->get_laporan_pembelian($data['tgl_mulai'], $data['tgl_selesai']);
		}
		$this->template->set('title','Laporan Pembelian');
		$this->template->load('adminLTE', 'contents' , 'view_pembelian/laporan_pembelian', $data);
	}

	public function cetak_laporan_word($tgl_mulai, $tgl_selesai)
	{
		header("Content-type: application/vnd.ms-word");
		header("Content-Disposition: attachment;Filename=lap_pembelian__".$tgl_mulai."-".$tgl_selesai.".doc");

		$temp_tgl_mulai = date_create($tgl_mulai);
		$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
		$temp_tgl_selesai = date_create($tgl_selesai);
		$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

		$data = array(
			'tgl_mulai' => $tgl_mulai,
			'tgl_selesai' => $tgl_selesai,
			'laporan'=>$this->M_Pembelian->get_laporan_pembelian($tgl_mulai, $tgl_selesai),
			'start' => 0
		);

		$this->load->view('view_pembelian/cetak_laporan_pembelian',$data);
	}

	public function cetak_laporan_pdf($tgl_mulai, $tgl_selesai)
	{
		$temp_tgl_mulai = date_create($tgl_mulai);
		$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
		$temp_tgl_selesai = date_create($tgl_selesai);
		$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');
		$data = array(
			'tgl_mulai' => $tgl_mulai,
			'tgl_selesai' => $tgl_selesai,
			'laporan'=>$this->M_Pembelian->get_laporan_pembelian($tgl_mulai, $tgl_selesai),
			'start' => 0
		);

		$this->load->view('view_pembelian/cetak_laporan_pembelian',$data);
	}

	public function cetak_laporan_excel($tgl_mulai, $tgl_selesai)
	{
		header("Content-type: application/vnd.ms.excel");

		header("Content-Disposition: attachment;Filename=laporan_pembelian_".$tgl_mulai."-".$tgl_selesai.".xls");

		$temp_tgl_mulai = date_create($tgl_mulai);
		$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
		$temp_tgl_selesai = date_create($tgl_selesai);
		$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

		$data = array(
			'tgl_mulai' => $tgl_mulai,
			'tgl_selesai' => $tgl_selesai,
			'laporan'=>$this->M_Pembelian->get_laporan_pembelian($tgl_mulai, $tgl_selesai),
			'start' => 0
		);
		$this->load->view('view_pembelian/cetak_laporan_pembelian',$data);
	}

	public function laporan_hutang()
	{      
		$temp_tgl_mulai = date_create($this->input->get('tgl_mulai'));
		$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
		$temp_tgl_selesai = date_create($this->input->get('tgl_selesai'));
		$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

        // membedakan sebelum dan sesudah isi filter data
		if ($this->input->get('tgl_mulai') == '' && $this->input->get('tgl_selesai') == '') 
		{
            // kondisi ketika pertama buka, belum filter data kosong
			$data['tgl_mulai']=null;
			$data['tgl_selesai']=null;
			$data['hutang'] = [];
		}else
		{
            // jika sudah ngisi form filter
			$data['tgl_mulai']=$tgl_mulai;
			$data['tgl_selesai']=$tgl_selesai;

			$data['hutang']=$this->M_Pembelian->get_laporan_hutang($tgl_mulai, $tgl_selesai);
		}
		$this->template->set('title','Laporan hutang');
		$this->template->load('adminLTE', 'contents' , 'view_pembelian/laporan_hutang', $data);
	}

	public function cetak_laporan_hutang_word($tgl_mulai, $tgl_selesai)
	{
		header("Content-type: application/vnd.ms-word");
		header("Content-Disposition: attachment;Filename=laporan_hutang_".$tgl_mulai."-".$tgl_selesai.".doc");

		$temp_tgl_mulai = date_create($tgl_mulai);
		$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
		$temp_tgl_selesai = date_create($tgl_selesai);
		$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

		$data = array(
			'tgl_mulai' => $tgl_mulai,
			'tgl_selesai' => $tgl_selesai,
			'hutang' => $this->M_Pembelian->get_laporan_hutang($tgl_mulai, $tgl_selesai),
			'start' => 0
		);

		$this->load->view('view_pembelian/cetak_laporan_hutang',$data);
	}

	public function cetak_laporan_hutang_pdf($tgl_mulai, $tgl_selesai)
	{
		$temp_tgl_mulai = date_create($tgl_mulai);
		$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
		$temp_tgl_selesai = date_create($tgl_selesai);
		$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

		$data = array(
			'tgl_mulai' => $tgl_mulai,
			'tgl_selesai' => $tgl_selesai,
			'hutang' => $this->M_Pembelian->get_laporan_hutang($tgl_mulai, $tgl_selesai),
			'start' => 0
		);

		$this->load->view('view_pembelian/cetak_laporan_hutang',$data);
	}

	public function cetak_laporan_hutang_excel($tgl_mulai, $tgl_selesai)
	{
		header("Content-type: application/vnd.ms.excel");

		header("Content-Disposition: attachment;Filename=laporan_hutang_".$tgl_mulai."-".$tgl_selesai.".xls");

		$temp_tgl_mulai = date_create($tgl_mulai);
		$tgl_mulai = date_format($temp_tgl_mulai,'Y-m-d');
		$temp_tgl_selesai = date_create($tgl_selesai);
		$tgl_selesai = date_format($temp_tgl_selesai,'Y-m-d');

		$data = array(
			'tgl_mulai' => $tgl_mulai,
			'tgl_selesai' => $tgl_selesai,
			'hutang' => $this->M_Pembelian->get_laporan_hutang($tgl_mulai, $tgl_selesai),
			'start' => 0
		);
		$this->load->view('view_pembelian/cetak_laporan_hutang',$data);
	}

}
