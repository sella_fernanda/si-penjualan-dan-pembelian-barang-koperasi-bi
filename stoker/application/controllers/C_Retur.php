<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Retur extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Retur');
	}

	public function list_retur()
	{
		$data['retur'] = $this->M_Retur->get_list();

		$this->template->set('title', 'Data Retur');
		$this->template->load('adminLTE', 'contents' , 'view_retur/list_retur', $data);
	}

	//menerima data jenis retur yg dipilih
	public function ambil_jenis_retur(){
		$this->session->set_userdata('jenis_retur', $this->input->post('jenis_retur'));
	}

	public function input_retur()
	{
		$jenis_retur = $this->session->userdata('jenis_retur');

		$data['id_retur'] = $this->input->post('id_retur');
		$data['no_pembelian'] = $this->input->post('no_pembelian');
		$data['no_barcode'] = $this->input->post('no_barcode');
		$data['jumlah'] = $this->input->post('jumlah');
		$data['keterangan'] = $jenis_retur;

		$no_pembelian = $this->input->post('no_pembelian');
		$no_barcode = $this->input->post('no_barcode');

		if ($jenis_retur == 'dikembalikan barang') {
			$this->M_Retur->insert_retur_barang($data);
		}else{
			$this->M_Retur->insert_retur_uang($no_pembelian,$no_barcode,$data);	
		}
		redirect(site_url('stoker/C_Retur/list_retur'));
	}

	//hapus transaksi retur
	public function batal($id_retur)
	{
		$retur = $this->M_Retur->get_by($id_retur); 
		$keterangan= $retur['keterangan'];

		if ($keterangan=='dikembalikan barang') {
			$this->M_Retur->batal_barang($id_retur);
		}else{
			$no_barcode= $retur['no_barcode'];
			$no_pembelian= $retur['no_pembelian'];

			$this->M_Retur->batal_uang($id_retur,$no_pembelian,$no_barcode);
		}

		redirect(site_url('stoker/C_Retur/list_retur'));
	}
	
}