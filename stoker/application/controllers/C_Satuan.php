<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Satuan extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Satuan');
	}

	public function list_satuan()
	{
		$data['satuan'] = $this->M_Satuan->get_satuan();

		$this->template->set('title', 'Data Jenis Satuan');
		$this->template->load('adminLTE', 'contents' , 'view_satuan/list_satuan', $data);
	}

	public function input_satuan()
	{
		$this->form_validation->set_rules('id_jns_satuan','ID Satuan','trim|required');
		$this->form_validation->set_rules('nama_satuan','Nama Satuan','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		if($this->form_validation->run() == TRUE) {
			$data['id_jns_satuan'] = $this->input->post('id_jns_satuan');
			$data['nama_satuan'] = $this->input->post('nama_satuan');

			$this->M_Satuan->insert_satuan($data);

			redirect(site_url('stoker/C_Satuan/list_satuan'));

		}else{
			//jika tidak menyimpan
			$data['kategori'] = $this->M_Satuan->get_satuan();
			$data['kodeunik'] = $this->M_Satuan->buat_kode();

			$this->template->set('title', 'Tambah Satuan Barang');
            $this->template->load('adminLTE', 'contents' , 'view_satuan/input_satuan', $data);
		}
	}

	public function edit_satuan($id_jns_satuan)
	{
		$this->form_validation->set_rules('id_jns_satuan','ID satuan','trim|required');
		$this->form_validation->set_rules('nama_satuan','Nama satuan','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		if($this->form_validation->run() == TRUE) {
			//jika menyimpan
			$data['id_jns_satuan'] = $this->input->post('id_jns_satuan');
			$data['nama_satuan'] = $this->input->post('nama_satuan');

			$this->M_Satuan->update_satuan($id_jns_satuan,$data);

			redirect(site_url('stoker/C_Satuan/list_satuan'));

		}else{
			//jika tidak menyimpan
			$satuan = $this->M_Satuan->get_satuan_by($id_jns_satuan); 

			$data['id_jns_satuan'] = $satuan['id_jns_satuan'];
			$data['nama_satuan'] = $satuan['nama_satuan'];

			$this->template->set('title', 'Edit Data Jenis Satuan');
            $this->template->load('adminLTE', 'contents' , 'view_satuan/edit_satuan', $data);
		}
	}

	public	function delete_satuan($id_jns_satuan)
	{
		$this->M_Satuan->delete_satuan($id_jns_satuan);

		redirect(site_url('stoker/C_Satuan/list_satuan'));
	}
}

