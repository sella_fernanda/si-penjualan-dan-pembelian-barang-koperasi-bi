<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Barang');
	}

	public function list_barang()
	{
		$data['barang'] = $this->M_Barang->get_list();

		$this->template->set('title', 'Data Barang');
		$this->template->load('adminLTE', 'contents' , 'view_barang/list_barang', $data);
	}

	public function input_barang()
	{
		$this->form_validation->set_rules('no_barcode','No Barcode','required');
		$this->form_validation->set_rules('nama_barang','Nama Barang','required');
		$this->form_validation->set_rules('stok_awal','Stok Awal','required');
		$this->form_validation->set_rules('harga_jual','Harga Jual','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		if($this->form_validation->run() == TRUE) {
			//jika menyimpan
			$config['upload_path']='./upload/';
			$config['allowed_types']='gif|jpg|JPG|JPEG|png|jpeg|bmp';
			$config['max_size']=0;

			$this->load->library('upload', $config);

			$this->upload->initialize($config);
			if(!$this->upload->do_upload('gambar'))
			{
				$gambar = $this->upload->data();
				$data['no_barcode'] = $this->input->post('no_barcode');
				$data['id_jns_satuan'] = $this->input->post('id_jns_satuan');
				$data['id_kategori'] = $this->input->post('id_kategori');
				$data['nama_barang'] = $this->input->post('nama_barang');
				$data['stok_awal'] = $this->input->post('stok_awal');
				$data['stok_total'] = $this->input->post('stok_awal');

				$no_barcode=$data['no_barcode'];

				$data1['id_harga'] = $this->input->post('id_harga');
				$data1['harga_jual'] = $this->input->post('harga_jual');
				$data1['keterangan'] = $this->input->post('keterangan');
				$data1['no_barcode'] = $this->input->post('no_barcode');
				$data1['tgl_ubah'] = $this->input->post('tgl_ubah');

				$this->M_Barang->insert($data,$data1,$no_barcode);

				redirect(site_url('stoker/C_Barang/list_barang'));
			}
			else{
				$gambar = $this->upload->data();
				$data['no_barcode'] = $this->input->post('no_barcode');
				$data['id_jns_satuan'] = $this->input->post('id_jns_satuan');
				$data['id_kategori'] = $this->input->post('id_kategori');
				$data['nama_barang'] = $this->input->post('nama_barang');
				$data['stok_awal'] = $this->input->post('stok_awal');
				$data['stok_total'] = $this->input->post('stok_awal');
				$data['gambar'] = $gambar['file_name'];

				$no_barcode=$data['no_barcode'];

				$data1['id_harga'] = $this->input->post('id_harga');
				$data1['harga_jual'] = $this->input->post('harga_jual');
				$data1['keterangan'] = $this->input->post('keterangan');
				$data1['no_barcode'] = $this->input->post('no_barcode');
				$data1['tgl_ubah'] = $this->input->post('tgl_ubah');

				$this->M_Barang->insert($data,$data1,$no_barcode);

				redirect(site_url('stoker/C_Barang/list_barang'));
			}

		}
		else{
				//jika tidak menyimpan
			$data['query']=$this->M_Barang->getDataKategori();
			$data['query2']=$this->M_Barang->getDataSatuan();
			$data['barang'] = $this->M_Barang->get_list();


			$this->template->set('title', 'Tambah Barang');
			$this->template->load('adminLTE', 'contents' , 'view_barang/input_barang', $data);
		}
	}

	public function edit($no_barcode)
	{
		$this->form_validation->set_rules('no_barcode','No Barcode','required');
		$this->form_validation->set_Rules('nama_barang','Nama Barang','required');
		$this->form_validation->set_Rules('stok_awal','Stok Awal','required');
		$this->form_validation->set_message('required', '*) Masukkan %s');

		if($this->form_validation->run() == TRUE) {
			//jika di klik simpan
			$config['upload_path']='./upload/';
			$config['allowed_types']='gif|jpg|JPG|JPEG|png|jpeg|bmp';
			$config['max_size']=0;

			$this->load->library('upload', $config);

			$this->upload->initialize($config);
			if(!$this->upload->do_upload('gambar'))
			{
				$gambar = $this->upload->data();
				$data['id_jns_satuan'] = $this->input->post('id_jns_satuan');
				$data['id_kategori'] = $this->input->post('id_kategori');
				$data['nama_barang'] = $this->input->post('nama_barang');
				$this->M_Barang->update($no_barcode,$data);

				redirect(site_url('stoker/C_Barang/list_barang'));
			}
			else{
				$ambil = $this->M_Barang->get($no_barcode)->row();
				$target_file='./upload/'.$ambil->gambar;
				unlink($target_file);

				$gambar = $this->upload->data();

				$data['id_jns_satuan'] = $this->input->post('id_jns_satuan');
				$data['id_kategori'] = $this->input->post('id_kategori');
				$data['nama_barang'] = $this->input->post('nama_barang');

				$data['gambar'] = $gambar['file_name'];

				$this->M_Barang->update($no_barcode,$data);

				redirect(site_url('stoker/C_Barang/list_barang'));
			}

		}else{

			$data['query']=$this->M_Barang->getDataKategori();
			$data['query1'] = $this->M_Barang->detail($no_barcode);
			$data['query2']=$this->M_Barang->getDataSatuan();

			$barang = $this->M_Barang->get_by($no_barcode); 

			$data['no_barcode'] = $barang['no_barcode'];
			$data['id_jns_satuan'] = $barang['id_jns_satuan'];
			$data['id_kategori'] = $barang['id_kategori'];
			$data['nama_barang'] = $barang['nama_barang'];
			$data['stok_awal'] = $barang['stok_awal'];
			$data['stok_total'] = $barang['stok_total'];
			$data['id_harga'] = $barang['id_harga'];
			$data['harga_jual'] = $barang['harga_jual'];
			$data['keterangan'] = $barang['keterangan'];
			$data['gambar'] = $barang['gambar'];

			$no_barcode= $barang['no_barcode'];

			$this->template->set('title','Edit Data Barang');
			$this->template->load('adminLTE', 'contents' , 'view_barang/edit_barang', $data);
		}
	}

	public	function detail($no_barcode)
	{	
		$this->template->set('title', 'Detail Data');

		$data['query'] = $this->M_Barang->detail($no_barcode);
		$data['harga_awal'] = $this->M_Barang->get_harga_awal($no_barcode);
		
		$this->template->load('adminLTE', 'contents' , 'view_barang/detail_barang', $data);

	}	

	public	function delete($no_barcode)
	{
		$this->M_Barang->delete($no_barcode);

		$ambil = $this->M_Barang->get($no_barcode)->row();
		$target_file='./upload/'.$ambil->gambar;
		unlink($target_file);

		redirect(site_url('stoker/C_Barang/list_barang'));
	}

}

