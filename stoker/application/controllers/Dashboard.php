<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Barang');
	}

	public function index()
	{
	//check_not_login();
     $this->template->set('title','HOME');
     $this->template->load('adminLTE2','contents','v_index2');
	}

	public function Toko()
	{
		//check_not_login();

		$queryPembelian  = $this->M_Barang->get_pembelian();
		$queryRetur= $this->M_Barang->get_retur();
		$queryLogistik= $this->M_Barang->get_logistik();
		$data['jml_pembelian']   = count($queryPembelian);
		$data['jml_retur']  = count($queryRetur);
		$data['jml_logistik']  = count($queryLogistik);
		$this->template->set('title', 'Dashboard');
		$this->template->load('adminLTE', 'contents' , 'v_index',$data);
	}

}

