<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Kategori_Barang extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Kategori_Barang');
	}

	public function list_kategori()
	{
		$data['kategori'] = $this->M_Kategori_Barang->get_kategori();

		$this->template->set('title', 'Data Kategori Barang');
		$this->template->load('adminLTE', 'contents' , 'view_kategori_barang/list_kategori', $data);
	}

	public function input_kategori()
	{
		$this->form_validation->set_rules('id_kategori','ID Kategori Barang','trim|required');
		$this->form_validation->set_rules('nama_kategori','Nama Kategori Barang','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		if($this->form_validation->run() == TRUE) {
			$data['id_kategori'] = $this->input->post('id_kategori');
			$data['nama_kategori'] = $this->input->post('nama_kategori');

			$this->M_Kategori_Barang->insert_kategori($data);

			redirect(site_url('stoker/C_Kategori_Barang/list_kategori'));

		}else{
			//jika tidak menyimpan
			$data['kategori'] = $this->M_Kategori_Barang->get_kategori();
			$data['kodeunik'] = $this->M_Kategori_Barang->buat_kode();

			$this->template->set('title', 'Tambah Kategori Barang');
            $this->template->load('adminLTE', 'contents' , 'view_kategori_barang/input_kategori', $data);
		}
	}

	public function edit_kategori($id_kategori)
	{
		$this->form_validation->set_rules('id_kategori','ID Kategori Barang','trim|required');
		$this->form_validation->set_rules('nama_kategori','Nama Kategori Barang','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		if($this->form_validation->run() == TRUE) {
			//jika menyimpan
			$data['id_kategori'] = $this->input->post('id_kategori');
			$data['nama_kategori'] = $this->input->post('nama_kategori');

			$this->M_Kategori_Barang->update_kategori($id_kategori, $data);

			redirect(site_url('stoker/C_Kategori_Barang/list_kategori'));

		}else{
			//jika tidak menyimpan
			$kategori = $this->M_Kategori_Barang->get_kategori_by($id_kategori); 

			$data['id_kategori'] = $kategori['id_kategori'];
			$data['nama_kategori'] = $kategori['nama_kategori'];

			$this->template->set('title', 'Edit Data Kategori Barang');
            $this->template->load('adminLTE', 'contents' , 'view_kategori_barang/edit_kategori', $data);
		}
	}

	public	function delete_kategori($id_kategori)
	{
		$this->M_Kategori_Barang->delete_kategori($id_kategori);

		redirect(site_url('stoker/C_Kategori_Barang/list_kategori'));
	}
}

