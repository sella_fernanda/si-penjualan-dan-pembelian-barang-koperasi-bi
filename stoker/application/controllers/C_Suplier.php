<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Suplier extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Suplier');
	}

	public function list_suplier()
	{
		$data['suplier'] = $this->M_Suplier->get_suplier();

		$this->template->set('title', 'Data Suplier');
		$this->template->load('adminLTE', 'contents' , 'view_suplier/list_suplier', $data);
	}

	public function input_suplier()
	{
		$this->form_validation->set_rules('id_suplier','ID Suplier','trim|required');
		$this->form_validation->set_rules('nama_suplier','Nama Suplier','required');
		$this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_rules('no_hp','No Telp','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		if($this->form_validation->run() == TRUE) {
			$data['id_suplier'] = $this->input->post('id_suplier');
			$data['nama_suplier'] = $this->input->post('nama_suplier');
			$data['alamat'] = $this->input->post('alamat');
			$data['no_hp'] = $this->input->post('no_hp');

			$this->M_Suplier->insert_suplier($data);

			redirect(site_url('stoker/C_Suplier/list_suplier'));

		}else{
			//jika tidak menyimpan
			$data['suplier'] = $this->M_Suplier->get_suplier();
			$data['kodeunik'] = $this->M_Suplier->buat_kode();

			$this->template->set('title', 'Tambah Suplier');
            $this->template->load('adminLTE', 'contents' , 'view_suplier/input_suplier', $data);
		}
	}

	public function edit_suplier($id_suplier)
	{
		$this->form_validation->set_rules('id_suplier','ID Suplier','trim|required');
		$this->form_validation->set_rules('nama_suplier','Nama Suplier','required');
		$this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_rules('no_hp','No Telp','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		if($this->form_validation->run() == TRUE) {
			//jika menyimpan
			$data['id_suplier'] = $this->input->post('id_suplier');
			$data['nama_suplier'] = $this->input->post('nama_suplier');
			$data['alamat'] = $this->input->post('alamat');
			$data['no_hp'] = $this->input->post('no_hp');

			$this->M_Suplier->update_suplier($id_suplier,$data);

			redirect(site_url('stoker/C_Suplier/list_suplier'));

		}else{
			//jika tidak menyimpan
			$suplier = $this->M_Suplier->get_suplier_by($id_suplier); 

			$data['id_suplier'] = $suplier['id_suplier'];
			$data['nama_suplier'] = $suplier['nama_suplier'];
			$data['alamat'] = $suplier['alamat'];
			$data['no_hp'] = $suplier['no_hp'];

			$this->template->set('title', 'Edit Data Suplier');
            $this->template->load('adminLTE', 'contents' , 'view_suplier/edit_suplier', $data);
		}
	}

	public	function delete_suplier($id_suplier)
	{
		$this->M_Suplier->delete_suplier($id_suplier);

		redirect(site_url('stoker/C_Suplier/list_suplier'));
	}
}

