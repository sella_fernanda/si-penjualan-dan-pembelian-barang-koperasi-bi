<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {
	
	// Logout di sini
	public function logout() {
		$this->session->unset_userdata('id_pembelian');
		$this->session->unset_userdata('no_pembelian');
		$this->session->unset_userdata('tgl_suply');
		$this->session->unset_userdata('suplier');
		$this->session->unset_userdata('jenis_pembayaran');
		$this->session->unset_userdata('barang');
		$this->session->unset_userdata('transport');
		$this->session->unset_userdata('angkut');
		$this->session->unset_userdata('biaya_lain');
		$barang=array(
			'id_kategori' => $this->input->post('id_kategori'), 
			'no_barcode' => $this->input->post('no_barcode'), 
			'jumlah_beli' => $this->input->post('jumlah_beli'), 
			'harga_beli' => $this->input->post('harga_beli'), 
			'ppn' => $this->input->post('ppn'), 
			'diskon' => $this->input->post('diskon'),
			'hpp' => $this->input->post('hpp'),
		);
		
		$this->simple_login->logout();	
	}	
}