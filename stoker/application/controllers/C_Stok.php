<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Stok extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Stok');
	}
	
	public function list_stok_barang()
	{
		$data['barang'] = $this->M_Stok->get_list();

		$this->template->set('title', 'Data Stok Opname');
		$this->template->load('adminLTE', 'contents' , 'view_stok/list_stok_barang', $data);
	}

	public function laporan_stok_barang()
	{
		$data['barang'] = $this->M_Stok->get_histori();
		$this->template->set('title', 'Laporan Stok Opname');
		$this->template->load('adminLTE', 'contents' , 'view_stok/laporan_stok', $data);
	}

	public function edit_stok_barang($no_barcode)
	{
		$this->form_validation->set_rules('no_barcode','ID Barang','required');
		$this->form_validation->set_rules('stok_awal','Stok Awal','trim|required');
		$this->form_validation->set_rules('stok_buy','Stok Pembelian','');
		$this->form_validation->set_rules('stok_penjualan','Stok Penjualan','');
		$this->form_validation->set_rules('stok_retur','Stok Retur','');
		$this->form_validation->set_rules('stok_total','Total','required');
		$this->form_validation->set_rules('stok_opname','Stok Opname','required');
		$this->form_validation->set_rules('gain_loss','Gain Loss','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		if($this->form_validation->run() == TRUE) {
			//jika menyimpan
			$data['no_barcode'] = $this->input->post('no_barcode');
			$data['stok_awal'] = $this->input->post('stok_awal');
			$data['stok_buy'] = $this->input->post('stok_buy');
			$data['stok_penjualan'] = $this->input->post('stok_penjualan');
			$data['stok_retur'] = $this->input->post('stok_retur');
			$data['total'] = $this->input->post('stok_total');
			$data['stok_opname'] = $this->input->post('stok_opname');
			$data['gain_loss'] = $this->input->post('gain_loss');
			$data['tgl_histori'] = $this->input->post('tgl_histori');

			$this->M_Stok->update_stok_opname($no_barcode,$data);


			redirect(site_url('stoker/C_Stok/laporan_stok_barang'));

		}else{
			//jika tidak menyimpan
			$data['stok_retur']=$this->M_Stok->get_total_stok3($no_barcode);
			$stok_retur=$data['stok_retur'];

			$data['stok_buy']=$this->M_Stok->get_total_stok5($no_barcode);
			$stok_buy=$data['stok_buy'];

			$data['stok_penjualan']=$this->M_Stok->get_total_stok2($no_barcode);
			$stok_penjualan=$data['stok_penjualan'];

			$stok_barang = $this->M_Stok->get_by($no_barcode);

			$data['no_barcode'] = $stok_barang['no_barcode'];
			$data['nama_barang'] = $stok_barang['nama_barang'];
			$data['stok_awal'] = $stok_barang['stok_awal'];
			$data['stok_buy'] = $stok_buy + $stok_retur;
			$data['stok_penjualan'] = $stok_penjualan;
			$data['stok_retur'] = $stok_retur;
			$data['stok_total'] = $stok_barang['stok_total'];

			$this->template->set('title', 'Cek Stok Opname');
            $this->template->load('adminLTE', 'contents' , 'view_stok/edit_stok_barang', $data);
		}
	}

	public	function delete($id_stok)
	{		
		$this->M_Stok->delete($id_stok);
		redirect(site_url('stoker/C_Stok/laporan_stok_barang'));
	}

	public function laporan_stok_opname()
    {       
        $tgl_mulai = $this->input->get('tgl_mulai');
        $tgl_selesai = $this->input->get('tgl_selesai');

        $data['laporan']=$this->M_Stok->get_laporan_stok($tgl_mulai, $tgl_selesai);

        // membedakan sebelum dan sesudah isi filter data
        if ($tgl_mulai == '' && $tgl_selesai == '') 
        {
            // kondisi ketika pertama buka, belum filter data kosong
            $data['tgl_mulai']='';
            $data['tgl_selesai']='';
        }else
        {
            // jika sudah ngisi form filter
            $data['tgl_mulai']=$tgl_mulai;
            $data['tgl_selesai']=$tgl_selesai;
            $data['laporan']=$this->M_Stok->get_laporan_stok($tgl_mulai, $tgl_selesai);
        }
        $this->template->set('title','Laporan Stok');
        $this->template->load('adminLTE', 'contents' , 'view_stok/laporan_stok_opname', $data);
    }

    public function cetak_laporan_word($tgl_mulai, $tgl_selesai)
    {
    	header("Content-type: application/vnd.ms-word");
    	header("Content-Disposition: attachment;Filename=lap_stok_opname_".$tgl_mulai."-".$tgl_selesai.".doc");

    	$data = array(
    		'tgl_mulai' => $tgl_mulai,
    		'tgl_selesai' => $tgl_selesai,
    		'laporan'=>$this->M_Stok->get_laporan_stok($tgl_mulai, $tgl_selesai),
            'start' => 0
        );

    	$this->load->view('view_stok/cetak_laporan_stok_opname',$data);
    }

    public function cetak_laporan_pdf($tgl_mulai, $tgl_selesai)
    {
    	$data = array(
    		'tgl_mulai' => $tgl_mulai,
    		'tgl_selesai' => $tgl_selesai,
    		'laporan'=>$this->M_Stok->get_laporan_stok($tgl_mulai, $tgl_selesai),
            'start' => 0
        );

    	$this->load->view('view_stok/cetak_laporan_stok_opname',$data);
    }

    public function cetak_laporan_excel($tgl_mulai, $tgl_selesai)
    {
    	header("Content-type: application/vnd.ms.excel");

    	header("Content-Disposition: attachment;Filename=lap_stok_opname_".$tgl_mulai."-".$tgl_selesai.".xls");

    	$data = array(
    		'tgl_mulai' => $tgl_mulai,
    		'tgl_selesai' => $tgl_selesai,
    		'laporan'=>$this->M_Stok->get_laporan_stok($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
    	$this->load->view('view_stok/cetak_laporan_stok_opname',$data);
    }
}

