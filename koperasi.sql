-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2020 at 05:21 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koperasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id_akun` int(11) NOT NULL,
  `nip` varchar(10) DEFAULT NULL,
  `id_peran` varchar(10) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id_akun`, `nip`, `id_peran`, `username`, `password`) VALUES
(1, '16031', 'P001', 'Sellaaa', '123'),
(2, '16031', 'P002', 'Sellaaa', '123'),
(3, '16031', 'P003', 'Sellaaa', '123'),
(4, '16031', 'P004', 'Sellaaa', '123'),
(5, '16031', 'P005', 'Sellaaa', '123');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` varchar(10) NOT NULL,
  `id_kategori` varchar(10) DEFAULT NULL,
  `id_jns_satuan` varchar(10) DEFAULT NULL,
  `no_barcode` varchar(50) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `stok_awal` int(20) NOT NULL,
  `stok_total` int(20) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `id_kategori`, `id_jns_satuan`, `no_barcode`, `nama_barang`, `stok_awal`, `stok_total`, `gambar`) VALUES
('BU-001', 'K001', 'JS004', '12', '12', 12, 12, 'bolp_kkenko.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembelian`
--

CREATE TABLE `detail_pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `no_pembelian` int(30) DEFAULT NULL,
  `id_kategori` varchar(10) DEFAULT NULL,
  `id_barang` varchar(10) DEFAULT NULL,
  `jumlah_beli` int(20) NOT NULL,
  `sisa_barang` int(10) NOT NULL,
  `harga_beli` int(20) NOT NULL,
  `ppn` int(20) NOT NULL,
  `diskon` int(20) NOT NULL,
  `hpp` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `id_penjualan` int(11) NOT NULL,
  `no_transaksi` varchar(20) DEFAULT NULL,
  `id_barang` varchar(10) DEFAULT NULL,
  `jumlah_barang` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `harga`
--

CREATE TABLE `harga` (
  `id_harga` varchar(10) NOT NULL,
  `harga_jual` int(20) NOT NULL,
  `keterangan` text NOT NULL,
  `id_barang` varchar(10) NOT NULL,
  `tgl_ubah` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga`
--

INSERT INTO `harga` (`id_harga`, `harga_jual`, `keterangan`, `id_barang`, `tgl_ubah`) VALUES
('H001', 12, 'Berlaku', 'BU-001', '2020-03-13');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pembayaran`
--

CREATE TABLE `jenis_pembayaran` (
  `id_jns_pembayaran` varchar(10) NOT NULL,
  `nama_pembayaran` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pembayaran`
--

INSERT INTO `jenis_pembayaran` (`id_jns_pembayaran`, `nama_pembayaran`) VALUES
('P001', 'Tunai'),
('P002', 'Hutang'),
('P003', 'Piutang');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_satuan`
--

CREATE TABLE `jenis_satuan` (
  `id_jns_satuan` varchar(10) NOT NULL,
  `nama_satuan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_satuan`
--

INSERT INTO `jenis_satuan` (`id_jns_satuan`, `nama_satuan`) VALUES
('JS001', 'Lusin'),
('JS003', 'Bendel'),
('JS004', 'Pcs'),
('JS006', 'Codi');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_barang`
--

CREATE TABLE `kategori_barang` (
  `id_kategori` varchar(10) NOT NULL,
  `nama_kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_barang`
--

INSERT INTO `kategori_barang` (`id_kategori`, `nama_kategori`) VALUES
('K001', 'ATK'),
('K002', 'Makanan'),
('K003', 'Minuman'),
('K004', 'Sembako'),
('K005', 'Baju'),
('K006', 'APD'),
('K007', 'Kue');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `nip` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nama_suami_istri` varchar(50) NOT NULL,
  `nama_anak` text NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tmp_lahir` varchar(50) NOT NULL,
  `no_ktp` varchar(50) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `jk` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `foto_profil` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`nip`, `nama`, `nama_suami_istri`, `nama_anak`, `tgl_lahir`, `tmp_lahir`, `no_ktp`, `no_hp`, `jk`, `alamat`, `foto_profil`) VALUES
('16031', 'Sella fernanda', '', '', '1999-05-23', 'Ponorogo', '1234567890', '081252654822', 'P', 'Sawoo, Ponorogo', 'sella');

-- --------------------------------------------------------

--
-- Table structure for table `peran`
--

CREATE TABLE `peran` (
  `id_peran` varchar(10) NOT NULL,
  `nama_peran` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peran`
--

INSERT INTO `peran` (`id_peran`, `nama_peran`) VALUES
('P001', 'Admin'),
('P002', 'Teller'),
('P003', 'Stoker'),
('P004', 'Viewer'),
('P005', 'Anggota');

-- --------------------------------------------------------

--
-- Table structure for table `retur`
--

CREATE TABLE `retur` (
  `id_retur` varchar(10) NOT NULL,
  `no_pembelian` int(30) DEFAULT NULL,
  `id_barang` varchar(10) NOT NULL,
  `jumlah` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE `stok` (
  `id_stok` int(11) NOT NULL,
  `id_barang` varchar(10) DEFAULT NULL,
  `stok_awal` int(20) NOT NULL,
  `stok_buy` int(20) NOT NULL,
  `stok_penjualan` int(20) NOT NULL,
  `stok_retur` int(20) NOT NULL,
  `total` int(20) NOT NULL,
  `stok_opname` int(20) NOT NULL,
  `gain_loss` int(20) NOT NULL,
  `tgl_histori` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `id_suplier` varchar(10) NOT NULL,
  `nama_suplier` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`id_suplier`, `nama_suplier`, `alamat`, `no_hp`) VALUES
('S001', 'Pena', 'Ngoresan', '081252654822'),
('S002', 'Asgross', 'Solo', '085675387621'),
('S003', 'Rea', 'Jebres', '087996885345'),
('S004', 'Surya Mart', 'Surakarta', '089778635466'),
('S005', 'Lotte Mart', 'Solo', '081243567832');

-- --------------------------------------------------------

--
-- Table structure for table `trans_pembelian`
--

CREATE TABLE `trans_pembelian` (
  `no_pembelian` int(30) NOT NULL,
  `tgl_suply` text NOT NULL,
  `id_suplier` varchar(10) DEFAULT NULL,
  `id_jns_pembayaran` varchar(10) DEFAULT NULL,
  `nip` varchar(10) DEFAULT NULL,
  `transportasi` int(10) NOT NULL,
  `angkut` int(10) NOT NULL,
  `biaya_lain` int(10) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_penjualan`
--

CREATE TABLE `trans_penjualan` (
  `no_transaksi` varchar(20) NOT NULL,
  `nip` varchar(10) DEFAULT NULL,
  `tgl_transaksi` text NOT NULL,
  `id_jns_pembayaran` varchar(10) DEFAULT NULL,
  `total_bayar` int(20) NOT NULL,
  `status` varchar(30) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`),
  ADD KEY `nip` (`nip`),
  ADD KEY `id_peran` (`id_peran`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_jns_satuan` (`id_jns_satuan`);

--
-- Indexes for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD PRIMARY KEY (`id_pembelian`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `no_pembelian` (`no_pembelian`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`id_penjualan`),
  ADD KEY `no_transaksi` (`no_transaksi`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id_harga`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  ADD PRIMARY KEY (`id_jns_pembayaran`);

--
-- Indexes for table `jenis_satuan`
--
ALTER TABLE `jenis_satuan`
  ADD PRIMARY KEY (`id_jns_satuan`);

--
-- Indexes for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `peran`
--
ALTER TABLE `peran`
  ADD PRIMARY KEY (`id_peran`);

--
-- Indexes for table `retur`
--
ALTER TABLE `retur`
  ADD PRIMARY KEY (`id_retur`),
  ADD KEY `no_pembelian` (`no_pembelian`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id_stok`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id_suplier`);

--
-- Indexes for table `trans_pembelian`
--
ALTER TABLE `trans_pembelian`
  ADD PRIMARY KEY (`no_pembelian`),
  ADD KEY `id_suplier` (`id_suplier`),
  ADD KEY `id_jns_pembayaran` (`id_jns_pembayaran`),
  ADD KEY `no_anggota` (`nip`);

--
-- Indexes for table `trans_penjualan`
--
ALTER TABLE `trans_penjualan`
  ADD PRIMARY KEY (`no_transaksi`),
  ADD KEY `id_jns_pembayaran` (`id_jns_pembayaran`),
  ADD KEY `nip` (`nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  MODIFY `id_penjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `akun`
--
ALTER TABLE `akun`
  ADD CONSTRAINT `akun_ibfk_1` FOREIGN KEY (`id_peran`) REFERENCES `peran` (`id_peran`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `akun_ibfk_2` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_barang` (`id_kategori`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`id_jns_satuan`) REFERENCES `jenis_satuan` (`id_jns_satuan`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD CONSTRAINT `detail_pembelian_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_pembelian_ibfk_2` FOREIGN KEY (`no_pembelian`) REFERENCES `trans_pembelian` (`no_pembelian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD CONSTRAINT `detail_penjualan_ibfk_1` FOREIGN KEY (`no_transaksi`) REFERENCES `trans_penjualan` (`no_transaksi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_penjualan_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `harga`
--
ALTER TABLE `harga`
  ADD CONSTRAINT `harga_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `retur`
--
ALTER TABLE `retur`
  ADD CONSTRAINT `retur_ibfk_1` FOREIGN KEY (`no_pembelian`) REFERENCES `trans_pembelian` (`no_pembelian`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `stok`
--
ALTER TABLE `stok`
  ADD CONSTRAINT `stok_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trans_pembelian`
--
ALTER TABLE `trans_pembelian`
  ADD CONSTRAINT `trans_pembelian_ibfk_1` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `trans_pembelian_ibfk_2` FOREIGN KEY (`id_jns_pembayaran`) REFERENCES `jenis_pembayaran` (`id_jns_pembayaran`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `trans_penjualan`
--
ALTER TABLE `trans_penjualan`
  ADD CONSTRAINT `trans_penjualan_ibfk_2` FOREIGN KEY (`id_jns_pembayaran`) REFERENCES `jenis_pembayaran` (`id_jns_pembayaran`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `trans_penjualan_ibfk_3` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
