<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Kasir extends CI_Model {
	public function get() 
	{
		$this->db->select('*'); 
		$this->db->join('harga', 'barang.no_barcode = harga.no_barcode');
		$this->db->where('harga.keterangan', "Berlaku");
		return $this->db->get('barang')->result();
	}

	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('trans_penjualan');
        $this->db->join('jenis_pembayaran', 'trans_penjualan.id_jns_pembayaran = jenis_pembayaran.id_jns_pembayaran');
		$this->db->where('keterangan', 'Pembelian Offline');
        $this->db->order_by('no_transaksi', "desc");
		return $this->db->get();
	}

	public function get_pegawai() 
	{
		$this->db->select('*'); 
		return $this->db->get('pegawai')->result();
	}

	public function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('barang');
		$this->db->join('harga', 'harga.no_barcode = barang.no_barcode');	
		$this->db->where('barang.no_barcode', $id);
		$this->db->where('harga.keterangan', "Berlaku");
		return $this->db->get()->row();
	}

    public function getJenisPembayaran()
    {
        $this->db->select('*');
        $this->db->from('jenis_pembayaran');
        $this->db->where('id_jns_pembayaran !=','P002');
        return $this->db->get();
    }

    public function no_transaksi()   
    {    
            $this->db->select('RIGHT(trans_penjualan.no_transaksi,3) as kode', FALSE);
            $this->db->order_by('no_transaksi','DESC');    
            $this->db->limit(1);    
            $query = $this->db->get('trans_penjualan');      //cek dulu apakah ada sudah ada kode di tabel.    
                if($query->num_rows() <> 0)
                {      
                //jika kode ternyata sudah ada.      
                    $data = $query->row();      
                    $kode = intval($data->kode) + 1;    
                }
                else{      
               //jika kode belum ada      
                    $kode = 1;    
                }
            $kodemax = str_pad($kode, 5, "0", STR_PAD_LEFT);    
            $kodejadi = "PJ".$kodemax;    
            return $kodejadi;  
    }

	public function detail($no_transaksi)
    {
        $this->db->select('*'); 
        $this->db->from('detail_penjualan');
        $this->db->join('barang', 'barang.no_barcode = detail_penjualan.no_barcode');
        $this->db->join('jenis_satuan', 'jenis_satuan.id_jns_satuan = barang.id_jns_satuan');
        $this->db->join('harga', 'harga.no_barcode = detail_penjualan.no_barcode');
        $this->db->join('trans_penjualan', 'trans_penjualan.no_transaksi = detail_penjualan.no_transaksi');
        $this->db->join('pegawai', 'trans_penjualan.nip = pegawai.nip',"left");
        $this->db->where('harga.keterangan', "Berlaku");

        $query = $this->db->get_where('', array('detail_penjualan.no_transaksi' => $no_transaksi));
        return $query;
    }

	public function getTotalBarang($no_barcode)
    {
        $this->db->select('stok_total');
        $this->db->from('barang');
        $this->db->where('no_barcode', $no_barcode);
        $query =$this->db->get();
        $query = $query->row();
        return $query->stok_total;
    }

    public function insert_trans($data)
    {
        $this->db->insert('trans_penjualan', $data); 
    }

     public function insert_det_trans($data,$no_barcode,$jumlah_barang)
    {
        $this->db->insert('detail_penjualan', $data); 

        $stok_ttl=$this->getTotalBarang($no_barcode);
        $stok_jual=$jumlah_barang;
        $hasil=($stok_ttl - $stok_jual) ;
        
        $data1['stok_total']= $hasil;

        $this->db->where('no_barcode',$no_barcode);
        $this->db->update('barang',$data1);
    }

    public function get_nota($no_transaksi)
    {
        $this->db->select('*'); 
        $this->db->from('detail_penjualan');
        $this->db->join('barang', 'barang.no_barcode = detail_penjualan.no_barcode');
        $this->db->join('jenis_satuan', 'jenis_satuan.id_jns_satuan = barang.id_jns_satuan');
        $this->db->join('harga', 'harga.no_barcode = detail_penjualan.no_barcode');
        $this->db->join('trans_penjualan', 'trans_penjualan.no_transaksi = detail_penjualan.no_transaksi');

        $query = $this->db->get_where('', array('detail_penjualan.no_transaksi' => $no_transaksi));
        return $query;
    }

}