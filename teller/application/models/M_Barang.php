<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Barang extends CI_Model {
	
	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('barang');
        $this->db->join('harga', 'barang.no_barcode = harga.no_barcode');
		$this->db->where('harga.keterangan', "Berlaku");
		return $this->db->get();
	}

	public function get($no_barcode)
	{
		$this->db->select('*');
		$this->db->from('barang');
		$this->db->where('barang.no_barcode', $no_barcode);
		return $this->db->get();
	}

	public function get_pengadaan_baru()
	{
		return $this->db->query("SELECT no_pengadaan FROM trans_pengadaan 
        WHERE status = 'Waiting List'")->result();
	}

	public function get_pesanan_baru()
	{
		return $this->db->query("SELECT no_transaksi FROM trans_penjualan 
        WHERE status = 'Waiting List'")->result();
	}

	public function getDataKategori()
    {
        return $this->db->get('kategori_barang'); 
    }

    public function getDataSatuan()
    {
        return $this->db->get('jenis_satuan'); 
    }

    public function insert($data,$data1,$no_barcode)
	{
    	$this->db->insert('barang', $data);
    	$this->db->insert('harga', $data1);
	}

	public function get_by($no_barcode)
	{
		$this->db->select('*');
		$this->db->from('barang');
		$this->db->join('harga', 'harga.no_barcode = barang.no_barcode');	
		$this->db->where('barang.no_barcode', $no_barcode);
		$this->db->where('harga.keterangan', "Berlaku");
		return $this->db->get()->row_array();
	}

	public function update($no_barcode,$id_harga,$data,$data1/*,$stok_total*/)
	{
		$this->db->where('no_barcode', $no_barcode);
		$this->db->update('barang',$data); 

		$this->db->where('id_harga', $id_harga);
		$this->db->update('harga', $data1);

		/*$this->db->where('no_barcode',$no_barcode);
        $this->db->update('barang',$stok_total);*/
	}

	public function detail($no_barcode)
	{
        $this->db->select('*'); 
        $this->db->from('barang');
        $this->db->join('kategori_barang', 'barang.id_kategori = kategori_barang.id_kategori');
        $this->db->join('jenis_satuan', 'barang.id_jns_satuan = jenis_satuan.id_jns_satuan');
        $this->db->join('harga', 'barang.no_barcode = harga.no_barcode');
		$this->db->where('harga.keterangan', "Berlaku");
        $query = $this->db->get_where('', array('barang.no_barcode' => $no_barcode));
        return $query;
	}

	public function delete($no_barcode)
	{
		$this->db->where('no_barcode', $no_barcode);
		$this->db->delete('barang');
	}

	public function get_peran()
	{
		$this->db->select('*');
		$this->db->from('peran');
		$this->db->where('id_peran', $this->session->userdata('id_peran'));
		return $this->db->get();
	}

}