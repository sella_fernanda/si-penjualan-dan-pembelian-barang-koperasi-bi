<?php
// Proteksi halaman
$this->simple_login->cek_login();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>KOPEBI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
   <!-- DataTables -->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
   <!-- Select2 -->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/select2/dist/css/select2.min.css">
   <!-- iCheck for checkboxes and radio inputs -->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/all.css">
   <!-- Google Font -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
   <!-- bootstrap datepicker -->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/js/jquery-2.2.3.min.js"></script>
 </head>
 <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
 <body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">

    <header class="main-header">
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <a href="<?php echo site_url('teller/Dashboard')?>" class="navbar-brand"><b>KOPE</b>BI</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="<?php echo site_url('teller/C_Kasir')?>"><span class="fa fa-shopping-cart"></span> Kasir </a></li> 
              </li>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-get-pocket"></span> Konfirmasi <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                 <li><a href="<?php echo site_url('teller/C_Penjualan/list_pesanan')?>">Konfirmasi Penjualan Online</a></li>
                 <li><a href="<?php echo site_url('teller/C_Pengadaan/list_konfirmasi_pengadaan')?>">Konfirmasi Pengadaan</span></a></li> 
               </ul>
             </li>

             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-database"></span> Data Penjualan <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
               <li><a href="<?php echo site_url('teller/C_Penjualan/list_penjualan')?>">Data Penjualan Online</a></li>
               <li><a href="<?php echo site_url('teller/C_Kasir/list_kasir')?>">Data Penjualan Offline</a></li> 
             </ul>
           </li>

           <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa  fa-file-text"></span> Laporan <span class="caret"></a>
              <ul class="dropdown-menu" role="menu">
               <li><a href="<?php echo site_url('teller/C_Penjualan/laporan_penjualan_on')?>">Laporan Penjualan Online</a></li>
               <li><a href="<?php echo site_url('teller/C_Penjualan/laporan_penjualan_off')?>">Laporan Penjualan Offline</a></li>
               <li><a href="<?php echo site_url('teller/C_Penjualan/laporan_piutang')?>">Laporan Piutang</a></li> 
             </ul>
           </li>

         </ul>
         <form class="navbar-form navbar-left" role="search">
          <div class="form-group">
            <input type="text" class="form-control" id="navbar-search-input" placeholder="Search" style="width: 150px">
          </div>
        </form>
      </div>

      <!-- /.navbar-collapse -->
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="<?php echo base_url();?>assets/dist/img/pp.jpg" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><?php echo ucfirst($this->session->userdata('nama')); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="<?php echo base_url();?>assets/dist/img/pp.jpg" class="img-circle" alt="User Image">
                <p>
                  <?php print_r($_SESSION['nama']) ?> 
                  <br><small><?php echo 'Teller' ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="<?php echo base_url('teller/C_login/logout')?>" class="btn btn-default">Sign out <span class="fa fa-sign-out"></span></a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /.navbar-custom-menu -->
    </div>
  </nav>
</header>
<div class="content-wrapper">

  <div class="container">
    <section class="content-header">
      <h1 style="font-size: 30px;">
        <ol class="breadcrumb" style="font-size: 17px; background-color: #dddddd">
          <li><a style="color: #0072C6;" href="#"><i class="fa fa-dashboard"></i> Teller</a></li>
          <li class="active"><?php echo $title; ?></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content" style="padding-top: 0px">
        <?php echo $contents; ?>
      </section>
    </div>
  </div>
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
      </div>
      <strong>Copyright &copy; 2020 <a href="https://adminlte.io">Bank Indonesia Solo</a>.</strong> All rights
      reserved.
    </div>
  </footer>
</div>

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<!-- daterange picker -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })

    $('.select2').select2()

  })
</script>
</body>
</html>
