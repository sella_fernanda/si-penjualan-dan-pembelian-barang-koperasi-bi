<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>KOPEBI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

  <div class="row">
    <div class="col-xs-12">
      <!-- /.box -->

      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>No Pengadaan</th>
                <th>Tanggal Pengadaan</th>
                <th>Jenis Pengadaan</th>
                <th>Penanggung Jawab</th>
                <th>Pembayaran</th>
                <th>Status</th>
                <th>Detail</th>
                <th>Konfirmasi</th>
                <th>Batal</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $no=0;
              foreach ($konf_pengadaan->result_array() as $value) { ?>
                <tr class="odd gradeX">
                  <td ><?php echo ++$no?></td>
                  <td ><?php echo $value['no_pengadaan']?></td>
                  <td ><?=date('d F Y h:i:s', strtotime($value['tgl_pengadaan']));?></td>
                  <td ><?php echo $value['keterangan']?></td>
                  <td ><?php echo $value['nama']?></td>
                  <td ><?php echo $value['nama_pembayaran']?></td>
                  <td >
                    <?php if($value['status']=="Waiting List"): ?>
                      <i class="label label-danger"><?php echo $value['status']?></i>
                      <?php elseif($value['status']=="On Process"): ?>
                        <i class="label label-warning"><?php echo $value['status']?></i>
                      <?php elseif($value['status']=="Ready"): ?>
                        <i class="label label-success"><?php echo $value['status']?></i>
                      <?php else: ?>
                        <i class="label label-primary"><?php echo $value['status']?></i>
                      <?php endif; ?>
                    </td>

                    <td ><a href="<?php echo site_url('teller/C_Pengadaan/detail/'.$value['no_pengadaan'])?>"><button class="btn btn-info" type="button"><i class="fa fa-info"></i></button></a></td>

                    <td><div class="text-white">
                      <?php if($value['status']=="Waiting List"): ?>
                        <i><a class="btn btn-danger" href="<?= base_url('teller/C_Pengadaan/konfirmasi_pengadaan1/').$value['no_pengadaan'] ?>" onclick="return confirm('Yakin ingin memproses pengadaan?')">
                        Proses</a></i>
                        <?php elseif($value['status']=="On Process"): ?>
                          <i><a class="btn btn-warning" href="<?= base_url('teller/C_Pengadaan/konfirmasi_pengadaan2/').$value['no_pengadaan'] ?>" onclick="return confirm('Yakin ingin konfirmasi pengadaan?')">
                          Ready</a></i>
                        <?php elseif($value['status']=="Ready"): ?>
                          <i><a class="btn btn-success" href="<?= base_url('teller/C_Pengadaan/konfirmasi_pengadaan3/').$value['no_pengadaan'] ?>" onclick="return confirm('Yakin ingin menyelesaikan pengadaan?')">
                          Selesai</a></i>
                        <?php else: ?>
                          <i class="label label-default">Transaksi Selesai</i>
                        <?php endif; ?>
                      </div></td>

                      <td >
                        <?php if($value['status']=="Selesai"): ?>
                          <a href="<?php echo site_url('teller/C_Pengadaan/batal/'.$value['no_pengadaan'])?>"><button class="btn btn-default" type="button" disabled=""><i class="fa fa-close"></i></button></a>
                          <?php else: ?>
                            <a href="<?php echo site_url('teller/C_Pengadaan/batal/'.$value['no_pengadaan'])?>"><button class="btn btn-danger" type="button"><i class="fa fa-close" onclick="return confirm('Yakin ingin membatalkan pengadaan?')"></i></button></a>
                        <?php endif; ?>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody></table>

              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- jQuery 3 -->
        <script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

        <script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <!-- page script -->
        <script>
          $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
          })
        </script>
      </body>
      </html>

