 <section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <?php date_default_timezone_set("Asia/Jakarta");
        $tgl = date('d/m/Y h:i:s'); ?>

        <i class="fa fa-globe"></i> KOPEBI.
        <small class="pull-right">Date: <?php echo $tgl ;?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <?php 
      $no=0;
      foreach ($query->result_array() as $value) { ?>
        <?php $id = $value['id_pengadaan'] ?>
        <?php $no = $value['no_pengadaan'] ?>
        <?php $byr = $value['nama_pembayaran'] ?>
        <?php $nama = $value['nama'] ?>
      <?php
      }
      ?>
      <b>Invoice : </b> <?php echo "#".$id ;?> <br>
      <br>
      <b>No Transaksi :</b> <?php echo $no ;?><br>
      <b>Pembayaran :</b> <?php echo $byr ;?><br>
      <b>Penanggung Jawab :</b> <?php echo $nama ;?>
      <br><br>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Satuan</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $no=0;
          foreach ($query->result_array() as $value) { ?>
            <tr>
              <td ><?php echo ++$no?></td>
              <td><?php echo $value['nama_barang']?></td>
              <td><?php echo $value['jumlah_barang']?></td>
              <td><?php echo $value['nama_satuan']?></td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="row">
    <!-- accepted payments column -->
    <div class="col-xs-6">
    
    </div>
    <!-- /.col -->
    <div class="col-xs-6">
      <?php 
      $no=0;
      foreach ($query->result_array() as $value) { ?>
        <?php $tgl_pengadaan = $value['tgl_pengadaan'] ?>
      <?php
      }
      ?>
      <p class="lead">Waktu Transaksi : <?php echo $tgl_pengadaan ;?></p>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="<?php echo site_url('teller/C_Pengadaan/list_konfirmasi_pengadaan')?>" class="btn btn-default">Kembali</a>
      <a href="" onclick="window.print()" class="btn btn-primary">Cetak <span class="fa fa-print"></span></a>
    </div>
  </div>
</section>