 <section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <?php date_default_timezone_set("Asia/Jakarta");
        $tgl = date('d/m/Y h:i:s'); ?>

        <i class="fa fa-globe"></i> KOPEBI.
        <small class="pull-right">Date: <?php echo $tgl ;?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <?php 
      $no=0;
      foreach ($query->result_array() as $value) { ?>
        <?php $id = $value['id_penjualan'] ?>
        <?php $no = $value['no_transaksi'] ?>
        <?php $nama = $value['nama'] ?>
      <?php
      }
      ?>
      <b>Invoice : </b> <?php echo "#".$id ;?> <br>
      <br>
      <b>No Transaksi :</b> <?php echo $no ;?><br>
      <b>Pembeli :</b> <?php echo $nama ;?>
      <br><br>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>No Barcode</th>
            <th>Nama Barang</th>
            <th>Harga</th>
            <th>Jumlah</th>
            <th>Satuan</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $no=0;
          foreach ($query->result_array() as $value) { ?>
            <tr>
              <td ><?php echo ++$no?></td>
              <td><?php echo $value['no_barcode']?></td>
              <td><?php echo $value['nama_barang']?></td>
              <td><?php echo $value['harga_jual']?></td>
              <td><?php echo $value['jumlah_barang']?></td>
              <td><?php echo $value['nama_satuan']?></td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="row">
    <!-- accepted payments column -->
    <div class="col-xs-6">
    
    </div>
    <!-- /.col -->
    <div class="col-xs-6">
      <?php 
      $no=0;
      foreach ($query->result_array() as $value) { ?>
        <?php $tgl_trans = $value['tgl_transaksi'] ?>
        <?php $total = number_format($value['total_bayar']) ?>
      <?php
      }
      ?>
      <p class="lead">Waktu Transaksi : <?php echo $tgl_trans ;?></p>

      <div class="table-responsive">
        <table class="table">
          <tr>
            <th class="lead">Total  :</th>
            <td class="lead">Rp <?php echo $total ;?></td>
          </tr>
        </table>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="<?php echo site_url('teller/C_Penjualan/list_pesanan')?>" class="btn btn-default">Kembali</a>
      <a href="" onclick="window.print()" class="btn btn-primary">Cetak <span class="fa fa-print"></span></a>
    </div>
  </div>
</section>