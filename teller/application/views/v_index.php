<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <br>
        <center>
          <img src="<?php echo base_url() ?>assets/img/BI.png" alt="logo bank indonesia" width="500" height="200">
        </center> 
        <br> <br>
      </div>
    </div>   
  </div>
</div>

<section class="content">
  <!-- small box -->
  <div class="col-sm-3"></div>
  <div class="row">
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
        </a>
        <p>Penjualan Online Baru</p>

        <h3><?php echo $jml_pesanan_baru ?></h3>

      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      <a href="<?php echo site_url('teller/C_Penjualan/list_pesanan'); ?>" class="small-box-footer">
        Detail <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <p>Pengadaan Baru</p>

        <h3><?php echo $jml_pengadaan_baru ?></h3>

      </div>
      <div class="icon">
        <i class="fa fa-envelope"></i>
      </div>
      <a href="<?php echo site_url('teller/C_Pengadaan/list_konfirmasi_pengadaan'); ?>" class="small-box-footer">
        Detail <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <!-- ./col -->
</div>
</section>