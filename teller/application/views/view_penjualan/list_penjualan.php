<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>KOPEBI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">

  <div class="row">
    <div class="col-xs-12">
      <!-- /.box -->

      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>No Transaksi</th>
                <th>Tanggal</th>
                <th>Pelanggan</th>
                <th>Pembayaran</th>
                <th>Total</th>
                <th>Detail</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $no=0;
              foreach ($penjualan->result_array() as $value) { ?>
                <tr class="odd gradeX">
                  <td ><?php echo ++$no?></td>
                  <td ><?php echo $value['no_transaksi']?></td>
                  <td ><?=date('d F Y h:i:s', strtotime($value['tgl_transaksi']));?></td>
                  <td ><?php echo $value['nama']?>
                  </td>
                  <td ><?php echo $value['nama_pembayaran']?></td>
                  <td ><?php echo 'Rp '.$value['total_bayar']?></td>

                  <td ><a href="<?php echo site_url('teller/C_Penjualan/detail/'.$value['no_transaksi'])?>"><button class="btn btn-info" type="button"><i class="fa fa-info"></i></button></a></td><!-- 
                  <td ><a href="<?php echo site_url('teller/C_Penjualan/cetak/'.$value['no_transaksi'])?>"><button class="btn btn-primary" type="button">Cetak <span class="fa fa-print"></span></button></a></td> -->
                </tr>
                <?php
              }
              ?>
            </tbody></table>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- jQuery 3 -->
    <script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

    <script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : false,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false
        })
      })
    </script>
  </body>
  </html>

