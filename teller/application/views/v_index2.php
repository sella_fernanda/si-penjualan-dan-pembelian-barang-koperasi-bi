<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <br>
        <center>
          <img src="<?php echo base_url() ?>assets/img/BI.png" alt="logo bank indonesia" width="500" height="200">
        </center> 
        <br> <br>
      </div>
    </div>   
  </div>
</div>
<div class="row">
  <div class="col-sm-3"></div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <a href="<?php echo base_url('teller/Dash_ksp'); ?>"><span class="info-box-icon bg-red"><i class="ion ion-stats-bars"></i></span></a>
      <div class="info-box-content">
        <span style="padding-top: 25px" class="info-box-number">Simpan Pinjam</span>
      </div>
    </div>
  </div>

  <div class="clearfix visible-sm-block"></div>

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <a href="<?php echo base_url('teller/Dashboard/Toko'); ?>"><span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span></a>
      <div class="info-box-content">
        <span style="padding-top: 25px" class="info-box-number">Toko</span>
      </div>
    </div>
  </div>
</div>