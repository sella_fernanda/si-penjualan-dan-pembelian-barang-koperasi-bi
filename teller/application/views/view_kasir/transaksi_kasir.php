<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>KOPEBI</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
	<!-- Google Font -->
	<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<section>
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-header with-border">
				</div>
				<div class="box-body">
					<form class="form-horizontal" id="form_transaksi" role="form">
						<div class="col-md-8">
							<div class="form-group">
								<label class="control-label col-md-3">Pelanggan :</label>
								<label>
									<input type="radio" class="pelanggan" name="pelanggan" value="umum"> Umum
								</label>
								<label>
									<input type="radio" class="pelanggan" name="pelanggan" value="pegawai"> Pegawai
								</label>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Bayar :</label>
								<div class="col-sm-5">
									<?php  
									$options['']='';
									foreach ($query2->result_array() as $rows)
									{
										$options[$rows['id_jns_pembayaran']]=$rows['nama_pembayaran'];
									}
									echo form_dropdown('pembayaran', $options,'',array('class'=>'form-control','id'=>'pembayaran'));
									?>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Nama Pegawai :</label>
								<div class="col-md-5">
									<select id="pegawai" name="pegawai" class="form-control select2">
										<option value="0"></option>
										<?php foreach ($pegawai as $pegawai): ?>
											<option value="<?= $pegawai->nip ?>"><?= $pegawai->nama ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-8">
							<div class="form-group">
								<label class="control-label col-md-3" 
								for="no_barcode">No Barcode :</label>
								<div class="col-md-5">
									<input list="list_barang" class="form-control reset" 
									placeholder="Isi Barcode..." name="no_barcode" id="no_barcode" 
									autocomplete="on" onchange="showBarang(this.value)">
									<!-- menampilkan list refernsi barang -->
									<datalist id="list_barang">
										<?php foreach ($barang as $barang): ?>
											<option value="<?= $barang->no_barcode ?>"><?= $barang->nama_barang ?></option>
										<?php endforeach ?>
									</datalist>
								</div>
								<div class="col-md-1">
									<a href="javascript:;" class="btn btn-primary" 
									data-toggle="modal" 
									data-target="#modal-cari-barang">
									<i class="fa fa-search"></i></a>
								</div>
							</div>
							<div id="barang"> <!-- data barang -->
								<div class="form-group">
									<label class="control-label col-md-3" 
									for="nama_barang">Nama Barang :</label>
									<div class="col-md-8">
										<input type="text" class="form-control reset" 
										name="nama_barang" id="nama_barang" 
										readonly="readonly">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" 
									for="harga_jual">Harga (Rp) :</label>
									<div class="col-md-8">
										<input type="text" class="form-control reset" 
										name="harga_jual" id="harga_jual" 
										readonly="readonly">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3" 
									for="qty">Quantity :</label>
									<div class="col-md-4">
										<input type="number" class="form-control reset" 
										autocomplete="off" onchange="subTotal(this.value)" 
										onkeyup="subTotal(this.value)" id="qty" min="0" 
										name="qty" placeholder="Isi jumlah...">
									</div>
								</div>
							</div><!-- end No Barcode -->
							<div class="form-group">
								<label class="control-label col-md-3" 
								for="sub_total">Sub-Total (Rp):</label>
								<div class="col-md-8">
									<input type="text" class="form-control reset" 
									name="sub_total" id="sub_total" 
									readonly="readonly">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-offset-3 col-md-3">
									<button type="button" class="btn btn-primary" 
									id="tambah" onclick="addbarang()">
									<i class="fa fa-cart-plus"></i> Tambah</button>
								</div>
							</div>
			      <!-- </div>
			      </div> --><!-- end panel-->
			  </div><!-- end col-md-8 -->
			  <div class="col-md-4 mb">
			  	<div class="col-md-12">
			  		<div class="form-group">
			  			<label for="total" class="besar">Total (Rp) :</label>
			  			<input type="text" class="form-control input-lg" 
			  			name="total" id="total" placeholder="0"
			  			readonly="readonly"  value="<?= number_format( 
			  			$this->cart->total(), 0 , '' , '.' ); ?>">
			  		</div>
			  		<div class="form-group">
			  			<label for="bayar" class="besar">Bayar (Rp) :</label>
			  			<input type="text" class="form-control input-lg uang" 
			  			name="bayar" placeholder="0" autocomplete="off"
			  			id="bayar" onkeyup="showKembali(this.value)">
			  		</div>
			  		<div class="form-group">
			  			<label for="kembali" class="besar">Kembali (Rp) :</label>
			  			<input type="text" class="form-control input-lg" 
			  			name="kembali" id="kembali" placeholder="0"
			  			readonly="readonly">
			  		</div>
			  	</div>
			  </div><!-- end col-md-4 -->
			</form>

			<table id="table_transaksi" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>No</th>
						<th>No Barcode</th>
						<th>Nama Barang</th>
						<th>Harga</th>
						<th>Quantity</th>
						<th>Sub-Total</th>
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
			<div class="col-md-offset-8" style="margin-top:20px">
				<a href="<?php echo site_url('teller/C_Kasir/simpan')?>"><button type="button" class="btn btn-primary btn-lg" 
					id="selesai" disabled="disabled" 
					onclick="">
					Selesai <i class="fa fa-angle-double-right"></i></button></a>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal selesai -->
	<div class="modal fade" id="modal-cari-barang" role="dialog">
		<div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Cari Barang</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="box-body">
							<table id="example1" class="table">
								<thead>
									<tr>
										<th>No Barcode</th>
										<th>Nama Barang</th>
										<th>Stok</th>
										<th>Harga Jual</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									foreach ($brg->result_array() as $value) { ?>
										<tr class="odd gradeX">
											<td ><?php echo $value['no_barcode']?></td>
											<td ><?php echo $value['nama_barang']?></td>
											<td ><?php echo $value['stok_total']?></td>
											<td ><?php echo 'Rp. '.number_format($value['harga_jual'])?></td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function () {

			function enableInput(i) {
				$(i).removeClass('disabled').prop('disabled', false).prop('required', true);
			}

			function disableInput(i) {
				$(i).addClass('disabled').prop('disabled', true).prop('required', false).val('');
			}

			$('input[name="pelanggan"]').on('change', function () {
				var value = $(this).val();

				if (value != "umum") {
					enableInput('select[name="pegawai"]');
					enableInput('select[name="pembayaran"]');
				} else {
					disableInput('select[name="pegawai"]');
					disableInput('select[name="pembayaran"]');
				}
			});

        	$(".pelanggan").on("click", function(){ //ambil data pelanggan
            var pelanggan = $(this).val(); // mengambil value
            $.ajax({
            	method : 'POST',
            	url : '<?php echo base_url('teller/C_Kasir/ambil_pelanggan'); ?>',
                data :  { pelanggan: pelanggan}, //mengirim variabel dengan nilai dari variabel itu
            });
        });

        	$("#pembayaran").on("change", function(){ //ambil data pembayaran
            var pembayaran = $(this).val(); // mengambil value
            $.ajax({
            	method : 'POST',
            	url : '<?php echo base_url('teller/C_Kasir/ambil_pembayaran'); ?>',
                data :  { pembayaran: pembayaran}, //mengirim variabel dengan nilai dari variabel itu
            });
        });

        	$("#pegawai").on("change", function(){ //ambil data pegawai
            var pegawai = $(this).val(); // mengambil value
            $.ajax({
            	method : 'POST',
            	url : '<?php echo base_url('teller/C_Kasir/ambil_pegawai'); ?>',
                data :  { pegawai: pegawai}, //mengirim variabel dengan nilai dari variabel itu
            });
        });

        	$("#bayar").on("change", function(){ //ambil data jumlah bayar
            var bayar = $(this).val(); // mengambil value
            $.ajax({
            	method : 'POST',
            	url : '<?php echo base_url('teller/C_Kasir/ambil_bayar'); ?>',
                data :  { bayar: bayar}, //mengirim variabel dengan nilai dari variabel itu
            });
        });
        });
    </script>
    <script type="text/javascript">

    	function showBarang(barcode) 
    	{
    		if (barcode == "") { //jika no_barcode kosong
    			$('#nama_barang').val('');
    			$('#harga_jual').val('');
    			$('#qty').val('');
    			$('#sub_total').val('');
    			$('#reset').hide();
    			return;
    		} else { 
    			if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
    				xmlhttp = new XMLHttpRequest(); //object  
    			} else {// code for IE6, IE5
    				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); //object 
    			}
    			xmlhttp.onreadystatechange = function() { //fungsi handler untuk event menampilkan data barang
    				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) { //mengecek state dan status AJAX.
    					document.getElementById("barang").innerHTML = xmlhttp.responseText; 
    				}
    			}
    			xmlhttp.open("GET", "<?= base_url('teller/C_Kasir/getbarang') ?>/"+barcode,true); //Menentukan Method dan URL
    			xmlhttp.send(); //Mengirim Request
    		}
    	}

    	//menampilkan sub_total
    	function subTotal(qty) 
    	{
    		var harga = $('#harga_jual').val().replace(".", "").replace(".", "");
    		$('#sub_total').val(convertToRupiah(harga*qty));
    	}

    	 //merubah angka menjadi rupiah
    	function convertToRupiah(angka)
    	{
    		var rupiah = '';    
    		var angkarev = angka.toString().split('').reverse().join('');

    		for(var i = 0; i < angkarev.length; i++) 
    			if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';

    		return rupiah.split('',rupiah.length-1).reverse().join('');
    	}

    	//menampilkan datatables (reload tabel)
    	var table;
    	$(document).ready(function() {

    		showKembali($('#bayar').val());

    		table = $('#table_transaksi').DataTable({ 
    			paging: false,
    			"info": false,
    			"searching": false,
        		"processing": true, //Feature control the processing indicator.
        		"serverSide": false, //Feature control DataTables' 
        		// server-side processing mode.

        		// Load data for the table's content from an Ajax source
        		"ajax": {
        			"url": "<?= site_url('teller/C_Kasir/ajax_list_transaksi')?>",
        			"type": "POST"
        		},

			        //Set column definition initialisation properties.
			        "columnDefs": [
			        { 
			          "targets": [ 0,1,2,3,4,5,6 ], //last column
			          "orderable": false, //set not orderable
			      },
			      ],

			  });
    	});

    	function reload_table(){  //reload datatable ajax 
    		table.ajax.reload(null,false); //user paging will not be reset on reload.
    	}

    	//menambahkan data ke datatable cart
    	function addbarang()
    	{
    		var no_barcode = $('#no_barcode').val();
    		var nama_barang = $('#nama_barang').val();

    		if (no_barcode == '') {
    			$('#no_barcode').focus();
    		}else if(qty == ''){
    			$('#qty').focus();
    		}else{
    			var stok = $('#jml_stok').html();
  				var cek_total_cart = $('#qty-cart-' + no_barcode).html(); //cek jumlah di cart
  				var qty = $('#qty').val();
  				if(stok == undefined){
  					stok = 0;
  				}
  				if(cek_total_cart == undefined){
  					cek_total_cart = 0;
  				}

  				var hasil = parseInt(qty) + parseInt(cek_total_cart);
  				if(hasil <= stok){
					// ajax add barang to datatables
					$.ajax({
						url : "<?= site_url('teller/C_Kasir/addbarang')?>",
						type: "POST",
						data: $('#form_transaksi').serialize(),
						dataType: "JSON",
						success: function(data)
						{
            				//reload ajax table
            				reload_table();
            			},
            			error: function (jqXHR, textStatus, errorThrown)
            			{
            				alert('Error adding data');
            			}
            		});

					showTotal();
					showKembali($('#bayar').val());
          			//mereset semua value setelah btn tambah ditekan
          			$('.reset').val('');
          		}else{
          			if(qty != 0 || qty == null){
          				alert('Anda Sudah Melebihi Jumlah Stok Produk ' + nama_barang); 
          			} else {
          				alert('Jumlah Qty Masih 0 !');

          			}	
          		}
          	};
        }

        // ajax delete item cart datatables
        function deletebarang(id,sub_total)
        {
        	var url = "<?= base_url()?>teller/C_Kasir/deletebarang/" + id;
        	$.ajax({
        		url : url,
        		type: "POST",
        		data: $('#form_transaksi').serialize(),
        		dataType: "JSON",
        		success: function(data)
        		{
            		//reload ajax table
            		reload_table();
            	},
	            error: function (jqXHR, textStatus, errorThrown)
	            {
	            	alert('Error deleting data '+id);
	            }
        	});

        	var ttl = $('#total').val().replace(".", "");

        	$('#total').val(convertToRupiah(ttl-sub_total));

        	showKembali($('#bayar').val());
        }

    function showTotal()
    {
    	var total = $('#total').val().replace(".", "").replace(".", "");

    	var sub_total = $('#sub_total').val().replace(".", "").replace(".", "");

    	$('#total').val(convertToRupiah((Number(total)+Number(sub_total))));

    }

    function showKembali(nominal)
    {
  		var total = $('#total').val().replace(".", "").replace(".", ""); //menghilangkan .
  		var bayar = nominal.replace(".", "").replace(".", "");
  		var kembali = bayar-total;

  		$('#kembali').val(convertToRupiah(kembali));

  		if (kembali >= 0) { //tombol selesai
  			$('#selesai').removeAttr("disabled");
  			$.ajax({
            	method : 'POST',
            	url : '<?php echo base_url('teller/C_Kasir/ambil_kembali'); ?>',
                data :  { kembali: kembali}, //mengirim variabel dengan nilai dari variabel itu
            });
  		}else{
  			$('#selesai').attr("disabled","disabled");
  		};

  		if (total == '0') {
  			$('#selesai').attr("disabled","disabled");
  		};
  	}

  </script>
  <script>
  	$(function () {
  		$('#example1').DataTable()
  		$('#example2').DataTable({
  			'paging'      : true,
  			'lengthChange': false,
  			'searching'   : true,
  			'ordering'    : true,
  			'info'        : true,
  			'autoWidth'   : false
  		})
  	})
  </script>
  <script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- DataTables -->
  <script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

  <script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
</div>
</section>
</body>
</html>