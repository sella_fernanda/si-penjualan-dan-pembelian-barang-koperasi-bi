<!doctype html>
<html>
<head>
    <title>Laporan Penjualan Offline</title>
    <style>
        .row{
            border:1px solid grey !important;
            padding: 5mm;
            margin: 20px auto;
            width: 80mm;
            background: #FFF;
        }
        .table {
            border:1px solid black !important; 
            border-collapse: collapse !important;
            width: 44mm;
        }
    </style>
</head> 
<body>
    <div class="row">
        <br><br>
        <p style="font-family: Arial; font-size: 10;" align="center"><b>KOPERASI BI SOLO</b></p>
        <p style="font-family: Arial; font-size: 5;" align="center">Jl. Jend. Sudirman Kel No.15, Kp. Baru, Surakarta</p>
        <div id="info">
            <b><p> No.Transaksi <span style="display:inline-block; width: 5%;"></span> : <?=$no_transaksi?>
            <br> Tanggal <span style="display:inline-block; width: 18%;"></span>: <?php echo date('d-m-Y h:i:s'); ?>
            <br> Kasir <span style="display:inline-block; width: 23%;"></span>: <?php echo $this->session->userdata('nama'); ?>
            <br> Pelanggan <span style="display:inline-block; width: 13%;"></span>: <?= $pelanggan ?>
        </p></b>
    </div>
    <table class="table table-striped">
        <tbody>
            <?php 
            $total = 0;
            foreach ($cart as $items) { ?>
                <?php echo $items["id"].' - '.$items["name"]; ?></br>
                <?php echo number_format($items["price"])?><span style="display:inline-block; width: 30%;"></span>
                <?php echo 'x '.number_format($items['qty']); ?><span style="display:inline-block; width: 30%;"></span> <?php echo number_format($items['subtotal']);?> </br>
            
                <?php $total += $items["subtotal"] ?>
            <?php } ?>
        </tbody>
        <tfoot>
            <p>
                <span style="display:inline-block; width: 40%;"></span>Total : <?php echo 'Rp '.number_format($total); ?> <br>
                <span style="display:inline-block; width: 40%;"></span>Nominal Bayar : <?php echo 'Rp '.number_format($bayar); ?><br>
                <span style="display:inline-block; width: 40%;"></span>Kembali : <?php echo 'Rp '.number_format($kembali); ?>
            </p>
            <p align="center">Terimakasih Atas Kunjungan Anda</p>
        </tfoot>
    </table>
    <script>
        window.print();
    </script>
</div>
</body>
</html>