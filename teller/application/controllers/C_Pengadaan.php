<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Pengadaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Pengadaan');
	}

	public function list_konfirmasi_pengadaan()
	{
		$data['konf_pengadaan'] = $this->M_Pengadaan->get_list();

		$this->template->set('title', 'Konfirmasi Transaksi Pengadaan');
		$this->template->load('adminLTE', 'contents' , 'view_pengadaan/konfirmasi_pengadaan', $data);
	} 

	public function konfirmasi_pengadaan1($no_pengadaan)
	{		
		$data['status']	= 'On Process';	
        $data['teller']	= $this->session->userdata('nip');
		$this->M_Pengadaan->update($no_pengadaan,$data);
		redirect('teller/C_Pengadaan/list_konfirmasi_pengadaan');
	}

	public function konfirmasi_pengadaan2($no_pengadaan)
	{		
		$data['status']	= 'Ready';	
		$this->M_Pengadaan->update2($no_pengadaan,$data);
		redirect('teller/C_Pengadaan/list_konfirmasi_pengadaan');
	}

	public function konfirmasi_pengadaan3($no_pengadaan)
	{		
		$data['status']	= 'Selesai';	
		$this->M_Pengadaan->update3($no_pengadaan,$data);
		redirect('teller/C_Pengadaan/list_konfirmasi_pengadaan');
	}

	public function batal($no_pengadaan){
		$this->M_Pengadaan->delete($no_pengadaan);
		redirect(site_url('teller/C_Pengadaan/list_konfirmasi_pengadaan'));
	}	

	public function detail($no_pengadaan)
	{
		$this->template->set('title', 'Detail Pengadaan');

		$query = $this->M_Pengadaan->detail($no_pengadaan);
		$data['query'] = $query;
		$this->template->load('adminLTE', 'contents' , 'view_pengadaan/detail_pengadaan', $data);
	}
}