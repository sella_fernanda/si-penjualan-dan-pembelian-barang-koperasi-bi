<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Kasir extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('M_Kasir','M_Barang'));
	}

	public function index()
	{
		$this->form_validation->set_Rules('no_barcode','No Barcode','required');
		$this->form_validation->set_Rules('jumlah_beli','Qty','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		$data['barang'] = $this->M_Kasir->get(); //list referensi barang
		$data['pegawai'] = $this->M_Kasir->get_pegawai(); //menampilkan pegawai
		$data['query2']=$this->M_Kasir->getJenisPembayaran(); //menampilkan jenis pembayaran
		$data['brg'] = $this->M_Barang->get_list(); //menampilkan barang di modal (cari)

		$this->template->set('title','Kasir');
		$this->template->load('adminLTE','contents','view_kasir/transaksi_kasir',$data);
	}

	//list data transaksi penjualan offline
	public function list_kasir()
	{
		$data['kasir'] = $this->M_Kasir->get_list();
		$this->template->set('title', 'Data Penjualan Offline');
		$this->template->load('adminLTE', 'contents' , 'view_kasir/list_kasir', $data);
	}

	//get barang berdasarkan no barcode yg dimasukkan
	public function getbarang($id)
	{
		$barang = $this->M_Kasir->get_by_id($id);
		if ($barang) { //true
			if ($barang->stok_total == '0') { //jika stok 0
				$disabled = 'disabled';
				$info_stok = '<span class="help-block badge" id="reset" 
				style="background-color: #d9534f;">
				stok habis</span>';
			}else{ //jika stok > 0
				$disabled = '';
				$info_stok = '<span class="help-block badge" id="reset" 
				style="background-color: #5cb85c;">stok : '
				.$barang->stok_total.'</span>';
				$stok = $barang->stok_total;
			}

			//menampilkan data barang
			echo '
			<div class="form-group">
			<label class="control-label col-md-3" for="nama_barang">Nama Barang :</label>
			<div class="col-md-8">
			<input type="text" class="form-control reset" name="nama_barang" id="nama_barang" value="'.$barang->nama_barang.'"readonly="readonly">
			</div>
			</div>
			<div class="form-group">
			<label class="control-label col-md-3" for="harga_jual">Harga (Rp) :</label>
			<div class="col-md-8">
			<input type="text" class="form-control reset" id="harga_jual" name="harga_jual" value="'.number_format( $barang->harga_jual, 0 ,'' , '.' ).'" readonly="readonly">
			</div>
			</div>
			<div class="form-group">
			<label class="control-label col-md-3" for="qty">Quantity :</label>
			<div class="col-md-4">
			<input type="number" class="form-control reset" name="qty" placeholder="Isi qty..." autocomplete="off" id="qty" onchange="subTotal(this.value)" onkeyup="subTotal(this.value)" min="0"  max="'.$barang->stok_total.'" '.$disabled.'>
			</div>'.$info_stok.'<div id="jml_stok" style="display:none;">'.$stok.'</div>
			</div>';
		}
		else{ //jika input no barcode tidak valid
			echo '
			<div class="form-group">
			<label class="control-label col-md-3" for="nama_barang">Nama Barang :</label>
			<div class="col-md-8">
			<input type="text" class="form-control reset" name="nama_barang" id="nama_barang" readonly="readonly">
			</div>
			</div>
			<div class="form-group">
			<label class="control-label col-md-3" for="harga_jual">Harga (Rp) :</label>
			<div class="col-md-8">
			<input type="text" class="form-control reset" name="harga_jual" id="harga_jual" readonly="readonly">
			</div>
			</div>
			<div class="form-group">
			<label class="control-label col-md-3" for="qty">Quantity :</label>
			<div class="col-md-4">
			<input type="number" class="form-control reset" autocomplete="off" onchange="subTotal(this.value)" onkeyup="subTotal(this.value)" id="qty" min="0" name="qty" placeholder="Isi qty...">
			</div>
			</div>';
		}

	}

	//menampilkan list data item cart
	public function ajax_list_transaksi()
	{
		$data = array();
		$no = 1; 
		foreach ($this->cart->contents() as $items){
			$row = array();
			$row[] = $no;
			$row[] = $items["id"];
			$row[] = $items["name"];
			$row[] = 'Rp. ' . number_format( $items['price'], 
				0 , '' , '.' ) . ',-';
			$row[] = '<div id="qty-cart-'.$items["id"].'">'.$items["qty"].'</div>';
			$row[] = 'Rp. ' . number_format( $items['subtotal'], 
				0 , '' , '.' ) . ',-';

			//tombol aksi hapus item cart
			$row[] = '<a 
			href="javascript:void(0);" style="color:rgb(255,128,128);
			text-decoration:none" onclick="deletebarang('
			."'".$items["rowid"]."'".','."'".$items['subtotal'].
			"'".')"> <i class="fa fa-close"></i> Delete</a>';

			$data[] = $row;
			$no++;
		}

		$output = array(
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	//MENAMBAHKAN DATA KE CART
	public function addbarang()
	{
		$data = array(
			'id' => $this->input->post('no_barcode'),
			'name' => $this->input->post('nama_barang'),
			'price' => str_replace('.', '', $this->input->post(
				'harga_jual')),
			'qty' => $this->input->post('qty')
		);
		$insert = $this->cart->insert($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ambil_pelanggan(){
		$this->session->set_userdata('pelanggan', $this->input->post('pelanggan'));
	}

	public function ambil_pembayaran(){
		$this->session->set_userdata('pembayaran', $this->input->post('pembayaran'));
	}

	public function ambil_pegawai(){
		$this->session->set_userdata('pegawai', $this->input->post('pegawai'));
	}

	public function ambil_bayar(){
		$this->session->set_userdata('bayar', $this->input->post('bayar'));
	}

	public function ambil_kembali(){
		$this->session->set_userdata('kembali', $this->input->post('kembali'));
	}

	public function simpan()
	{
		if($this->session->userdata('pelanggan') == 'umum') {
			$cart = $this->cart->contents();
			date_default_timezone_set("Asia/Jakarta");
			$tgl_transaksi = date('Y-m-d h:i:s');
			$no_transaksi= $this->M_Kasir->no_transaksi();
			$kasir = array(
				'no_transaksi'=> $no_transaksi,
				'tgl_transaksi'=>$tgl_transaksi,
				'total_bayar'=> $this->cart->total(),
				'id_jns_pembayaran'=>'P001',
				'status'=>'Selesai',
				'keterangan' => 'Pembelian Offline',
				'opsi' => 'none',
				'teller' => $this->session->userdata('nip')
			);
			$this->M_Kasir->insert_trans($kasir);

			foreach ($cart as $kasir) {
				$det_kasir = array(
					'no_transaksi'=>$no_transaksi,
					'no_barcode'=>$kasir['id'],
					'jumlah_barang'=>$kasir['qty']
				);

				$no_barcode=$kasir['id'];
				$jumlah_barang=$kasir['qty'];

			$this->M_Kasir->insert_det_trans($det_kasir,$no_barcode,$jumlah_barang);

			}
		} else {
			$cart = $this->cart->contents();
			$nip=$this->session->userdata('nip');
			date_default_timezone_set("Asia/Jakarta");
			$tgl_transaksi = date('Y-m-d h:i:s');
			$no_transaksi= $this->M_Kasir->no_transaksi();
			$kasir = array(
				'no_transaksi'=> $no_transaksi,
				'nip'=> $this->session->userdata('pegawai'),
				'tgl_transaksi'=>$tgl_transaksi,
				'total_bayar'=> $this->cart->total(),
				'id_jns_pembayaran'=>$this->session->userdata('pembayaran'),
				'status'=>'Selesai',
				'keterangan' => 'Pembelian Offline',
				'opsi' => 'none',
				'teller' => $nip
			);
			
			$this->M_Kasir->insert_trans($kasir);

			foreach ($cart as $kasir) {
				$det_kasir = array(
					'no_transaksi'=>$no_transaksi,
					'no_barcode'=>$kasir['id'],
					'jumlah_barang'=>$kasir['qty']
				);

				$no_barcode=$kasir['id'];
				$jumlah_barang=$kasir['qty'];

			$this->M_Kasir->insert_det_trans($det_kasir,$no_barcode,$jumlah_barang);

			}
		}

		$data['cart'] = $cart;
		$data['no_transaksi'] = $no_transaksi;
		$data['pelanggan'] = $this->session->userdata('pelanggan');
		$data['bayar'] = $this->session->userdata('bayar');
		$data['kembali'] = $this->session->userdata('kembali');

		$this->cart->destroy();
		$this->load->view('view_kasir/cetak_struk',$data);
	}

	public function detail($no_transaksi)
	{
		$query = $this->M_Kasir->detail($no_transaksi);
		$data['query'] = $query;
		
		$this->template->set('title', 'Detail Pembalian Offline');
		$this->template->load('adminLTE', 'contents' , 'view_kasir/detail_transaksi_kasir', $data);
	}

	public function deletebarang($id) 
	{
		$data = array(
			'rowid' => $id,
			'qty' => 0
		);
		$this->cart->update($data);
			echo json_encode(array("status" => TRUE));
	}

    public function cetak_struk($no_transaksi)
    {
    	$struk = $this->M_Kasir->detail($no_transaksi);
    	$data['struk'] = $struk;

    	$this->load->view('view_kasir/cetak_struk',$data);
    }

}