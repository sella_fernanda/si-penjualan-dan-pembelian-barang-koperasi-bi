<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Barang');
	}

	public function index()
	{
		$this->template->set('title','HOME');
		$this->template->load('adminLTE2','contents','v_index2');
	}

	public function Toko()
	{
		$queryPesananBaru = $this->M_Barang->get_pesanan_Baru();
		$queryPengadaanBaru = $this->M_Barang->get_pengadaan_Baru();
		$data['jml_pesanan_baru']  = count($queryPesananBaru);
		$data['jml_pengadaan_baru']  = count($queryPengadaanBaru);
		$this->template->set('title', 'Dashboard');
		$this->template->load('adminLTE', 'contents' , 'v_index',$data);
	}

}

