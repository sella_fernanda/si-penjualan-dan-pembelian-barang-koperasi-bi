<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Penjualan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Penjualan');
    }

    public function list_pesanan()
    {
        $data['pesan']=$this->M_Penjualan->get_list();

        $this->template->set('title','Konfirmasi Transaksi Penjualan Online');
        $this->template->load('adminLTE','contents','view_pesanan/list_pesanan',$data);
    }

    public function detail_pesanan($no_transaksi)
    {
        $this->template->set('title', 'Detail Pesanan');

        $query = $this->M_Penjualan->detail($no_transaksi);
        $data['query'] = $query;
        $this->template->load('adminLTE', 'contents' , 'view_pesanan/detail_pesanan', $data);
    }

    public function list_penjualan()
    {
        $data['penjualan'] = $this->M_Penjualan->get_penjualan();

        $this->template->set('title','Data Penjualan Online');
        $this->template->load('adminLTE','contents','view_penjualan/list_penjualan',$data);
    }

    public function detail($no_transaksi)
    {
        $query = $this->M_Penjualan->detail($no_transaksi);
        $data['query'] = $query;

        $this->template->set('title', 'Detail Pembalian Online');
        $this->template->load('adminLTE', 'contents' , 'view_penjualan/detail_penjualan', $data);
    }

    public function konfirmasi_penjualan1($no_transaksi)
    {		
        $data['status']	= 'On Process';
        $data['teller']	= $this->session->userdata('nip');
        $this->M_Penjualan->update1($no_transaksi,$data);
        redirect('teller/C_Penjualan/list_pesanan');
    }

    public function konfirmasi_penjualan2($no_transaksi)
    {		
        $data['status']	= 'Ready';	
        $this->M_Penjualan->update2($no_transaksi,$data);
        redirect('teller/C_Penjualan/list_pesanan');
    }

    public function konfirmasi_penjualan3($no_transaksi)
    {		
        $data['status']	= 'Selesai';	
        $this->M_Penjualan->update3($no_transaksi,$data);
        redirect('teller/C_Penjualan/list_pesanan');
    }

    public function batal($no_transaksi)
    {
        $penjualan = $this->M_Penjualan->get_by($no_transaksi);
        $i=0;
        foreach($penjualan->result_array() as $value) {
            $no_barcode= $value['no_barcode'];

            $this->M_Penjualan->update($no_transaksi,$no_barcode);
            $i++;
        }

        $this->M_Penjualan->delete($no_transaksi);
        redirect('teller/C_Penjualan/list_pesanan');
    }

    public function laporan_penjualan_on()
    {		
        $tgl_mulai = $this->input->get('tgl_mulai');
        $tgl_selesai = $this->input->get('tgl_selesai');

        $data['laporan']=$this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai);

        $data['jumlah']=$this->M_Penjualan->get_jumlah($tgl_mulai, $tgl_selesai);
        $data['jumlah_tunai']=$this->M_Penjualan->get_jumlah_tunai($tgl_mulai, $tgl_selesai);
        $data['jumlah_piutang']=$this->M_Penjualan->get_jumlah_piutang($tgl_mulai, $tgl_selesai);
		// membedakan sebelum dan sesudah isi filter data
        if ($tgl_mulai == '' && $tgl_selesai == '') 
        {
			// kondisi ketika pertama buka, belum filter data kosong
            $data['tgl_mulai']='';
            $data['tgl_selesai']='';
        }else
        {
			// jika sudah ngisi form filter
            $data['tgl_mulai']=$tgl_mulai;
            $data['tgl_selesai']=$tgl_selesai;
            $data['laporan']=$this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai);
        }
        $this->template->set('title','Laporan Penjualan Online');
        $this->template->load('adminLTE', 'contents' , 'view_penjualan/laporan_penjualan_online', $data);
    }

    public function cetak_laporan_on_word($tgl_mulai, $tgl_selesai)
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=laporan_penj_on_".$tgl_mulai."-".$tgl_selesai.".doc");

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'laporan'=>$this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai),
            'jumlah'=> $this->M_Penjualan->get_jumlah($tgl_mulai, $tgl_selesai),
            'jumlah_tunai'=> $this->M_Penjualan->get_jumlah_tunai($tgl_mulai, $tgl_selesai),
            'jumlah_piutang'=> $this->M_Penjualan->get_jumlah_piutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );

        $this->load->view('view_penjualan/cetak_laporan_penjualan_online',$data);
    }

    public function cetak_laporan_on_excel($tgl_mulai, $tgl_selesai)
    {
        $this->load->helper('exportexcel');
        $namaFile = "lap_penj_on". "_" . "$tgl_mulai-$tgl_selesai" .".xls";
        $judul = "Laporan Penjualan Online";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "No Transaksi");
        xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Transaksi");
        xlsWriteLabel($tablehead, $kolomhead++, "Pegawai");
        xlsWriteLabel($tablehead, $kolomhead++, "Pembayaran");
        xlsWriteLabel($tablehead, $kolomhead++, "Teller");
        xlsWriteLabel($tablehead, $kolomhead++, "Total Bayar");

        foreach ($this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai) as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->no_transaksi);
            xlsWriteLabel($tablebody, $kolombody++, $data->tgl_transaksi);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_pegawai);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_pembayaran);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_teller);
            xlsWriteNumber($tablebody, $kolombody++, $data->total_bayar);

            $tablebody++;
            $nourut++;
        }

        foreach ($this->M_Penjualan->get_jumlah($tgl_mulai, $tgl_selesai) as $data1) 
        {
            $kolombody = 5;
            xlsWriteLabel($tablebody, $kolombody++, "Total");
            xlsWriteNumber($tablebody, $kolombody++, $data1->total_bayar);
        }

        foreach ($this->M_Penjualan->get_jumlah_tunai($tgl_mulai, $tgl_selesai) as $data2) 
        {
            xlsWriteLabel($tablebody, $kolombody++, "Total Tunai");
            xlsWriteNumber($tablebody, $kolombody++, $data2->total_bayar);
        }

        foreach ($this->M_Penjualan->get_jumlah_piutang($tgl_mulai, $tgl_selesai) as $data3)
        {
            xlsWriteLabel($tablebody, $kolombody++, "Total Piutang");
            xlsWriteNumber($tablebody, $kolombody++, $data3->total_bayar);
        }

        xlsEOF();
        exit();
    }

    public function cetak_laporan_on_pdf($tgl_mulai, $tgl_selesai)
    {
        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'laporan'=>$this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai),
            'jumlah'=> $this->M_Penjualan->get_jumlah($tgl_mulai, $tgl_selesai),
            'jumlah_tunai'=> $this->M_Penjualan->get_jumlah_tunai($tgl_mulai, $tgl_selesai),
            'jumlah_piutang'=> $this->M_Penjualan->get_jumlah_piutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );

        $this->load->view('view_penjualan/cetak_laporan_penjualan_online',$data);
    }

    /*public function cetak_laporan_on_pdf_fpdf($tgl_mulai, $tgl_selesai)
    {
        ini_set('magic_quotes_runtime', 0);
        
        ob_start();
        //Membuat object baru bernama $pdf dari class FPDF() dan menjalanka konstraktor. Konstruktor antara lain mengatur orientasi kertas, ukuran kertas (paper size) dan satuan ukuran kertas.
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting font
        $pdf->SetFont('Arial','B',12);
        // mencetak string
        //lebar cell, tinggi cell, text yang akan ditampilkan, border cell, pilihan pindah baris setelah cell, perataan text, isi dari cell, link pada text.
        $pdf->Cell(10,20,'',0,1);
        $pdf->Cell(190,7,'KOPERASI PEGAWAI BANK INDONESIA',0,1,'C');
        $pdf->Cell(190,7,'LAPORAN PENJUALAN ONLINE',0,1,'C');
        $pdf->Cell(190,7,'Periode'. date('d F Y', strtotime($tgl_mulai)).' - '.date('d F Y', strtotime($tgl_selesai)),0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(10,12,'No',1,0,'C');
        $pdf->Cell(20,12,'No Transaksi',1,0,'C');
        $pdf->Cell(75,12,'Tanggal Transaksi',1,0,'C');
        $pdf->Cell(20,12,'Pegawai',1,0,'C');
        $pdf->Cell(15,12,'Pembayaran',1,0,'C');
        $pdf->Cell(20,12,'Teller',1,1,'C');
        $pdf->Cell(20,12,'Total Bayar',1,0,'C');
        $pdf->SetFont('Arial','',10);
        $laporan=$this->M_Penjualan->get_laporan_penjualan_on($tgl_mulai, $tgl_selesai);
        $start = 1;
        foreach ($laporan as $row){
            $pdf->Cell(10);
            $pdf->Cell(10,6,$start++,1,0,'C');
            $pdf->Cell(15,6,$row->no_transaksi,1,0,'C');
            $pdf->Cell(80,6,date('d F Y h:i:s', strtotime($row->tgl_transaksi)),1,0);
            $pdf->Cell(25,6,$row->nama,1,0,'C');
            $pdf->Cell(20,6,$row->nama_pembayaran,1,0,'C');
            $pdf->Cell(20,6,$row->teller,1,1,'C');
            $pdf->Cell(20,6,'Rp. '.number_format($row->total_bayar),1,1,'C');
        }
        $jumlah=$this->M_Penjualan->get_jumlah($tgl_mulai, $tgl_selesai);
        foreach ($jumlah as $total) {
            $pdf->Cell(10);
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(110,6,'Total',1,0,'R'); 
            $pdf->Cell(20,6,'Rp. '.number_format($total->total_bayar),1,0,'C');
        }
        $jumlah_tunai=$this->M_Penjualan->get_jumlah_tunai($tgl_mulai, $tgl_selesai);
        foreach ($jumlah_tunai as $total) {
            $pdf->Cell(10);
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(110,6,'Total Pembayaran Tunai',1,0,'R'); 
            $pdf->Cell(20,6,'Rp. '.number_format($total->total_bayar),1,0,'C');
        }
        $jumlah_piutang=$this->M_Penjualan->get_jumlah_piutang($tgl_mulai, $tgl_selesai);
        foreach ($jumlah_piutang as $total) {
            $pdf->Cell(10);
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(110,6,'Total Pembayaran Piutang',1,0,'R'); 
            $pdf->Cell(20,6,'Rp. '.number_format($total->total_bayar),1,0,'C');
        }

        $filename = 'Laporan_Penjualan_Online.pdf';
        $pdf->Output('D',$filename);
    }*/

    public function laporan_penjualan_off()
    {       
        $tgl_mulai = $this->input->get('tgl_mulai');
        $tgl_selesai = $this->input->get('tgl_selesai');

        $data['laporan']=$this->M_Penjualan->get_laporan_penjualan_off($tgl_mulai, $tgl_selesai);

        $data['jumlah']=$this->M_Penjualan->get_jumlah_off($tgl_mulai, $tgl_selesai);
        $data['jumlah_tunai']=$this->M_Penjualan->get_jumlah_tunai_off($tgl_mulai, $tgl_selesai);
        $data['jumlah_piutang']=$this->M_Penjualan->get_jumlah_piutang_off($tgl_mulai, $tgl_selesai);
        // membedakan sebelum dan sesudah isi filter data
        if ($tgl_mulai == '' && $tgl_selesai == '') 
        {
            // kondisi ketika pertama buka, belum filter data kosong
            $data['tgl_mulai']='';
            $data['tgl_selesai']='';
        }else
        {
            // jika sudah ngisi form filter
            $data['tgl_mulai']=$tgl_mulai;
            $data['tgl_selesai']=$tgl_selesai;
            $data['laporan']=$this->M_Penjualan->get_laporan_penjualan_off($tgl_mulai, $tgl_selesai);
        }
        $this->template->set('title','Laporan Penjualan Offline');
        $this->template->load('adminLTE', 'contents' , 'view_penjualan/laporan_penjualan_offline', $data);
    }

    public function cetak_laporan_off_word($tgl_mulai, $tgl_selesai)
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=lap_penj_off__".$tgl_mulai."-".$tgl_selesai.".doc");

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'laporan'=>$this->M_Penjualan->get_laporan_penjualan_off($tgl_mulai, $tgl_selesai),
            'jumlah'=> $this->M_Penjualan->get_jumlah_off($tgl_mulai, $tgl_selesai),
            'jumlah_tunai'=> $this->M_Penjualan->get_jumlah_tunai_off($tgl_mulai, $tgl_selesai),
            'jumlah_piutang'=> $this->M_Penjualan->get_jumlah_piutang_off($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        
        $this->load->view('view_penjualan/cetak_laporan_penjualan_offline',$data);
    }

    public function cetak_laporan_off_excel($tgl_mulai, $tgl_selesai)
    {
        $this->load->helper('exportexcel');
        $namaFile = "lap_penj_off". "_" . "$tgl_mulai-$tgl_selesai" .".xls";
        $judul = "Laporan Penjualan Offline";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "No Transaksi");
        xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Transaksi");
        xlsWriteLabel($tablehead, $kolomhead++, "Pegawai");
        xlsWriteLabel($tablehead, $kolomhead++, "Pembayaran");
        xlsWriteLabel($tablehead, $kolomhead++, "Teller");
        xlsWriteLabel($tablehead, $kolomhead++, "Total Bayar");

        foreach ($this->M_Penjualan->get_laporan_penjualan_off($tgl_mulai, $tgl_selesai) as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->no_transaksi);
            xlsWriteLabel($tablebody, $kolombody++, $data->tgl_transaksi);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_pegawai);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_pembayaran);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_teller);
            xlsWriteNumber($tablebody, $kolombody++, $data->total_bayar);

            $tablebody++;
            $nourut++;
        }

        foreach ($this->M_Penjualan->get_jumlah_off($tgl_mulai, $tgl_selesai) as $data1) 
        {
            $kolombody = 5;
            xlsWriteLabel($tablebody, $kolombody++, "Total");
            xlsWriteNumber($tablebody, $kolombody++, $data1->total_bayar);
        }

        foreach ($this->M_Penjualan->get_jumlah_tunai_off($tgl_mulai, $tgl_selesai) as $data2) {
            xlsWriteLabel($tablebody, $kolombody++, "Total Tunai");
            xlsWriteNumber($tablebody, $kolombody++, $data2->total_bayar);
        }

        foreach ($this->M_Penjualan->get_jumlah_piutang_off($tgl_mulai, $tgl_selesai) as $data3) {
            xlsWriteLabel($tablebody, $kolombody++, "Total Piutang");
            xlsWriteNumber($tablebody, $kolombody++, $data3->total_bayar);
        }

        xlsEOF();
        exit();
    }

    public function cetak_laporan_off_pdf($tgl_mulai, $tgl_selesai)
    {
        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'laporan'=>$this->M_Penjualan->get_laporan_penjualan_off($tgl_mulai, $tgl_selesai),
            'jumlah'=> $this->M_Penjualan->get_jumlah_off($tgl_mulai, $tgl_selesai),
            'jumlah_tunai'=> $this->M_Penjualan->get_jumlah_tunai_off($tgl_mulai, $tgl_selesai),
            'jumlah_piutang'=> $this->M_Penjualan->get_jumlah_piutang_off($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        
        $this->load->view('view_penjualan/cetak_laporan_penjualan_offline',$data);
    }

    public function laporan_piutang()
    {       
        $tgl_mulai = $this->input->get('tgl_mulai');
        $tgl_selesai = $this->input->get('tgl_selesai');

        $data['piutang']=$this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai);
        $data['jumlah']=$this->M_Penjualan->get_piutang($tgl_mulai, $tgl_selesai);

        // membedakan sebelum dan sesudah isi filter data
        if ($tgl_mulai == '' && $tgl_selesai == '') 
        {
            // kondisi ketika pertama buka, belum filter data kosong
            $data['tgl_mulai']='';
            $data['tgl_selesai']='';
        }else
        {
            // jika sudah ngisi form filter
            $data['tgl_mulai']=$tgl_mulai;
            $data['tgl_selesai']=$tgl_selesai;
            $data['piutang']=$this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai);
        }
        $this->template->set('title','Laporan Piutang');
        $this->template->load('adminLTE', 'contents' , 'view_penjualan/laporan_piutang', $data);
    }

    public function cetak_laporan_piutang_word($tgl_mulai, $tgl_selesai)
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=laporan_piutang_".$tgl_mulai."-".$tgl_selesai.".doc");

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'piutang' => $this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai),
            'jumlah' => $this->M_Penjualan->get_piutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        
        $this->load->view('view_penjualan/cetak_laporan_piutang',$data);
    }

    /*public function cetak_laporan_piutang_excel($tgl_mulai, $tgl_selesai)
    {
        $this->load->helper('exportexcel');
        $namaFile = "lap_piutang". "_" . "$tgl_mulai-$tgl_selesai" .".xls";
        $judul = "Laporan Piutang";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Pegawai");
        xlsWriteLabel($tablehead, $kolomhead++, "No Transaksi");
        xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Transaksi");
        xlsWriteLabel($tablehead, $kolomhead++, "Keterangan");
        xlsWriteLabel($tablehead, $kolomhead++, "Piutang");

        $total_piutang = 0;
        $piutang = $this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai)
        for($i = 0; $i < count($piutang); $i++) {
            $tempNip = $row[$i]->nip;
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $piutang->nama);
            xlsWriteLabel($tablebody, $kolombody++, $piutang->no_transaksi);
            xlsWriteLabel($tablebody, $kolombody++, $piutang->tgl_transaksi);
            xlsWriteLabel($tablebody, $kolombody++, $piutang->keterangan);
            xlsWriteNumber($tablebody, $kolombody++, $piutang->total_bayar);

            $nextNip = isset($row[$i+1]->nip) ? $row[$i+1]->nip : 0
            $total_piutang += $row[$i]->total_bayar;
            if($tempNip != $nextNip):
                xlsWriteNumber($tablebody, $kolombody++, $piutang->total_piutang);
                $total_piutang = 0;
            endif;
            $tablebody++;
            $nourut++;
        }

        foreach ($this->M_Penjualan->get_piutang($tgl_mulai, $tgl_selesai) as $data) 
        {
            $kolombody = 4;
            xlsWriteLabel($tablebody, $kolombody++, "Total");
            xlsWriteNumber($tablebody, $kolombody++, $data->total_bayar);
        }

        xlsEOF();
        exit();
    }*/

    public function cetak_laporan_piutang_pdf($tgl_mulai, $tgl_selesai)
    {
        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'piutang' => $this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai),
            'jumlah' => $this->M_Penjualan->get_piutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        
        $this->load->view('view_penjualan/cetak_laporan_piutang',$data);
    }

    public function excel($tgl_mulai, $tgl_selesai)
    {
        header("Content-type: application/vnd.ms.excel");

        header("Content-Disposition: attachment;Filename=laporan_piutang_".$tgl_mulai."-".$tgl_selesai.".xls");

        $data = array(
            'tgl_mulai' => $tgl_mulai,
            'tgl_selesai' => $tgl_selesai,
            'piutang' => $this->M_Penjualan->get_laporan_piutang($tgl_mulai, $tgl_selesai),
            'jumlah' => $this->M_Penjualan->get_piutang($tgl_mulai, $tgl_selesai),
            'start' => 0
        );
        $this->load->view('view_penjualan/cetak_laporan_piutang',$data);
    }

    
}