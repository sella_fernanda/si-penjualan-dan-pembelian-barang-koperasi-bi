<?php echo form_open('anggota/C_Pengadaan/input_pengadaan/'); ?>

<section>
  <div class="col-md-6">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Pengadaan Barang</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <!-- /.box-header -->

      <div class="box-body">
        <div class="row">
          <div class="col-md-6">    
            <div class="form-group">
              <label>Nama Barang</label>
              <input type="text" name="nama_barang" class="form-control" placeholder="Masukkan nama barang" required>
              <?php echo form_error('nama_barang'); ?>
            </div>
            <div class="form-group">
              <label>Jumlah Barang</label>
              <input type="number" name="jumlah_barang" class="form-control" placeholder="Masukkan jumlah barang" required>
              <?php echo form_error('jumlah_barang'); ?>
            </div>
            <div class="form-group">
              <label>Satuan</label>
              <?php  
              foreach ($query2->result_array() as $row2)
              {
                $option2[$row2['id_jns_satuan']]=$row2['nama_satuan'];
              }
              echo form_dropdown('id_jns_satuan', $option2,'', 'class="form-control"');
              echo form_error('nama_satuan');
              ?>
            </div>

            <div class="box-footer" style="">
              <button type="submit" class="btn btn-info"><span class="fa fa-shopping-cart"></span> Tambahkan</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="badge bg-blue" style="width:250px; height: 30px; font-size: 20px;">Keranjang Pengadaan</h4>
          <form role="form" form name='pengadaan'>
            <div class="form-group">
              <label class="col-sm-4 control-label">Jenis Pengadaan</label>
              <div class="col-sm-6">
                <?php  
                $option = array(
                  ''         => '-- Pilih --',
                  'Instansi'         => 'Instansi',
                  'Pegawai'           => 'Pegawai'
                );

                $jns_pengadaan = array('instansi', 'pegawai');
                echo form_dropdown('jns_pengadaan', $option, '',array('class'=>'form-control','id'=>'jns_pengadaan'),'required="required"');
                ?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Jenis Pembayaran</label>
              <div class="col-sm-6">
                <?php  
                $options['']='-- Pilih --';
                foreach ($query->result_array() as $rows)
                {
                  $options[$rows['id_jns_pembayaran']]=$rows['nama_pembayaran'];
                }
                echo form_dropdown('id_jns_pembayaran', $options,'',array('class'=>'form-control','id'=>'jns_pembayaran','required'));
                ?>
                <br>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-bordered table-striped" >
                <thead>
                  <tr>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if ($this->session->has_userdata('barang')) {
                    $list_barang = $this->session->userdata('barang');
                    foreach ($list_barang as $key => $value) {
                      ?>
                      <tr>
                        <td><?php echo $value['nama_barang']; ?></td>
                        <td><?php echo $value['jumlah_barang']; ?></td>
                        <td><?php echo $this->M_Pengadaan->getNamaSatuan($value['id_jns_satuan']); ?></td>
                        <td><a href="<?php echo base_url('anggota/C_Pengadaan/hapus/'.$key) ?>"><span class="fa fa-trash"></span></a></td>
                      </tr>

                      <?php 
                    }  
                  }
                  ?>
                </tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
<div align="center">
  <a href="<?php echo site_url('anggota/C_Pengadaan/list_pengadaan')?>"><button type="button" class="btn btn-default">Cancel</button></a>
  <a href="<?php echo base_url('anggota/C_Pengadaan/reset') ?>"><button class="btn btn-danger">Reset</button></a>
  <a href="<?php echo base_url('anggota/C_Pengadaan/simpan/') ?>"><button id="btn-submit" class="btn btn-primary" disabled="true">Submit <span class="fa fa-arrow-circle-right"></span></button></a>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
  var cek = 0;
  var cek2 = 0;
  $("#jns_pembayaran").on("change", function(){ambilData($(this).val(), 1);});

  $("#jns_pengadaan").on("change", function(){ambilData($(this).val(), 2);});

  function ambilData(value, index){
    if(value == "" && index == 1){
      cek = 0;
      $("#btn-submit").attr('disabled',true);
    } else if(value == "" && index == 2){
      cek2 = 0;
      $("#btn-submit").attr('disabled',true);
    }else{
      if(index == 1){
        cek = 1;
        var data = {jns_pembayaran : value};
        var url = '<?php echo base_url('anggota/C_Pengadaan/ambil_jns_pembayaran'); ?>';
      } else{
        var data = {jns_pengadaan : value};
        var url = '<?php echo base_url('anggota/C_Pengadaan/ambil_jns_pengadaan'); ?>';
        cek2 = 1;
      }
           // mengambil id dari jns_pengadaan yang dipilih
           $.ajax({
            method : 'POST',
            url : url,
                data :  data, //mengirim variabel dengan nilai dari variabel itu
              });
           if(cek == 1 && cek2 == 1){
            $("#btn-submit").attr('disabled',false);
          }
        }
      }
    </script>
    <script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-2.2.3.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
  </div>