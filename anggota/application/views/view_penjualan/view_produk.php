<div class="container"  style="margin-left:-40px; width: 1200px"><br/>
	<div class="box box-info" >
		<div class="box-header with-border">
			<div class="row">
				<div class="col-md-8">
					<div class="input-group input-group-sm" style="width: 230px;">
						<form method="POST" action="<?php echo base_url("anggota/C_Penjualan/cari")?>">
							<div class="input-group-btn">
								<select name="kategori" id="kategori" class="form-control">
									<option value="">-- Cari Kategori --</option>
									<?php foreach($query4->result() as $row):?>
										<option value="<?php echo $row->id_kategori;?>"
											<?php echo set_select('kategori', $row->id_kategori); ?>><?php echo $row->nama_kategori;?>
										</option>
									<?php endforeach;?>
								</select>
								<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button> </div>
							</form>
						</div><br>
						<div class="row">
							<?php foreach ($barang->result_array() as $row) { ?>
								<div class="col-md-4">
									<div class="thumbnail">
										<?php if ($row['gambar'] == NULL) { ?>
											<img style="width: 100px; height:100px" src="<?php echo base_url('stoker/upload/default.JPG') ?>">
										<?php } else { ?>
											<img style="width: 100px; height:100px" src="<?php echo base_url('stoker/upload/'.$row['gambar']) ?>">
										<?php } ?>
										<div class="caption">
											<h4><?php echo $row['nama_barang'];?></h4>
											<h5 align="right" class="label label-success"><?php echo 'Stok : '.$row['stok_total'];?></h5>
											<div class="row">
												<div class="col-md-7">
													<h5><?php echo 'Rp '.number_format($row['harga_jual']);?></h5>
												</div>
												<div class="col-md-5">
													<input type="number" onKeyDown="return false" name="banyak" id="<?php echo $row['no_barcode'];?>" value="1" min="0" max="<?php echo $row['stok_total'] ?>" class="banyak form-control">
												</div>
											</div>
											<div align="center">
												<button class="add_cart btn btn-primary" data-produkid="<?php echo $row['no_barcode'];?>" data-produknama="<?php echo $row['nama_barang'];?>" data-produkharga="<?php echo $row['harga_jual'];?>"
													data-produkstok="<?php echo $row['stok_total'];?>">Beli</button>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>

							</div>

						</div>
						<div class="col-md-4">
							<h4 class="badge bg-blue" style="width:200px; height: 30px; font-size: 20px;">Keranjang Belanja</h4>
							<form action="tampil" method="post">
								<div class="form-group">
									<label class="col-sm-2 control-label">Bayar</label>
									<div class="col-sm-10">
										<?php  
										$options['']='-- Pilih --';
										foreach ($query2->result_array() as $rows)
										{
											$options[$rows['id_jns_pembayaran']]=$rows['nama_pembayaran'];
										}
										echo form_dropdown('id_jns_pembayaran', $options,'',array('class'=>'form-control','id'=>'jenis_pembayaran'));
										?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Opsi</label>
									<div class="col-sm-10">
										<?php  
										$option = array(
											''         => '-- Pilih --',
											'Diantar'         => 'Diantar',
											'Diambil'           => 'Diambil'
										);

										$jns_pengadaan = array('diantar', 'diambil');
										echo form_dropdown('opsi', $option, '',array('class'=>'form-control','id'=>'opsi'));
										?>
									</div>
								</div>
							</form>
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Barang</th>
										<th>Harga</th>
										<th>Jumlah</th>
										<th>Subtotal</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="detail_cart">

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/js/jquery-2.2.3.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){

				var cek = 0;
				var cek2 = 0;

				$('.add_cart').click(function(){
					var total_stok = $(this).data("produkstok"); //get total_Stok
					var no_barcode    = $(this).data("produkid");
					var nama_barang  = $(this).data("produknama");
					var harga_jual = $(this).data("produkharga");
					var banyak     = $('#' + no_barcode).val(); //jumlah yg dibeli
					var cek_total_cart = $('#jml-produk-' + no_barcode).html(); //cek jumlah di cart
					if(cek_total_cart == undefined){
						cek_total_cart = 0;
					}
					var hasil = parseInt(banyak) + parseInt(cek_total_cart);
					if(hasil <= total_stok ){
						$.ajax({
							url : "<?php echo base_url();?>anggota/C_Penjualan/add_to_cart",
							method : "POST",
							data : {no_barcode: no_barcode, nama_barang: nama_barang, harga_jual: harga_jual, banyak: banyak},
							success: function(data){
								$('#detail_cart').html(data);
							}
						});
					}else{
						alert('Anda Sudah Melebihi Jumlah Stok Produk ' + nama_barang);
					}


				});

		// Load shopping cart
		$('#detail_cart').load("<?php echo base_url();?>anggota/C_Penjualan/load_cart");

		//Hapus Item Cart
		$(document).on('click','.hapus_cart',function(){
			var row_id=$(this).attr("id"); //mengambil row_id dari artibut id
			$.ajax({
				url : "<?php echo base_url();?>anggota/C_Penjualan/hapus_cart",
				method : "POST",
				data : {row_id : row_id},
				success :function(data){
					$('#detail_cart').html(data);
				}
			});
		});

		//get data on change dan dissable button
		$("#jenis_pembayaran").on("change", function(){ambilData($(this).val(), 1);});

		$("#opsi").on("change", function(){ambilData($(this).val(), 2);});

		function ambilData(value, index){
			if(value == "" && index == 1){
				cek = 0;
				$("#btn-submit").attr('disabled',true);
			} else if(value == "" && index == 2){
				cek2 = 0;
				$("#btn-submit").attr('disabled',true);
			}else{
				if(index == 1){
					cek = 1;
        			var data = {jenis_pembayaran : value};//mengambil id dari jns_pengadaan yang dipilih
        			var url = '<?php echo base_url('anggota/C_Penjualan/ambil_jenis_pembayaran'); ?>';
        		} else{
        			var data = {opsi : value};
        			var url = '<?php echo base_url('anggota/C_Penjualan/ambil_opsi'); ?>';
        			cek2 = 1;
        		}
        		$.ajax({
        			method : 'POST',
        			url : url,
                data :  data, //mengirim variabel dengan nilai dari variabel itu
            });
        		if(cek == 1 && cek2 == 1){
        			$("#btn-submit").attr('disabled',false);
        		}
        	}


        }
    });
</script>