<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <br>
                <center>
                    <img src="<?php echo base_url() ?>assets/img/BI.png" alt="logo bank indonesia" width="500" height="200">
                </center> 
                <br> <br>
            </div>
        </div>   
    </div>
</div>

<section class="content">
          <!-- small box -->
        <div class="col-sm-1"></div>
           <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <p>Draft Pesanan</p>

              <h3><?php echo $jml_pesanan ?></h3>

            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="<?php echo site_url('anggota/C_Penjualan/list_pesan'); ?>" class="small-box-footer">
              Detail <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <p>Draft Pengadaan</p>

              <h3><?php echo $jml_pengadaan ?></h3>

            </div>
            <div class="icon">
              <i class="fa fa-envelope-o"></i>
            </div>
            <a href="<?php echo site_url('anggota/C_Pengadaan/list_pengadaan'); ?>" class="small-box-footer">
              Detail <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              </a>
              <p>Draft Piutang</p>

              <h3><?php echo $jml_piutang ?></h3>

            </div>
            <div class="icon">
              <i class="fa fa-envelope"></i>
            </div>
            <a href="<?php echo site_url('anggota/C_Penjualan/list_piutang'); ?>" class="small-box-footer">
              Detail <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
      </div>
</section>