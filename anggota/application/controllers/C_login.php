<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {
	
	// Logout di sini
	public function logout() {
		$this->session->unset_userdata('id_pengadaan');
		$this->session->unset_userdata('no_pengadaan');
		$this->session->unset_userdata('jns_pengadaan');
		$this->session->unset_userdata('jns_pembayaran');
		$this->session->unset_userdata('barang');
		$barang=array(
			'nama_barang' => $this->input->post('nama_barang'),
			'jumlah_barang' => $this->input->post('jumlah_barang'),
			'id_jns_satuan' => $this->input->post('id_jns_satuan')
		);
		
		$this->simple_login->logout();	
	}	
}