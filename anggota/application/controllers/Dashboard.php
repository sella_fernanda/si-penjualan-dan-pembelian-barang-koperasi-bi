<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Penjualan');
	}

	public function index()
	{
	 $this->template->set('title','HOME');
     $this->template->load('adminLTE2','contents','v_index2');
	}

	public function Toko()
	{
		$queryPesanan = $this->M_Penjualan->get_pesanan();
		$queryPengadaan = $this->M_Penjualan->get_pengadaan();
		$queryPiutang = $this->M_Penjualan->get_piutang();
		$data['jml_pesanan']  = count($queryPesanan);
		$data['jml_pengadaan'] = count($queryPengadaan);
		$data['jml_piutang']   = count($queryPiutang);
		$this->template->set('title', 'Dashboard');
		$this->template->load('adminLTE', 'contents' , 'v_index',$data);
	}

}

