<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Penjualan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('M_Penjualan','M_Pembelian'));
	}

	//list produk
	public function list()
	{
		$data['barang']=$this->M_Penjualan->get_list();
		$data['query4']=$this->M_Pembelian->getDataKategori();
		$data['query2']=$this->M_Penjualan->getJenisPembayaran();

		$this->template->set('title','Toko');
		$this->template->load('adminLTE','contents','view_penjualan/view_produk',$data);
	}

	//list transaksi
	public function list_pesan()
	{
		$data['pesan']=$this->M_Penjualan->get_list_pesan();

		$this->template->set('title','Pembelian Online');
		$this->template->load('adminLTE','contents','view_penjualan/view_pesan',$data);
	}

	//menambahkan data di keranjang cart
	function add_to_cart(){
		$data = array(
			'id' => $this->input->post('no_barcode'), 
			'name' => $this->input->post('nama_barang'), 
			'price' => $this->input->post('harga_jual'), 
			'qty' => $this->input->post('banyak'), 
		);
		$this->cart->insert($data);
		echo $this->show_cart();
	}

	function load_cart(){ //load data cart
		echo $this->show_cart();
	}

	function show_cart(){ //Fungsi untuk menampilkan data Cart
		$output = '';
		$no = 0;
		foreach ($this->cart->contents() as $items) {
			$no++;
			$output .='
			<tr>
			<td>'.$items['name'].'</td>
			<td>'.number_format($items['price']).'</td>
			<td id="jml-produk-'.$items['id'].'">'.$items['qty'].'</td>
			<td>'.number_format($items['subtotal']).'</td>
			<td><button type="button" id="'.$items['rowid'].'" class="hapus_cart btn btn-danger btn-xs"><span class="fa fa-trash"></span></button></td>
			</tr>
			';
		}
		$output .= '

		<tr>
		<th colspan="3">Total</th>
		<th colspan="2">'.'Rp '.number_format($this->cart->total()).'</th>
		</tr>

		<tr>
		<th><a href="'.base_url('anggota/C_Penjualan/reset').'"><button type="submit" name="btn_reset" class="btn btn-danger">Reset</button></a></th>
		<th><a href="'.base_url('anggota/C_Penjualan/bayar').'"><button type="submit" name="btn_submit" id="btn-submit" class="btn btn-primary" disabled="true">Pesan <span class="fa fa-angle-double-right"></span></button></a></th>
		</tr>
		';
		return $output;
	}

	//get value jenis pembayaran
	public function ambil_jenis_pembayaran(){
		$this->session->set_userdata('jenis_pembayaran', $this->input->post('jenis_pembayaran'));
	}

	//get value opsi
	public function ambil_opsi(){
		$this->session->set_userdata('opsi', $this->input->post('opsi'));
	}

	function reset()
	{
		$this->cart->destroy();
		redirect('anggota/C_Penjualan/list');
	}

	//simpan - insert
	function bayar()
	{
		if (!$this->cart->contents()) {
			redirect('anggota/C_Penjualan/list');
		}else{
			$nip=$this->session->userdata('nip');
			$cart = $this->cart->contents();
			date_default_timezone_set("Asia/Jakarta");
			$tgl_transaksi = date('Y-m-d h:i:s');
			$no_transaksi= $this->M_Penjualan->no_transaksi();
			$trans = array(
				'no_transaksi'=> $no_transaksi,
				'nip'=> $nip,
				'tgl_transaksi'=>$tgl_transaksi,
				'total_bayar'=> $this->cart->total(),
				'id_jns_pembayaran'=>$this->session->userdata('jenis_pembayaran'),
				'status'=>'Waiting List',
				'keterangan' => 'Pembelian Online',
				'opsi'=>$this->session->userdata('opsi')
			);

			$this->M_Penjualan->insert_trans($trans);

			foreach ($cart as $trans) {
				$det_trans = array(
					'no_transaksi'=>$no_transaksi,
					'no_barcode'=>$trans['id'],
					'jumlah_barang'=>$trans['qty']
				);

				$no_barcode=$trans['id'];
				$jumlah_barang=$trans['qty'];

				$this->M_Penjualan->insert_det_trans($det_trans,$no_barcode,$jumlah_barang);
			}

			$this->cart->destroy();
			$this->session->unset_userdata('jenis_pembayaran');
			$this->session->unset_userdata('opsi');
			redirect('anggota/C_Penjualan/list_pesan');
		}
	}

	function hapus_cart(){ //fungsi untuk menghapus item cart
		$data = array(
			'rowid' => $this->input->post('row_id'), 
			'qty' => 0, 
		);
		$this->cart->update($data);
		echo $this->show_cart();
	}
	
	public function detail($no_transaksi)
	{
		$this->template->set('title', 'Detail Pesanan');

		$query = $this->M_Penjualan->detail($no_transaksi);
		$data['query'] = $query;
		$this->template->load('adminLTE', 'contents' , 'view_penjualan/detail_pesan', $data);
	}

	//hapus (membatalkan) data transaksi
	public function batal($no_transaksi)
	{
		$penjualan = $this->M_Penjualan->get_by($no_transaksi);
		$i=0;
		foreach($penjualan->result_array() as $value) {
			$no_barcode= $value['no_barcode'];

			$this->M_Penjualan->update($no_transaksi,$no_barcode);
			$i++;
		}

		$this->M_Penjualan->delete($no_transaksi);
		redirect(site_url('anggota/C_Penjualan/list_pesan'));
	}

	//cari produk sesuai kategori
	function cari(){
		$kategori=$this->input->post('kategori');
		$data['barang'] = $this->M_Penjualan->pencarian($kategori);
		$data['query4']=$this->M_Pembelian->getDataKategori();
		$data['query2']=$this->M_Penjualan->getJenisPembayaran();
		$this->template->set('title','Toko');
		$this->template->load('adminLTE', 'contents' , 'view_penjualan/view_produk', $data);
	}

	//menampilkan list hutang sesuai akun
	public function list_piutang(){
		$data['total'] = $this->M_Penjualan->get_total_piutang()->total;
		$data['query']=$this->M_Penjualan->get_piutang_beli();
		$data['query1']=$this->M_Penjualan->get_piutang_pengadaan();

		$this->template->set('title','Piutang');
		$this->template->load('adminLTE','contents','view_penjualan/view_piutang',$data);
	}

}