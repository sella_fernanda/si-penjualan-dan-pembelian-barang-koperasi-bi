<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Pengadaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Pengadaan');
	}

	public function list_pengadaan()
	{
		$data['pengadaan']=$this->M_Pengadaan->get_list_pengadaan();

		$this->template->set('title','Pengadaan');
		$this->template->load('adminLTE','contents','view_pengadaan/list_pengadaan',$data);
	}

	public function input_pengadaan()
	{
		$this->form_validation->set_Rules('nama_barang','Nama Barang','required');
		$this->form_validation->set_Rules('jumlah_barang','Jumlah Barang','required');

		$this->form_validation->set_message('required', '*) Masukkan %s');

		//jika menyimpan
		if($this->form_validation->run() == TRUE) {
			if ($this->input->post()) {
				if ($this->session->has_userdata('barang')) {
					$data=$this->session->userdata();
					$this->session->unset_userdata('barang');
					$barang=array(
						'nama_barang' => $this->input->post('nama_barang'), 
						'jumlah_barang' => $this->input->post('jumlah_barang'),
						'id_jns_satuan' => $this->input->post('id_jns_satuan')
					);
					//menyimpan session dalam bentuk multi array
					$data['barang'][]=$barang;
					$this->session->set_userdata($data);

				}else{
					$data['barang'] = array(
						array(
							'nama_barang' => $this->input->post('nama_barang'),
							'jumlah_barang' => $this->input->post('jumlah_barang'),
							'id_jns_satuan' => $this->input->post('id_jns_satuan')  
						)
					);
					$this->session->set_userdata($data);
				}
			}
			redirect(site_url('anggota/C_Pengadaan/input_pengadaan'));

		}else{
			//jika tidak menyimpan
			$data['query']=$this->M_Pengadaan->getJenisPembayaran();
			$data['query2']=$this->M_Pengadaan->getDataSatuan();

			$this->template->set('title', 'Transaksi Pengadaan');
			$this->template->load('adminLTE', 'contents' , 'view_pengadaan/input_pengadaan', $data);
		}
	}

	//get value jenis pembayaran 
	public function ambil_jns_pembayaran(){
		$this->session->set_userdata('jns_pembayaran', $this->input->post('jns_pembayaran'));	
	}

	//get value jenis pengadaan 
	public function ambil_jns_pengadaan(){
		$this->session->set_userdata('jns_pengadaan', $this->input->post('jns_pengadaan'));
	}

	public function simpan()
	{
		if (!$this->session->has_userdata('barang')) {
			redirect('anggota/C_Pengadaan/input_pengadaan');
		}else{
			$nip=$this->session->userdata('nip');
			$id_pengadaan=$this->session->userdata('id_pengadaan');
			$no_pengadaan= $this->M_Pengadaan->no_pengadaan();
			$tgl_pengadaan= date('Y-m-d h:i:s');
			$jns_pengadaan=$this->session->userdata('jns_pengadaan');
			$id_jns_pembayaran=$this->session->userdata('jns_pembayaran');
			$barang = $this->session->userdata('barang');

			$trans = array(
				'no_pengadaan'=> $no_pengadaan,
				'nip'=> $nip,
				'tgl_pengadaan'=> $tgl_pengadaan,
				'id_jns_pembayaran'=> $id_jns_pembayaran,
				'status'=>'Waiting List'
			);
			if($this->session->userdata('jns_pengadaan')=="Instansi"){
				$trans['keterangan'] = "Pengadaan Instansi";
			}else{
				$trans['keterangan'] = "Pengadaan Pegawai";
			};

			$this->M_Pengadaan->insert_trans($trans);

			$data_barang=$this->session->userdata('barang');
			$i = 0;
			foreach ($data_barang as $brg) {
				$detail[] = array(
					'id_pengadaan'=> $id_pengadaan,
					'no_pengadaan'=> $no_pengadaan,
					'nama_barang'=> $brg['nama_barang'],
					'jumlah_barang'=> $brg['jumlah_barang'],
					'id_jns_satuan'=> $brg['id_jns_satuan']
				);

				$this->M_Pengadaan->insert_detail($detail[$i]);
				$i++;
			}

			$this->session->unset_userdata('id_pengadaan');
			$this->session->unset_userdata('no_pengadaan');
			$this->session->unset_userdata('jns_pengadaan');
			$this->session->unset_userdata('jns_pembayaran');
			$this->session->unset_userdata('barang');
			$barang=array(
				'nama_barang' => $this->input->post('nama_barang'),
				'jumlah_barang' => $this->input->post('jumlah_barang'),
				'id_jns_satuan' => $this->input->post('id_jns_satuan')
			);
			redirect(site_url('anggota/C_Pengadaan/list_pengadaan'));
		}
	}

	public function reset()
	{
		$this->session->unset_userdata('id_pengadaan');
		$this->session->unset_userdata('no_pengadaan');
		$this->session->unset_userdata('jns_pengadaan');
		$this->session->unset_userdata('jns_pembayaran');
		$this->session->unset_userdata('barang');
		$barang=array(
			'nama_barang' => $this->input->post('nama_barang'),
			'jumlah_barang' => $this->input->post('jumlah_barang'),
			'id_jns_satuan' => $this->input->post('id_jns_satuan')
		);
		redirect(base_url('anggota/C_Pengadaan/input_pengadaan'));
	}

	//hapus item pada tabel session
	public function hapus($nama_barang)
	{
		$list_barang=$this->session->userdata('barang');
		$this->session->unset_userdata('barang');
		unset($list_barang[$nama_barang]);
		$this->session->set_userdata('barang',$list_barang);
		redirect(base_url('anggota/C_Pengadaan/input_pengadaan'));
	}

	//hapus (membatalkan) data transaksi
	public function batal($no_pengadaan){
		$this->M_Pengadaan->delete($no_pengadaan);
		redirect(site_url('anggota/C_Pengadaan/list_pengadaan'));
	}	

	public function detail($no_pengadaan)
	{
		$this->template->set('title', 'Detail Pengadaan');

		$query = $this->M_Pengadaan->detail($no_pengadaan);
		$data['query'] = $query;
		$this->template->load('adminLTE', 'contents' , 'view_pengadaan/detail_pengadaan', $data);
	}
}