<?php if(! defined('BASEPATH')) exit('Akses langsung tidak diperbolehkan'); 

class Simple_login {
	//SET SUPER GLOBAL
	var $CI = NULL;
	public function __construct() {
		$this->CI =& get_instance();
	}
	
	// Proteksi halaman
	public function cek_login() {
		if($this->CI->session->userdata('username') == '' OR $this->CI->session->userdata('id_peran') != 'P005' ) {
			$this->CI->session->set_flashdata('sukses','Anda belum login');
			redirect(base_url('index.php/C_login'));
		}
	}
	// Fungsi logout
	public function logout() {
		$this->CI->session->unset_userdata('nip');
		$this->CI->session->unset_userdata('nama');
		$this->CI->session->unset_userdata('username');
		$this->CI->session->unset_userdata('id_peran');
		$this->CI->session->unset_userdata('id_akun');
		redirect(base_url());
	}
}