<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pengadaan extends CI_Model {

	public function get_list_pengadaan()
    {
        $this->db->select('*');
        $this->db->from('trans_pengadaan');
         $this->db->join('jenis_pembayaran', 'jenis_pembayaran.id_jns_pembayaran = trans_pengadaan.id_jns_pembayaran','left');
        $this->db->where('nip', $this->session->userdata('nip'));
        $this->db->order_by('field(status,"Waiting List","On Process", "Ready", "Selesai")');
        $this->db->order_by('no_pengadaan', "desc");
        return $this->db->get();
    }

	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('trans_pengadaan');
        $this->db->join('pegawai', 'trans_pengadaan.nip = pegawai.nip');
        $this->db->join('jenis_pembayaran', 'jenis_pembayaran.id_jns_pembayaran = trans_pengadaan.id_jns_pembayaran','left');
		return $this->db->get();
	}

    public function getDataSatuan()
    {
        return $this->db->get('jenis_satuan'); 
    }

    public function getNamaSatuan($id)
    {
        $this->db->where('id_jns_satuan',$id);
        return $this->db->get('jenis_satuan')->row()->nama_satuan; 
    }

	public function getJenisPembayaran()
	{
		$this->db->select('*');
		$this->db->from('jenis_pembayaran');
		$this->db->where('id_jns_pembayaran !=','P002');
		return $this->db->get();
	}

	public function tampil_input_bayar($id)
	{
		$this->db->select('nama_pembayaran');
		$this->db->from('jenis_pembayaran');
		$this->db->where('id_jns_pembayaran', $id);
		return $this->db->get();
	}

	public function insert_trans($data)
    {
        $this->db->insert('trans_pengadaan', $data); 
    }

     public function insert_detail($data)
    {
        $this->db->insert('detail_pengadaan', $data); 
    }

    public function get_pengadaan_by($no_pengadaan)
    {
        $this->db->select('*');
        $this->db->from('trans_pengadaan');
        $this->db->where('no_pengadaan', $no_pengadaan);
        $this->db->join('detail_pengadaan', 'trans_pengadaan.no_pengadaan = detail_pengadaan.no_pengadaan');
        return $this->db->get()->row_array();
    }

    public function update($no_pengadaan,$data)
    {
        $this->db->where('no_pengadaan',$no_pengadaan);
        $this->db->update('trans_pengadaan',$data);
    }

    public function update2($no_pengadaan,$data)
    {
        $this->db->where('no_pengadaan',$no_pengadaan);
        $this->db->update('trans_pengadaan',$data);
    }

    public function update3($no_pengadaan,$data)
    {
        $this->db->where('no_pengadaan',$no_pengadaan);
        $this->db->update('trans_pengadaan',$data);
    }

    public function delete($no_pengadaan)
    {
        $this->db->where('no_pengadaan', $no_pengadaan);
        $this->db->delete('trans_pengadaan');
    }

    public function detail($no_pengadaan)
    {
        $this->db->select('*'); 
        $this->db->from('detail_pengadaan');
        $this->db->join('trans_pengadaan', 'trans_pengadaan.no_pengadaan = detail_pengadaan.no_pengadaan');
        $this->db->join('jenis_pembayaran', 'jenis_pembayaran.id_jns_pembayaran = trans_pengadaan.id_jns_pembayaran','left');
        $this->db->join('pegawai', 'trans_pengadaan.nip = pegawai.nip');
        $this->db->join('jenis_satuan', 'detail_pengadaan.id_jns_satuan = jenis_satuan.id_jns_satuan');

        $query = $this->db->get_where('', array('detail_pengadaan.no_pengadaan' => $no_pengadaan));
        return $query;
    }

	public function no_pengadaan()   
	{    
		$this->db->select('RIGHT(trans_pengadaan.no_pengadaan,3) as kode', FALSE);
		$this->db->order_by('no_pengadaan','DESC');    
		$this->db->limit(1);    
            $query = $this->db->get('trans_pengadaan');      //cek dulu apakah ada sudah ada kode di tabel.    
            if($query->num_rows() <> 0)
            {      
                //jika kode ternyata sudah ada.      
            	$data = $query->row();      
            	$kode = intval($data->kode) + 1;    
            }
            else{      
               //jika kode belum ada      
            	$kode = 1;    
            }
            $kodemax = str_pad($kode, 5, "0", STR_PAD_LEFT);    
            $kodejadi = "PA".$kodemax;    
            return $kodejadi;  
        }

    }