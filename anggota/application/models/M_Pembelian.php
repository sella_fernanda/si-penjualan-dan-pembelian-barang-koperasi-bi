<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pembelian extends CI_Model {

	public function get_list()
	{
		$this->db->select('*');
		$this->db->from('trans_pembelian');
        $this->db->join('suplier', 'suplier.id_suplier = trans_pembelian.id_suplier');
        $this->db->join('jenis_pembayaran', 'jenis_pembayaran.id_jns_pembayaran = trans_pembelian.id_jns_pembayaran','left');
		return $this->db->get();
	}

    public function tampil_input_suplier($id)
    {
        $this->db->select('nama_suplier');
        $this->db->from('suplier');
        $this->db->where('id_suplier', $id);
        return $this->db->get();
    }

    public function tampil_input_bayar($id)
    {
        $this->db->select('nama_pembayaran');
        $this->db->from('jenis_pembayaran');
        $this->db->where('id_jns_pembayaran', $id);
        return $this->db->get();
    }

    public function tampil_input_kategori($id)
    {
        $this->db->select('nama_kategori');
        $this->db->from('kategori_barang');
        $this->db->where('id_kategori', $id);
        return $this->db->get();
    }

    public function tampil_input_barang($id)
    {
        $this->db->select('nama_barang');
        $this->db->from('barang');
        $this->db->where('no_barcode', $id);
        return $this->db->get();
    }

	public function getDataBarang()
    {
        return $this->db->get('barang'); 
    }

    public function getJenisPembayaran()
    {
        return $this->db->get('jenis_pembayaran'); 
    }

    public function getDataSuplier()
    {
        return $this->db->get('suplier'); 
    }

    public function getHarga($no_barcode)
        {
            $this->db->select('harga.harga_jual');
            $this->db->from('harga');
            $this->db->where('harga.no_barcode', $no_barcode);
            $this->db->where('keterangan', 'Berlaku');
            $query =$this->db->get();
            $query = $query->row();
            return $query->harga_jual;
        }

    public function getKet($no_barcode)
    {
            $data['keterangan']='Tidak Berlaku';
            $this->db->where('no_barcode',$no_barcode);
            $this->db->update('harga',$data);
    }

    public function buat_kode()   
    {    
            $this->db->select('RIGHT(harga.id_harga,3) as kode', FALSE);
            $this->db->order_by('id_harga','DESC');    
            $this->db->limit(1);    
            $query = $this->db->get('harga');      //cek dulu apakah ada sudah ada kode di tabel.    
                if($query->num_rows() <> 0)
                {      
                //jika kode ternyata sudah ada.      
                    $data = $query->row();      
                    $kode = intval($data->kode) + 1;    
                }
                else{      
               //jika kode belum ada      
                    $kode = 1;    
                }
            $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT);    
            $kodejadi = "H".$kodemax;    
            return $kodejadi;  
    }

    public function insert_trans($data)
    {
        $this->db->insert('trans_pembelian', $data); 
    }

     public function insert_detail($data)
    {
        $this->db->insert('detail_pembelian', $data); 
    }

    public function insert($data,$data1/*,$data2,$id_pembelian,$no_barcode,$stok_total*/)
	{
    	$this->db->insert('trans_pembelian', $data1);
        $this->db->insert('detail_pembelian', $data);
        /*
        $hlama=$this->getHarga($no_barcode);
        $hbaru=$this->input->post('harga_jual');
        $hasil=($hlama + $hbaru)/2 ;
        
        $harga['harga_jual']= $hasil;
        
        $this->getKet($no_barcode);

        $id=$this->buat_kode();
        $data2['id_harga']=$id;

        $data2['keterangan']='Berlaku';

        $this->db->insert('harga', $data2);  

        $this->db->where('id_harga',$id);
        $this->db->update('harga',$harga);

        $this->db->where('no_barcode',$no_barcode);
        $this->db->update('barang',$stok_total);*/
	}

	public function getDataKategori()
    {
        return $this->db->get('kategori_barang'); 
    }
    public function getIdKategori($id)
    {
        $this->db->where('id_kategori',$id);
        return $this->db->get('kategori_barang')->row()->nama_kategori; 
    }

    public function getIdBarang($id)
    {
        $this->db->where('no_barcode',$id);
        return $this->db->get('barang')->row()->nama_barang; 
    }

    function get_subkategori($no_barcode){
        $hasil=$this->db->query("SELECT * FROM barang WHERE id_kategori='$no_barcode'");
        return $hasil->result();
    }

    /*public function get_by($id_pembelian)
	{
		$this->db->select('*');
		$this->db->from('trans_pembelian');
		$this->db->where('id_pembelian', $id_pembelian);
		return $this->db->get()->row_array();
	}*/

    /*public function get_trans_pembelian($id_pembelian){
        $this->db->where('id_pembelian',$id_pembelian);
        $query = $this->db->get('trans_pembelian');
        return $query->result();
    }*/


    /*public function getHarga_update($no_barcode)
        {
            $this->db->select('harga.harga_jual');
            $this->db->from('harga');
            $this->db->where('harga.no_barcode', $no_barcode);
            $this->db->where('keterangan', 'Tidak Berlaku');
            $this->db->limit(1);
            $query =$this->db->get();
            $query = $query->row();
            return $query->harga_jual;
        }*/

    
	/*public function update($id_pembelian,$data,$no_barcode,$stok_total)
	{  
		$this->db->where('id_pembelian', $id_pembelian);
		$this->db->update('trans_pembelian',$data); 
        
        $halama=$this->getHarga_update($no_barcode);
        $habaru=$this->input->post('harga_jual');
        $hasil=($halama + $habaru)/2 ;
        
        $harga['harga_jual']= $hasil;

        $this->db->where('no_barcode',$no_barcode);
        $this->db->where('keterangan','Berlaku');
        $this->db->update('harga',$harga);

        $this->db->where('no_barcode',$no_barcode);
        $this->db->update('barang',$stok_total);
	}*/

	public function detail($no_pembelian)
	{
        $this->db->select('*'); 
        $this->db->from('trans_pembelian');
        $this->db->join('detail_pembelian', 'detail_pembelian.no_pembelian = trans_pembelian.no_pembelian');
        $this->db->join('pegawai', 'trans_pembelian.nip = pegawai.nip');
        $this->db->join('barang', 'detail_pembelian.no_barcode = barang.no_barcode');
        $this->db->join('kategori_barang', 'detail_pembelian.id_kategori = kategori_barang.id_kategori');
        $query = $this->db->get_where('', array('trans_pembelian.no_pembelian' => $no_pembelian));
        return $query;
	}

	public function delete($no_pembelian)
		{
			$this->db->where('no_pembelian', $no_pembelian);
			$this->db->delete('trans_pembelian');
		}
}